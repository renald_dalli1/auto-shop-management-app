﻿namespace GUI
{
    partial class StaffSearchJobNumberForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblJobNumber = new System.Windows.Forms.Label();
            this.txtSearchJobNumber = new System.Windows.Forms.TextBox();
            this.btnSearchJob = new System.Windows.Forms.Button();
            this.pbDeteteJob = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbDeteteJob)).BeginInit();
            this.SuspendLayout();
            // 
            // lblJobNumber
            // 
            this.lblJobNumber.AutoSize = true;
            this.lblJobNumber.Location = new System.Drawing.Point(171, 271);
            this.lblJobNumber.Name = "lblJobNumber";
            this.lblJobNumber.Size = new System.Drawing.Size(89, 17);
            this.lblJobNumber.TabIndex = 8;
            this.lblJobNumber.Text = "Job Number:";
            this.lblJobNumber.Click += new System.EventHandler(this.lblJobNumber_Click);
            // 
            // txtSearchJobNumber
            // 
            this.txtSearchJobNumber.Location = new System.Drawing.Point(263, 270);
            this.txtSearchJobNumber.Name = "txtSearchJobNumber";
            this.txtSearchJobNumber.Size = new System.Drawing.Size(138, 22);
            this.txtSearchJobNumber.TabIndex = 7;
            this.txtSearchJobNumber.TextChanged += new System.EventHandler(this.txtSearchJobNumber_TextChanged);
            // 
            // btnSearchJob
            // 
            this.btnSearchJob.Location = new System.Drawing.Point(232, 316);
            this.btnSearchJob.Name = "btnSearchJob";
            this.btnSearchJob.Size = new System.Drawing.Size(123, 53);
            this.btnSearchJob.TabIndex = 6;
            this.btnSearchJob.Text = "Search";
            this.btnSearchJob.UseVisualStyleBackColor = true;
            this.btnSearchJob.Click += new System.EventHandler(this.btnSearchJob_Click);
            // 
            // pbDeteteJob
            // 
            this.pbDeteteJob.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbDeteteJob.Image = global::GUI.Properties.Resources.ASmanagment_App;
            this.pbDeteteJob.Location = new System.Drawing.Point(174, 12);
            this.pbDeteteJob.Name = "pbDeteteJob";
            this.pbDeteteJob.Size = new System.Drawing.Size(240, 222);
            this.pbDeteteJob.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbDeteteJob.TabIndex = 14;
            this.pbDeteteJob.TabStop = false;
            this.pbDeteteJob.Click += new System.EventHandler(this.pbDeteteJob_Click);
            // 
            // StaffSearchJobNumberForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(590, 414);
            this.Controls.Add(this.pbDeteteJob);
            this.Controls.Add(this.lblJobNumber);
            this.Controls.Add(this.txtSearchJobNumber);
            this.Controls.Add(this.btnSearchJob);
            this.Name = "StaffSearchJobNumberForm";
            this.Text = "Search Job Number";
            this.Load += new System.EventHandler(this.StaffSearchJobNumberForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbDeteteJob)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblJobNumber;
        private System.Windows.Forms.TextBox txtSearchJobNumber;
        private System.Windows.Forms.Button btnSearchJob;
        private System.Windows.Forms.PictureBox pbDeteteJob;
    }
}