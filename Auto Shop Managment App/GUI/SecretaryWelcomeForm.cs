﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class SecretaryWelcomeForm : Form
    {
        public SecretaryWelcomeForm()
        {
            InitializeComponent();
        }

        private void tsmiAddNewJob_Click(object sender, EventArgs e)
        {
            SecretaryJobStatusUpdateForm secretaryjobstatusupdateForm = new SecretaryJobStatusUpdateForm();

            secretaryjobstatusupdateForm.Show();
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void SecretaryWelcomeForm_Load(object sender, EventArgs e)
        {

        }

        private void tsmiEndJob_Click(object sender, EventArgs e)
        {
            SecretaryEndJobForm secretaryendjobForm = new SecretaryEndJobForm();

            secretaryendjobForm.Show();

        }

        private void tsmiSendEmail_Click(object sender, EventArgs e)
        {
            SecretarySendEmailForm secretarysendemailForm = new SecretarySendEmailForm();
          
            secretarysendemailForm.Show();
        }

        private void tsmiRepairReport_Click(object sender, EventArgs e)
        {
            SecretaryRepairReportForm secretaryrepairreportForm = new SecretaryRepairReportForm();

            secretaryrepairreportForm.Show();

        }

        private void tsmiJobList_Click(object sender, EventArgs e)
        {
            SecretaryJobListForm secretaryjoblistForm = new SecretaryJobListForm();

            secretaryjoblistForm.Show();
        }

        private void SecretaryWelcomeForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }
    }
}
