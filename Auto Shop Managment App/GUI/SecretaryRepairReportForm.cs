﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class SecretaryRepairReportForm : Form
    {
        public SecretaryRepairReportForm()
        {
            InitializeComponent();
        }

        private void lblContact_Click(object sender, EventArgs e)
        {

        }

        private void txtContact_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblLoggedAs_Click(object sender, EventArgs e)
        {

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string userInput = Microsoft.VisualBasic.Interaction.InputBox("Please Enter Job Number", "Database Search");

            int search = int.Parse(userInput);

            DataTable dt = AutoShopService.GetJobDetails(search);

            

            if (Convert.ToInt32(dt.Rows.Count.ToString()) > 0)
            {
                this.txtJobNumber.Text = dt.Rows[0]["JobNumber"].ToString();
                this.txtName.Text = dt.Rows[0]["Name"].ToString();
                this.txtSurname.Text = dt.Rows[0]["Surname"].ToString();
                this.txtIdCard.Text = dt.Rows[0]["IdCard"].ToString();
                this.txtContact.Text = dt.Rows[0]["Contact"].ToString();
                this.txtEmail.Text = dt.Rows[0]["Email"].ToString();
                this.txtAddress.Text = dt.Rows[0]["Address"].ToString();
                this.txtVehicleModel.Text = dt.Rows[0]["VehicleModel"].ToString();
                this.txtNumberPlate.Text = dt.Rows[0]["NumberPlate"].ToString();

            }
            else
            {
                AutoShopService.CloseConnection();

            }

            DataTable ds = AutoShopService.GetSurveyDetails(search);

         

            if (Convert.ToInt32(ds.Rows.Count.ToString()) > 0)
            {
                this.cmbFaultType.Text = ds.Rows[0]["FaultType"].ToString();
                this.cmbFaildPart.Text = ds.Rows[0]["FaildPart"].ToString();
            }
            else
            {
                AutoShopService.CloseConnection();

            }

            DataTable dr = AutoShopService.GetPartsAndRepairDetails(search);

    

            if (Convert.ToInt32(dr.Rows.Count.ToString()) > 0)
            {

                this.txtRepairNotes.Text = dr.Rows[0]["PartsAndRepairNotes"].ToString();
            }
            else
            {
                AutoShopService.CloseConnection();

            }

            DataTable drr = AutoShopService.GetRepairReportDetails(search);

           

            if (Convert.ToInt32(drr.Rows.Count.ToString()) > 0)
            {
                this.txtIssuedBy.Text = drr.Rows[0]["IssuedBy"].ToString();
                this.txtDateIssued.Text = drr.Rows[0]["DateIssued"].ToString();
            }
            else
            {
                AutoShopService.CloseConnection();

            }

            //Data Grid View PartsList
            System.Data.DataTable dtPartsList = AutoShopService.GetPartsItemsToView(int.Parse(txtJobNumber.Text));

            dgPartList.Columns.Clear();
            dgPartList.AutoSize = true;
            dgPartList.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCells;
            dgPartList.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;

            DataGridViewLinkColumn link = new DataGridViewLinkColumn();
            link.UseColumnTextForLinkValue = false;
            link.LinkBehavior = LinkBehavior.SystemDefault;
            link.HeaderText = "PartsListLink";
            link.LinkColor = Color.Blue;
            link.TrackVisitedState = false;
            link.Text = "Link";
            link.UseColumnTextForLinkValue = false;
            this.dgPartList.Columns.Add(link);
            dgPartList.Columns[0].Visible = false;

            dgPartList.DataSource = dtPartsList;

        }

        private void SecretaryRepairReportForm_Load(object sender, EventArgs e)
        {
        }

        private void pnlDetails_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //Validation if update button is clicked and important textboxex are left null
            if (txtIssuedBy.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Please Search Record or Update Records");
                return;
            }

            //Validation if update button is clicked and important textboxex are left null
            if (txtDateIssued.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Please Search Record or Update Records");
                return;
            }
            try
            {

                bool success = AutoShopService.AddRepairReportDetails(txtIssuedBy.Text, DateTime.Parse(txtDateIssued.Text), int.Parse(this.txtJobNumber.Text));

                //Showing messagebox to the user informing him that entry was successful


                MessageBox.Show("Repair Report Updated", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (SqlException ex)
            {

                MessageBox.Show("Error Encountered whilst submitting New Job Order", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw ex;
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
         // on Print Button Click the windows form containing the current details will be printed  
                Panel panel = new Panel();
                this.Controls.Add(panel);
                Graphics grp = panel.CreateGraphics();
                Size formSize = this.ClientSize;
                bitmap = new Bitmap(formSize.Width, formSize.Height, grp);
                grp = Graphics.FromImage(bitmap);
                Point panelLocation = PointToScreen(panel.Location);
                grp.CopyFromScreen(panelLocation.X, panelLocation.Y, 0, 0, formSize);
                printPreviewDialog1.Document = printDocument1;
                printPreviewDialog1.PrintPreviewControl.Zoom = 1;
                printPreviewDialog1.ShowDialog();
            }
            Bitmap bitmap;
            private void CaptureScreen()
            {
                Graphics myGraphics = this.CreateGraphics();
                Size s = this.Size;
                bitmap = new Bitmap(s.Width, s.Height, myGraphics);
                Graphics memoryGraphics = Graphics.FromImage(bitmap);
                memoryGraphics.CopyFromScreen(this.Location.X, this.Location.Y, 0, 0, s);
            }
        

        private void printPreviewDialog1_Load(object sender, EventArgs e)
        {

        }

        private void printDocument1_PrintPage(object sender, PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(bitmap, 0, 0);
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
