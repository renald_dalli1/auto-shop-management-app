﻿namespace GUI
{
    partial class StaffUpdateJobDetailsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StaffUpdateJobDetailsForm));
            this.lblCurrentStatusTitle = new System.Windows.Forms.Label();
            this.cmbCurrentStatus = new System.Windows.Forms.ComboBox();
            this.pnlCurrentStatus = new System.Windows.Forms.Panel();
            this.txtCurrentStatusD = new System.Windows.Forms.TextBox();
            this.lblDetailsTitle = new System.Windows.Forms.Label();
            this.pnlDetails = new System.Windows.Forms.Panel();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.lblAddress = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.txtSurname = new System.Windows.Forms.TextBox();
            this.lblSurname = new System.Windows.Forms.Label();
            this.txtNumberPlate = new System.Windows.Forms.TextBox();
            this.lblNumberPlate = new System.Windows.Forms.Label();
            this.txtVehicleModel = new System.Windows.Forms.TextBox();
            this.lblVehicleModel = new System.Windows.Forms.Label();
            this.txtContact = new System.Windows.Forms.TextBox();
            this.lblContact = new System.Windows.Forms.Label();
            this.txtIdCard = new System.Windows.Forms.TextBox();
            this.lblIDCard = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.lblLoggedAs = new System.Windows.Forms.Label();
            this.txtJobNumber = new System.Windows.Forms.TextBox();
            this.lblJobNumber = new System.Windows.Forms.Label();
            this.txtSurveyCompletedBy = new System.Windows.Forms.TextBox();
            this.lblJSurveyCompletedBy = new System.Windows.Forms.Label();
            this.txtRepairedBy = new System.Windows.Forms.TextBox();
            this.lblRepairedBy = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.lblFaultType = new System.Windows.Forms.Label();
            this.lblFaildPart = new System.Windows.Forms.Label();
            this.txtRepairNotes = new System.Windows.Forms.RichTextBox();
            this.lblRepairNotes = new System.Windows.Forms.Label();
            this.cmbFaultType = new System.Windows.Forms.ComboBox();
            this.cmbFaildPart = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnDeletePart = new System.Windows.Forms.Button();
            this.btnAddPart = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPrice = new System.Windows.Forms.Label();
            this.lblBrand = new System.Windows.Forms.Label();
            this.lblQuantity = new System.Windows.Forms.Label();
            this.lblPartDescription = new System.Windows.Forms.Label();
            this.lblPartNumber = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.txtBrand = new System.Windows.Forms.TextBox();
            this.txtQuantity = new System.Windows.Forms.TextBox();
            this.txtPartDescription = new System.Windows.Forms.TextBox();
            this.txtPartNumber = new System.Windows.Forms.TextBox();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.dgPartList = new System.Windows.Forms.DataGridView();
            this.lblPartsList = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.pnlCurrentStatus.SuspendLayout();
            this.pnlDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPartList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCurrentStatusTitle
            // 
            this.lblCurrentStatusTitle.AutoSize = true;
            this.lblCurrentStatusTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentStatusTitle.Location = new System.Drawing.Point(665, 70);
            this.lblCurrentStatusTitle.Name = "lblCurrentStatusTitle";
            this.lblCurrentStatusTitle.Size = new System.Drawing.Size(147, 24);
            this.lblCurrentStatusTitle.TabIndex = 39;
            this.lblCurrentStatusTitle.Text = "Current Status:";
            // 
            // cmbCurrentStatus
            // 
            this.cmbCurrentStatus.FormattingEnabled = true;
            this.cmbCurrentStatus.Location = new System.Drawing.Point(44, 44);
            this.cmbCurrentStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbCurrentStatus.Name = "cmbCurrentStatus";
            this.cmbCurrentStatus.Size = new System.Drawing.Size(386, 24);
            this.cmbCurrentStatus.TabIndex = 0;
            // 
            // pnlCurrentStatus
            // 
            this.pnlCurrentStatus.Controls.Add(this.txtCurrentStatusD);
            this.pnlCurrentStatus.Controls.Add(this.cmbCurrentStatus);
            this.pnlCurrentStatus.Location = new System.Drawing.Point(669, 95);
            this.pnlCurrentStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlCurrentStatus.Name = "pnlCurrentStatus";
            this.pnlCurrentStatus.Size = new System.Drawing.Size(448, 146);
            this.pnlCurrentStatus.TabIndex = 36;
            // 
            // txtCurrentStatusD
            // 
            this.txtCurrentStatusD.Location = new System.Drawing.Point(44, 97);
            this.txtCurrentStatusD.Name = "txtCurrentStatusD";
            this.txtCurrentStatusD.ReadOnly = true;
            this.txtCurrentStatusD.Size = new System.Drawing.Size(386, 22);
            this.txtCurrentStatusD.TabIndex = 1;
            // 
            // lblDetailsTitle
            // 
            this.lblDetailsTitle.AutoSize = true;
            this.lblDetailsTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDetailsTitle.Location = new System.Drawing.Point(27, 70);
            this.lblDetailsTitle.Name = "lblDetailsTitle";
            this.lblDetailsTitle.Size = new System.Drawing.Size(291, 24);
            this.lblDetailsTitle.TabIndex = 35;
            this.lblDetailsTitle.Text = "Customer and Vehicle Details:";
            // 
            // pnlDetails
            // 
            this.pnlDetails.Controls.Add(this.txtAddress);
            this.pnlDetails.Controls.Add(this.lblAddress);
            this.pnlDetails.Controls.Add(this.txtEmail);
            this.pnlDetails.Controls.Add(this.lblEmail);
            this.pnlDetails.Controls.Add(this.txtSurname);
            this.pnlDetails.Controls.Add(this.lblSurname);
            this.pnlDetails.Controls.Add(this.txtNumberPlate);
            this.pnlDetails.Controls.Add(this.lblNumberPlate);
            this.pnlDetails.Controls.Add(this.txtVehicleModel);
            this.pnlDetails.Controls.Add(this.lblVehicleModel);
            this.pnlDetails.Controls.Add(this.txtContact);
            this.pnlDetails.Controls.Add(this.lblContact);
            this.pnlDetails.Controls.Add(this.txtIdCard);
            this.pnlDetails.Controls.Add(this.lblIDCard);
            this.pnlDetails.Controls.Add(this.txtName);
            this.pnlDetails.Controls.Add(this.lblName);
            this.pnlDetails.Location = new System.Drawing.Point(16, 95);
            this.pnlDetails.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlDetails.Name = "pnlDetails";
            this.pnlDetails.Size = new System.Drawing.Size(625, 238);
            this.pnlDetails.TabIndex = 34;
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(107, 196);
            this.txtAddress.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(492, 22);
            this.txtAddress.TabIndex = 7;
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(12, 196);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(64, 17);
            this.lblAddress.TabIndex = 20;
            this.lblAddress.Text = "Address:";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(107, 158);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(492, 22);
            this.txtEmail.TabIndex = 6;
            this.txtEmail.Leave += new System.EventHandler(this.txtEmail_Leave);
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(12, 158);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(46, 17);
            this.lblEmail.TabIndex = 18;
            this.lblEmail.Text = "Email:";
            // 
            // txtSurname
            // 
            this.txtSurname.Location = new System.Drawing.Point(340, 22);
            this.txtSurname.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSurname.Name = "txtSurname";
            this.txtSurname.Size = new System.Drawing.Size(124, 22);
            this.txtSurname.TabIndex = 1;
            this.txtSurname.Leave += new System.EventHandler(this.txtSurname_Leave);
            // 
            // lblSurname
            // 
            this.lblSurname.AutoSize = true;
            this.lblSurname.Location = new System.Drawing.Point(245, 22);
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Size = new System.Drawing.Size(69, 17);
            this.lblSurname.TabIndex = 16;
            this.lblSurname.Text = "Surname:";
            // 
            // txtNumberPlate
            // 
            this.txtNumberPlate.Location = new System.Drawing.Point(448, 112);
            this.txtNumberPlate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNumberPlate.Name = "txtNumberPlate";
            this.txtNumberPlate.Size = new System.Drawing.Size(151, 22);
            this.txtNumberPlate.TabIndex = 5;
            this.txtNumberPlate.Leave += new System.EventHandler(this.txtNumberPlate_Leave);
            // 
            // lblNumberPlate
            // 
            this.lblNumberPlate.AutoSize = true;
            this.lblNumberPlate.Location = new System.Drawing.Point(339, 113);
            this.lblNumberPlate.Name = "lblNumberPlate";
            this.lblNumberPlate.Size = new System.Drawing.Size(98, 17);
            this.lblNumberPlate.TabIndex = 14;
            this.lblNumberPlate.Text = "Number Plate:";
            // 
            // txtVehicleModel
            // 
            this.txtVehicleModel.Location = new System.Drawing.Point(448, 62);
            this.txtVehicleModel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtVehicleModel.Name = "txtVehicleModel";
            this.txtVehicleModel.Size = new System.Drawing.Size(151, 22);
            this.txtVehicleModel.TabIndex = 3;
            this.txtVehicleModel.Leave += new System.EventHandler(this.txtVehicleModel_Leave);
            // 
            // lblVehicleModel
            // 
            this.lblVehicleModel.AutoSize = true;
            this.lblVehicleModel.Location = new System.Drawing.Point(339, 65);
            this.lblVehicleModel.Name = "lblVehicleModel";
            this.lblVehicleModel.Size = new System.Drawing.Size(100, 17);
            this.lblVehicleModel.TabIndex = 12;
            this.lblVehicleModel.Text = "Vehicle Model:";
            // 
            // txtContact
            // 
            this.txtContact.Location = new System.Drawing.Point(107, 116);
            this.txtContact.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtContact.Name = "txtContact";
            this.txtContact.Size = new System.Drawing.Size(100, 22);
            this.txtContact.TabIndex = 4;
            this.txtContact.Leave += new System.EventHandler(this.txtContact_Leave);
            // 
            // lblContact
            // 
            this.lblContact.AutoSize = true;
            this.lblContact.Location = new System.Drawing.Point(12, 116);
            this.lblContact.Name = "lblContact";
            this.lblContact.Size = new System.Drawing.Size(60, 17);
            this.lblContact.TabIndex = 8;
            this.lblContact.Text = "Contact:";
            // 
            // txtIdCard
            // 
            this.txtIdCard.Location = new System.Drawing.Point(107, 65);
            this.txtIdCard.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtIdCard.Name = "txtIdCard";
            this.txtIdCard.Size = new System.Drawing.Size(100, 22);
            this.txtIdCard.TabIndex = 2;
            this.txtIdCard.Leave += new System.EventHandler(this.txtIdCard_Leave);
            // 
            // lblIDCard
            // 
            this.lblIDCard.AutoSize = true;
            this.lblIDCard.Location = new System.Drawing.Point(12, 65);
            this.lblIDCard.Name = "lblIDCard";
            this.lblIDCard.Size = new System.Drawing.Size(59, 17);
            this.lblIDCard.TabIndex = 6;
            this.lblIDCard.Text = "ID Card:";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(107, 21);
            this.txtName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(100, 22);
            this.txtName.TabIndex = 0;
            this.txtName.Leave += new System.EventHandler(this.txtName_Leave);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(12, 21);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(49, 17);
            this.lblName.TabIndex = 4;
            this.lblName.Text = "Name:";
            // 
            // lblLoggedAs
            // 
            this.lblLoggedAs.AutoSize = true;
            this.lblLoggedAs.Location = new System.Drawing.Point(979, 1125);
            this.lblLoggedAs.Name = "lblLoggedAs";
            this.lblLoggedAs.Size = new System.Drawing.Size(142, 17);
            this.lblLoggedAs.TabIndex = 40;
            this.lblLoggedAs.Text = "Logged In: Staff User";
            // 
            // txtJobNumber
            // 
            this.txtJobNumber.Location = new System.Drawing.Point(123, 39);
            this.txtJobNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtJobNumber.Name = "txtJobNumber";
            this.txtJobNumber.ReadOnly = true;
            this.txtJobNumber.Size = new System.Drawing.Size(100, 22);
            this.txtJobNumber.TabIndex = 0;
            // 
            // lblJobNumber
            // 
            this.lblJobNumber.AutoSize = true;
            this.lblJobNumber.Location = new System.Drawing.Point(28, 39);
            this.lblJobNumber.Name = "lblJobNumber";
            this.lblJobNumber.Size = new System.Drawing.Size(89, 17);
            this.lblJobNumber.TabIndex = 30;
            this.lblJobNumber.Text = "Job Number:";
            // 
            // txtSurveyCompletedBy
            // 
            this.txtSurveyCompletedBy.Location = new System.Drawing.Point(875, 263);
            this.txtSurveyCompletedBy.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSurveyCompletedBy.Name = "txtSurveyCompletedBy";
            this.txtSurveyCompletedBy.Size = new System.Drawing.Size(165, 22);
            this.txtSurveyCompletedBy.TabIndex = 0;
            this.txtSurveyCompletedBy.Leave += new System.EventHandler(this.txtSurveyCompletedBy_Leave);
            // 
            // lblJSurveyCompletedBy
            // 
            this.lblJSurveyCompletedBy.AutoSize = true;
            this.lblJSurveyCompletedBy.Location = new System.Drawing.Point(725, 266);
            this.lblJSurveyCompletedBy.Name = "lblJSurveyCompletedBy";
            this.lblJSurveyCompletedBy.Size = new System.Drawing.Size(147, 17);
            this.lblJSurveyCompletedBy.TabIndex = 41;
            this.lblJSurveyCompletedBy.Text = "Survey Completed By:";
            // 
            // txtRepairedBy
            // 
            this.txtRepairedBy.Location = new System.Drawing.Point(875, 302);
            this.txtRepairedBy.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtRepairedBy.Name = "txtRepairedBy";
            this.txtRepairedBy.Size = new System.Drawing.Size(165, 22);
            this.txtRepairedBy.TabIndex = 1;
            this.txtRepairedBy.Leave += new System.EventHandler(this.txtRepairedBy_Leave);
            // 
            // lblRepairedBy
            // 
            this.lblRepairedBy.AutoSize = true;
            this.lblRepairedBy.Location = new System.Drawing.Point(779, 305);
            this.lblRepairedBy.Name = "lblRepairedBy";
            this.lblRepairedBy.Size = new System.Drawing.Size(90, 17);
            this.lblRepairedBy.TabIndex = 53;
            this.lblRepairedBy.Text = "Repaired By:";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // lblFaultType
            // 
            this.lblFaultType.AutoSize = true;
            this.lblFaultType.Location = new System.Drawing.Point(184, 33);
            this.lblFaultType.Name = "lblFaultType";
            this.lblFaultType.Size = new System.Drawing.Size(79, 17);
            this.lblFaultType.TabIndex = 19;
            this.lblFaultType.Text = "Fault Type:";
            // 
            // lblFaildPart
            // 
            this.lblFaildPart.AutoSize = true;
            this.lblFaildPart.Location = new System.Drawing.Point(626, 33);
            this.lblFaildPart.Name = "lblFaildPart";
            this.lblFaildPart.Size = new System.Drawing.Size(72, 17);
            this.lblFaildPart.TabIndex = 20;
            this.lblFaildPart.Text = "Faild Part:";
            // 
            // txtRepairNotes
            // 
            this.txtRepairNotes.Location = new System.Drawing.Point(15, 91);
            this.txtRepairNotes.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtRepairNotes.Name = "txtRepairNotes";
            this.txtRepairNotes.Size = new System.Drawing.Size(1015, 72);
            this.txtRepairNotes.TabIndex = 2;
            this.txtRepairNotes.Text = "";
            // 
            // lblRepairNotes
            // 
            this.lblRepairNotes.AutoSize = true;
            this.lblRepairNotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRepairNotes.Location = new System.Drawing.Point(12, 12);
            this.lblRepairNotes.Name = "lblRepairNotes";
            this.lblRepairNotes.Size = new System.Drawing.Size(152, 18);
            this.lblRepairNotes.TabIndex = 22;
            this.lblRepairNotes.Text = "Repair Information:";
            this.lblRepairNotes.Click += new System.EventHandler(this.lblRepairNotes_Click);
            // 
            // cmbFaultType
            // 
            this.cmbFaultType.FormattingEnabled = true;
            this.cmbFaultType.Items.AddRange(new object[] {
            "Suspension",
            "Brakes",
            "Cooling",
            "Fuel",
            "Ignition",
            "Steering",
            "Transmission",
            "Electronics",
            "Lights",
            "Exhaust"});
            this.cmbFaultType.Location = new System.Drawing.Point(290, 30);
            this.cmbFaultType.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbFaultType.Name = "cmbFaultType";
            this.cmbFaultType.Size = new System.Drawing.Size(291, 24);
            this.cmbFaultType.TabIndex = 0;
            this.cmbFaultType.Text = "Select";
            // 
            // cmbFaildPart
            // 
            this.cmbFaildPart.FormattingEnabled = true;
            this.cmbFaildPart.Items.AddRange(new object[] {
            "Disc Brake Front",
            "Disc Brake Rear",
            "Water Pump",
            "Spark Plug",
            "ABS Sensor",
            "Alternator",
            "Radiator",
            "Master Cylinder",
            "Shock Absorber Front",
            "Shock Absorber Rear",
            "Springs Front",
            "Springs Rear",
            "Air Filter",
            "Glow Plug",
            "Fan Belt"});
            this.cmbFaildPart.Location = new System.Drawing.Point(715, 32);
            this.cmbFaildPart.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbFaildPart.Name = "cmbFaildPart";
            this.cmbFaildPart.Size = new System.Drawing.Size(291, 24);
            this.cmbFaildPart.TabIndex = 1;
            this.cmbFaildPart.Text = "Select";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnRefresh);
            this.panel1.Controls.Add(this.btnDeletePart);
            this.panel1.Controls.Add(this.btnAddPart);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblPrice);
            this.panel1.Controls.Add(this.lblBrand);
            this.panel1.Controls.Add(this.lblQuantity);
            this.panel1.Controls.Add(this.lblPartDescription);
            this.panel1.Controls.Add(this.lblPartNumber);
            this.panel1.Controls.Add(this.txtPrice);
            this.panel1.Controls.Add(this.txtBrand);
            this.panel1.Controls.Add(this.txtQuantity);
            this.panel1.Controls.Add(this.txtPartDescription);
            this.panel1.Controls.Add(this.txtPartNumber);
            this.panel1.Controls.Add(this.lblRemarks);
            this.panel1.Controls.Add(this.dgPartList);
            this.panel1.Controls.Add(this.lblPartsList);
            this.panel1.Controls.Add(this.lblRepairNotes);
            this.panel1.Controls.Add(this.lblFaildPart);
            this.panel1.Controls.Add(this.lblFaultType);
            this.panel1.Controls.Add(this.txtRepairNotes);
            this.panel1.Controls.Add(this.cmbFaildPart);
            this.panel1.Controls.Add(this.cmbFaultType);
            this.panel1.Location = new System.Drawing.Point(16, 353);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1104, 555);
            this.panel1.TabIndex = 35;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Image = global::GUI.Properties.Resources.Refresh_icon;
            this.btnRefresh.Location = new System.Drawing.Point(880, 234);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 42);
            this.btnRefresh.TabIndex = 103;
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click_1);
            // 
            // btnDeletePart
            // 
            this.btnDeletePart.Image = global::GUI.Properties.Resources.Delete_icon;
            this.btnDeletePart.Location = new System.Drawing.Point(774, 234);
            this.btnDeletePart.Name = "btnDeletePart";
            this.btnDeletePart.Size = new System.Drawing.Size(75, 42);
            this.btnDeletePart.TabIndex = 102;
            this.btnDeletePart.UseVisualStyleBackColor = true;
            this.btnDeletePart.Click += new System.EventHandler(this.btnDeletePart_Click_1);
            // 
            // btnAddPart
            // 
            this.btnAddPart.Image = global::GUI.Properties.Resources.add;
            this.btnAddPart.Location = new System.Drawing.Point(666, 234);
            this.btnAddPart.Name = "btnAddPart";
            this.btnAddPart.Size = new System.Drawing.Size(75, 42);
            this.btnAddPart.TabIndex = 0;
            this.btnAddPart.UseVisualStyleBackColor = true;
            this.btnAddPart.Click += new System.EventHandler(this.btnAddPart_Click_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 172);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 18);
            this.label1.TabIndex = 100;
            this.label1.Text = "Update Parts:";
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Location = new System.Drawing.Point(318, 285);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(44, 17);
            this.lblPrice.TabIndex = 99;
            this.lblPrice.Text = "Price:";
            // 
            // lblBrand
            // 
            this.lblBrand.AutoSize = true;
            this.lblBrand.Location = new System.Drawing.Point(22, 285);
            this.lblBrand.Name = "lblBrand";
            this.lblBrand.Size = new System.Drawing.Size(50, 17);
            this.lblBrand.TabIndex = 98;
            this.lblBrand.Text = "Brand:";
            // 
            // lblQuantity
            // 
            this.lblQuantity.AutoSize = true;
            this.lblQuantity.Location = new System.Drawing.Point(300, 205);
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Size = new System.Drawing.Size(65, 17);
            this.lblQuantity.TabIndex = 97;
            this.lblQuantity.Text = "Quantity:";
            // 
            // lblPartDescription
            // 
            this.lblPartDescription.AutoSize = true;
            this.lblPartDescription.Location = new System.Drawing.Point(21, 247);
            this.lblPartDescription.Name = "lblPartDescription";
            this.lblPartDescription.Size = new System.Drawing.Size(109, 17);
            this.lblPartDescription.TabIndex = 96;
            this.lblPartDescription.Text = "PartDescription:";
            // 
            // lblPartNumber
            // 
            this.lblPartNumber.AutoSize = true;
            this.lblPartNumber.Location = new System.Drawing.Point(20, 205);
            this.lblPartNumber.Name = "lblPartNumber";
            this.lblPartNumber.Size = new System.Drawing.Size(88, 17);
            this.lblPartNumber.TabIndex = 95;
            this.lblPartNumber.Text = "PartNumber:";
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new System.Drawing.Point(371, 282);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(151, 22);
            this.txtPrice.TabIndex = 7;
            // 
            // txtBrand
            // 
            this.txtBrand.Location = new System.Drawing.Point(78, 280);
            this.txtBrand.Name = "txtBrand";
            this.txtBrand.Size = new System.Drawing.Size(151, 22);
            this.txtBrand.TabIndex = 6;
            // 
            // txtQuantity
            // 
            this.txtQuantity.Location = new System.Drawing.Point(371, 202);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new System.Drawing.Size(151, 22);
            this.txtQuantity.TabIndex = 4;
            // 
            // txtPartDescription
            // 
            this.txtPartDescription.Location = new System.Drawing.Point(135, 244);
            this.txtPartDescription.Name = "txtPartDescription";
            this.txtPartDescription.Size = new System.Drawing.Size(387, 22);
            this.txtPartDescription.TabIndex = 5;
            // 
            // txtPartNumber
            // 
            this.txtPartNumber.Location = new System.Drawing.Point(115, 202);
            this.txtPartNumber.Name = "txtPartNumber";
            this.txtPartNumber.Size = new System.Drawing.Size(151, 22);
            this.txtPartNumber.TabIndex = 3;
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(16, 72);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(68, 17);
            this.lblRemarks.TabIndex = 85;
            this.lblRemarks.Text = "Remarks:";
            this.lblRemarks.Click += new System.EventHandler(this.label1_Click);
            // 
            // dgPartList
            // 
            this.dgPartList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPartList.Location = new System.Drawing.Point(15, 350);
            this.dgPartList.Name = "dgPartList";
            this.dgPartList.RowTemplate.Height = 24;
            this.dgPartList.Size = new System.Drawing.Size(1071, 193);
            this.dgPartList.TabIndex = 27;
            // 
            // lblPartsList
            // 
            this.lblPartsList.AutoSize = true;
            this.lblPartsList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPartsList.Location = new System.Drawing.Point(12, 323);
            this.lblPartsList.Name = "lblPartsList";
            this.lblPartsList.Size = new System.Drawing.Size(85, 18);
            this.lblPartsList.TabIndex = 24;
            this.lblPartsList.Text = "Parts List:";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(449, 912);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 42);
            this.btnSearch.TabIndex = 0;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.Location = new System.Drawing.Point(580, 912);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 42);
            this.btnUpdate.TabIndex = 1;
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // StaffUpdateJobDetailsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1143, 1055);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.txtRepairedBy);
            this.Controls.Add(this.lblRepairedBy);
            this.Controls.Add(this.txtSurveyCompletedBy);
            this.Controls.Add(this.lblJSurveyCompletedBy);
            this.Controls.Add(this.lblCurrentStatusTitle);
            this.Controls.Add(this.lblDetailsTitle);
            this.Controls.Add(this.lblLoggedAs);
            this.Controls.Add(this.txtJobNumber);
            this.Controls.Add(this.lblJobNumber);
            this.Controls.Add(this.pnlCurrentStatus);
            this.Controls.Add(this.pnlDetails);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "StaffUpdateJobDetailsForm";
            this.Text = "Update Job Details";
            this.Load += new System.EventHandler(this.StaffUpdateJobDetailsForm_Load);
            this.pnlCurrentStatus.ResumeLayout(false);
            this.pnlCurrentStatus.PerformLayout();
            this.pnlDetails.ResumeLayout(false);
            this.pnlDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPartList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.Label lblCurrentStatusTitle;
        private System.Windows.Forms.ComboBox cmbCurrentStatus;
        private System.Windows.Forms.Panel pnlCurrentStatus;
        private System.Windows.Forms.Label lblDetailsTitle;
        private System.Windows.Forms.Panel pnlDetails;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.TextBox txtSurname;
        private System.Windows.Forms.Label lblSurname;
        private System.Windows.Forms.TextBox txtNumberPlate;
        private System.Windows.Forms.Label lblNumberPlate;
        private System.Windows.Forms.TextBox txtVehicleModel;
        private System.Windows.Forms.Label lblVehicleModel;
        private System.Windows.Forms.TextBox txtContact;
        private System.Windows.Forms.Label lblContact;
        private System.Windows.Forms.TextBox txtIdCard;
        private System.Windows.Forms.Label lblIDCard;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblLoggedAs;
        private System.Windows.Forms.TextBox txtJobNumber;
        private System.Windows.Forms.Label lblJobNumber;
        private System.Windows.Forms.TextBox txtSurveyCompletedBy;
        private System.Windows.Forms.Label lblJSurveyCompletedBy;
        private System.Windows.Forms.TextBox txtRepairedBy;
        private System.Windows.Forms.Label lblRepairedBy;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cmbFaildPart;
        private System.Windows.Forms.ComboBox cmbFaultType;
        private System.Windows.Forms.Label lblRepairNotes;
        private System.Windows.Forms.RichTextBox txtRepairNotes;
        private System.Windows.Forms.Label lblFaildPart;
        private System.Windows.Forms.Label lblFaultType;
        private System.Windows.Forms.DataGridView dgPartList;
        private System.Windows.Forms.Label lblRemarks;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnDeletePart;
        private System.Windows.Forms.Button btnAddPart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.Label lblBrand;
        private System.Windows.Forms.Label lblQuantity;
        private System.Windows.Forms.Label lblPartDescription;
        private System.Windows.Forms.Label lblPartNumber;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.TextBox txtBrand;
        private System.Windows.Forms.TextBox txtQuantity;
        private System.Windows.Forms.TextBox txtPartDescription;
        private System.Windows.Forms.TextBox txtPartNumber;
        private System.Windows.Forms.Label lblPartsList;
        private System.Windows.Forms.TextBox txtCurrentStatusD;
    }
}