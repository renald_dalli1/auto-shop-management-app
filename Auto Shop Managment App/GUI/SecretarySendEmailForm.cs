﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Mail;
namespace GUI
{
    public partial class SecretarySendEmailForm : Form
    {
        //declaring NetworkCredential, SmtpClient and Mail Message
        NetworkCredential login;
        SmtpClient client;
        MailMessage msg;

        public SecretarySendEmailForm()
        {
            InitializeComponent();
        }

        private void SecretarySendEmailForm_Load(object sender, EventArgs e)
        {

        }

        private void btnSend_Click_1(object sender, EventArgs e)
        {
            //On Button Send Click the email with details will be send to entered recepient by using the email address configured 

            //Inserting Username and password
            login = new NetworkCredential(txtUsername.Text, txtPassword.Text);
            client = new SmtpClient(txtSmtp.Text);
            client.Port = Convert.ToInt32(txtPort.Text);
            client.EnableSsl = chkSSL.Checked;
            client.Credentials = login;
            // New Mail message configuration
            msg = new MailMessage { From = new MailAddress(txtUsername.Text + txtSmtp.Text.Replace("smtp.", "@"), "Auto Shop Managment App", Encoding.UTF8) };
            //Add New mail message
            msg.To.Add(new MailAddress(txtTo.Text));
            if (!string.IsNullOrEmpty(txtCC.Text))
                msg.To.Add(new MailAddress(txtCC.Text));
            //Components of email configured
            msg.Subject = txtSubject.Text;
            msg.Body = txtMessage.Text;
            msg.BodyEncoding = Encoding.UTF8;
            msg.IsBodyHtml = true;
            msg.Priority = MailPriority.Normal;
            // call back if sending email fails
            msg.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            client.SendCompleted += new SendCompletedEventHandler(SendCompleteCallback);
            string userstate = "Sending...";
            client.SendAsync(msg, userstate);
        }

        private static void SendCompleteCallback(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Cancelled)
                //messege box showing user sending email cancelled
                MessageBox.Show(string.Format("{0} send cancelled.", e.UserState), "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            if (e.Error != null)
                //messege box showing user an Error was encountered
                MessageBox.Show(string.Format("{0} {1}", e.UserState, e.Error), "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
                //messege box showing user email was sent 
                MessageBox.Show("Your Message Has Been Sent!", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void txtSmtp_TextChanged(object sender, EventArgs e)
        {

        }

    }
    }


    

