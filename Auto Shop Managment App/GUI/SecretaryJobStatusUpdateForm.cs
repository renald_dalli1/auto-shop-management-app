﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace GUI
{
    public partial class SecretaryJobStatusUpdateForm : Form
    {
        public SecretaryJobStatusUpdateForm()
        {
            InitializeComponent();
        }

        private void SecretaryJobStatusUpdateForm_Load(object sender, EventArgs e)
        {
            //on combobox cmbCurrentStatus, the Current Status wull be showb in the drop down and the value will be its StatusID. Only CurrentStatus with Job will be shown
            this.cmbCurrentStatus.DisplayMember = "CurrentStatus";
            this.cmbCurrentStatus.ValueMember = "StatusID";
            this.cmbCurrentStatus.DataSource = AutoShopService.GetAllJobStatus_Job();
            this.cmbCurrentStatus.Text = "Please Select";
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            //On Search click button a messege box asking for job number will be shown. if value entered matches JobNumber in database, the details will be shown
            string userInput = Microsoft.VisualBasic.Interaction.InputBox("Please Enter Job Number", "Database Search");

            // check user input is is null
            if (userInput != "")
            {
                int search = int.Parse(userInput);

                DataTable dt = AutoShopService.GetJobDetails(search);


                if (Convert.ToInt32(dt.Rows.Count.ToString()) > 0)
                {
                    this.txtJobNumber.Text = dt.Rows[0]["JobNumber"].ToString();
                    this.txtName.Text = dt.Rows[0]["Name"].ToString();
                    this.txtSurname.Text = dt.Rows[0]["Surname"].ToString();
                    this.txtIdCard.Text = dt.Rows[0]["IdCard"].ToString();
                    this.txtContact.Text = dt.Rows[0]["Contact"].ToString();
                    this.txtEmail.Text = dt.Rows[0]["Email"].ToString();
                    this.txtAddress.Text = dt.Rows[0]["Address"].ToString();
                    this.txtVehicleModel.Text = dt.Rows[0]["VehicleModel"].ToString();
                    this.txtNumberPlate.Text = dt.Rows[0]["NumberPlate"].ToString();
                    this.txtJobCreatedBy.Text = dt.Rows[0]["JobCreatedBy"].ToString();
                    this.cmbCurrentStatus.Text = dt.Rows[0]["StatusIDFK"].ToString();
                    this.txtCurrentStatusD.Text = dt.Rows[0]["CurrentStatus"].ToString();


                }
                else
                {
                    AutoShopService.CloseConnection();

                }
            }


         else
         {
                MessageBox.Show("Job Number is required to find Job", "Job Search Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

         }


        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {

            //Validation if update button is clicked and important textboxex are left null
            if (txtCurrentStatusD.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Please Search Record or Update Status");
                return;
            }
            try
            {
                //on Update Button click CurrentStatus values from the components will be updated into database
                bool updateSuccess = AutoShopService.updateCurrentStatus(int.Parse(this.txtJobNumber.Text), cmbCurrentStatus.SelectedIndex + 11);
                MessageBox.Show("Job Status Updated Successfully", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (SqlException ex)
            {

                MessageBox.Show("Error Encountered whilst updating Job Status", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw ex;
            }
        }
    }
}