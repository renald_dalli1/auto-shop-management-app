﻿namespace GUI
{
    partial class SecretaryJobListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExport = new System.Windows.Forms.Button();
            this.lblLoggedAs = new System.Windows.Forms.Label();
            this.dgJobList = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgJobList)).BeginInit();
            this.SuspendLayout();
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(38, 36);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(266, 58);
            this.btnExport.TabIndex = 55;
            this.btnExport.Text = "Export to Excel";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // lblLoggedAs
            // 
            this.lblLoggedAs.AutoSize = true;
            this.lblLoggedAs.Location = new System.Drawing.Point(1700, 77);
            this.lblLoggedAs.Name = "lblLoggedAs";
            this.lblLoggedAs.Size = new System.Drawing.Size(174, 17);
            this.lblLoggedAs.TabIndex = 63;
            this.lblLoggedAs.Text = "Logged In: Secretary User";
            // 
            // dgJobList
            // 
            this.dgJobList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgJobList.Location = new System.Drawing.Point(38, 110);
            this.dgJobList.Name = "dgJobList";
            this.dgJobList.RowTemplate.Height = 24;
            this.dgJobList.Size = new System.Drawing.Size(1855, 860);
            this.dgJobList.TabIndex = 53;
            // 
            // SecretaryJobListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1932, 994);
            this.Controls.Add(this.dgJobList);
            this.Controls.Add(this.lblLoggedAs);
            this.Controls.Add(this.btnExport);
            this.Name = "SecretaryJobListForm";
            this.Text = "Job List";
            this.Load += new System.EventHandler(this.SecretaryJobListForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgJobList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Label lblLoggedAs;
        private System.Windows.Forms.DataGridView dgJobList;
    }
}