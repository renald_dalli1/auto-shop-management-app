﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Sql;
using System.Data;
using System.Data.SqlClient;

namespace GUI
{
    class AutoShopService
    {
        
        #region SqlConnectionCode

        //connection string connecting to SQL Database
        private static string _connStr = @"Data Source=RENSMSI\SQLEXPRESS;Initial Catalog=AutoShopManagementApplication;Integrated Security=True";


        //declare sql connection
        private static SqlConnection _conn = null;

        /// <summary>
        /// Method to open the connection of the database
        /// </summary>
        /// <returns></returns>
        public static bool OpenConnection()
        {
            //Checking if SqlConnection is null
            if (_conn == null)

                //If SqlConnection is null, the connection string is passed as a parameter to connect to the database
                _conn = new SqlConnection(_connStr);

            //Cheking the State of the connection if it's state is not Opened
            if (_conn.State != ConnectionState.Open)
            {
                try
                {
                    //open connection
                    _conn.Open();
                }
                catch (SqlException ex)
                {
                    //Throwing an exception error if the connection with the database is lost
                    ex.ToString();
                }
            }

            //changing the connection state to Open
            return (_conn.State == ConnectionState.Open);
        }
    
        /// <summary>
        /// Method to close the connection of the database
        /// </summary>
        public static void CloseConnection()
        {
            //Checking if SqlConnection is null
            if (_conn != null)
            {
                //Checking the current connection state if it is set to Open 
                if (_conn.State == ConnectionState.Open)
                {
                    //If database connection state is set to Open, connection is closed 
                    _conn.Close();
                }
            }
        }

        /// <summary>
        /// Method to get data from database according to sql string
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static DataTable GetData(string sql)
        {
            //Creating an instance of a Datatable 
            DataTable dt = new DataTable();

            //establishing connection with the database
            if (OpenConnection())
            {
                //Creating an instance of SqlDataAdapter passing an SQL Command String
                SqlDataAdapter da = new SqlDataAdapter(new SqlCommand(sql, _conn));

                try
                {
                    //Adding SQL query record result retrieved from the SqlDataAdapter into the Datatable
                    da.Fill(dt);
                }
                catch
                {
                    //Voiding the data table
                    dt = null;
                }

                //Calling the CloseConnection method to Close the database connection
                CloseConnection();
            }

            //return the Datatable with records
            return dt;
        }

        /// <summary>
        /// Method to get data from database according to sql command
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        public static DataTable GetData(SqlCommand cmd)
        {
            //Creating an instance of a Datatable
            DataTable dt = new DataTable();

            //establishing connection with the database
            if (OpenConnection())
            {
                //Passing the connection string as part of the SqlCommand
                cmd.Connection = _conn;

                //Creating an instance of MySqlDataAdapter passing an SqlCommand String
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                try
                {
                    //Adding SQL query record result retrieved from the SqlDataAdapter into the Datatable
                    da.Fill(dt);
                }
                catch (SqlException ex)
                {
                    //Voiding the data table
                    dt = null;
                    throw ex;
                }

                //Calling the CloseConnection method to Close the database connection
                CloseConnection();
            }

            //return the Datatable with retrieved Records
            return dt;
        }

        

        /// <summary>
        /// This method gets one column and one row of data according to sql string
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static object GetOneColumn(string sql)
        {
            object obj = null;
            if (OpenConnection())
            {
                SqlCommand cmd = new SqlCommand(sql, _conn);

                try
                {
                    obj = cmd.ExecuteScalar();
                }
                catch { }

                CloseConnection();
            }

            return obj;
        }

        /// <summary>
        /// This method gets one column and one row of data according to sql command
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        public static object GetOneColumn(SqlCommand cmd)
        {
            object obj = null;
            if (OpenConnection())
            {
                cmd.Connection = _conn;

                try
                {
                    obj = cmd.ExecuteScalar();
                }
                catch { }

                CloseConnection();
            }
         return obj;
        }
        /// <summary>
        /// This method ExecuteNonQuery the sql command for any insert/update or deletion of records from the table.
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        private static bool ExecuteNonQuery(SqlCommand cmd)
        {
            //instance of a boolean variable with an initial False status 
            bool success = false;

            //establishing connection with the database
            if (OpenConnection())
            {
                try
                {
                    //Assigning the SQL Query to the database connection string 
                    cmd.Connection = _conn;
                    //Executes an SQL statement against the connection and returns the number of rows affected.
                    cmd.ExecuteNonQuery();
                    //Assigning the boolean variable to True State
                    success = true;
                }
                catch (SqlException ex)
                {
                    success = false;
                    throw ex;
                }

                //Calling the Close Connection method to Close the database connection
                CloseConnection();
            }

            //return
            return success;
        }

        #endregion

        #region SQL INSERT Statements
        // ADDJob Statment when JobDetails are added to the database

        //Declaring Parameters 
        public static bool AddJob(string p_Name, string p_Surname, string p_IdCard, int p_Contact, string p_Email, string p_Address, string p_VehicleModel, string p_NumberPlate, string p_JobCreatedBy, DateTime p_DateCreated, int p_StatusIDFK) {

        //sql statment declaring values of parameters to be inserted in JobDetails Table
        string sql = "insert into JobDetailsTable" + "(Name, Surname, IdCard, Contact, Email, Address, VehicleModel, NumberPlate, JobCreatedBy, DateCreated, StatusIDFK)" + "values(@Name, @Surname, @IdCard, @Contact, @Email, @Address, @VehicleModel, @NumberPlate, @JobCreatedBy, @DateCreated, @StatusIDFK)";

        //Using sql command to add  new SqlParameters
            SqlCommand cmd = new SqlCommand(sql, _conn);

            cmd.Parameters.Add(new SqlParameter("@Name",p_Name));
            cmd.Parameters.Add(new SqlParameter("@Surname", p_Surname));
            cmd.Parameters.Add(new SqlParameter("@IdCard", p_IdCard));
            cmd.Parameters.Add(new SqlParameter("@Contact", p_Contact));
            cmd.Parameters.Add(new SqlParameter("@Email", p_Email));
            cmd.Parameters.Add(new SqlParameter("@Address", p_Address));
            cmd.Parameters.Add(new SqlParameter("@VehicleModel", p_VehicleModel));
            cmd.Parameters.Add(new SqlParameter("@NumberPlate", p_NumberPlate));
            cmd.Parameters.Add(new SqlParameter("@JobCreatedBy", p_JobCreatedBy));
            cmd.Parameters.Add(new SqlParameter("@DateCreated", p_DateCreated));
            cmd.Parameters.Add(new SqlParameter("@StatusIDFK", p_StatusIDFK));

            return ExecuteNonQuery(cmd);
         }
    
        // ADDSurvey Statment when new SurveyDetails are added to the database
        public static bool AddSurvey(DateTime p_SurveyUpdateDate, string p_FaultType, string p_FaildPart, string p_SurveyNotes, string p_SurveyCompletedBy, int p_JobNumberFK)
        {

            string sql = "insert into SurveyTable" + "(SurveyUpdateDate, FaultType, FaildPart, SurveyNotes, SurveyCompletedBy, JobNumberFK)" + "values(@SurveyUpdateDate, @FaultType, @FaildPart, @SurveyNotes, @SurveyCompletedBy, @JobNumberFK)";

            SqlCommand cmd = new SqlCommand(sql, _conn);

            cmd.Parameters.Add(new SqlParameter("@SurveyUpdateDate", p_SurveyUpdateDate));
            cmd.Parameters.Add(new SqlParameter("@FaultType", p_FaultType));
            cmd.Parameters.Add(new SqlParameter("@FaildPart", p_FaildPart));
            cmd.Parameters.Add(new SqlParameter("@SurveyNotes", p_SurveyNotes));
            cmd.Parameters.Add(new SqlParameter("@SurveyCompletedBy", p_SurveyCompletedBy));
            cmd.Parameters.Add(new SqlParameter("@JobNumberFK", p_JobNumberFK));
   

            return ExecuteNonQuery(cmd);

        }

        // ADDPartsandRepair Statment when new PartsandRepairDetails are added to the database
        public static bool AddPartsandRepair(DateTime p_PartsAndRepairUpdateDate, string p_PartsAndRepairNotes, string p_PartsAndRepairRepairedBy, int p_JobNumberFK)
        {

            string sql = "insert into PartsAndRepairTable" + "(PartsAndRepairUpdateDate, PartsAndRepairNotes, PartsAndRepairRepairedBy, JobNumberFK)" + "values(@PartsAndRepairUpdateDate, @PartsAndRepairNotes, @PartsAndRepairRepairedBy, @JobNumberFK)";

            SqlCommand cmd = new SqlCommand(sql, _conn);

            cmd.Parameters.Add(new SqlParameter("@PartsAndRepairUpdateDate", p_PartsAndRepairUpdateDate));
            cmd.Parameters.Add(new SqlParameter("@PartsAndRepairNotes", p_PartsAndRepairNotes));
            cmd.Parameters.Add(new SqlParameter("@PartsAndRepairRepairedBy", p_PartsAndRepairRepairedBy));
            cmd.Parameters.Add(new SqlParameter("@JobNumberFK", p_JobNumberFK));

            return ExecuteNonQuery(cmd);

        }

        // ADDPartsList Statment when new PartsListDetails are added to the database
        public static bool AddPartsList( string p_PartNumber, string p_PartDescription, string p_Brand, int p_Quantity, decimal p_Price, int p_JobNumberFK)
        {
           
            {

                string sql = "insert into PartsListTable" + "(PartNumber, PartDescription, Brand, Quantity, Price, JobNumberFK)" + "values(@PartNumber, @PartDescription, @Brand, @Quantity, @Price, @JobNumberFK)";

                SqlCommand cmd = new SqlCommand(sql, _conn);

                cmd.Parameters.Add(new SqlParameter("@PartNumber", p_PartNumber));
                cmd.Parameters.Add(new SqlParameter("@PartDescription", p_PartDescription));
                cmd.Parameters.Add(new SqlParameter("@Brand", p_Brand));
                cmd.Parameters.Add(new SqlParameter("@Quantity", p_Quantity));
                cmd.Parameters.Add(new SqlParameter("@Price", p_Price));
                cmd.Parameters.Add(new SqlParameter("@JobNumberFK", p_JobNumberFK));

                return ExecuteNonQuery(cmd);

            }
        }

        // ADDPRepairReport Statment when new RepairReportDetails are added to the database
        public static bool AddRepairReportDetails(string p_IssuedBy, DateTime p_DateIssued, int p_JobNumberFK)
        {

            {

                string sql = "insert into RepairReportTable" + "(IssuedBy, DateIssued, JobNumberFK)" + "values(@IssuedBy, @DateIssued, @JobNumberFK)";

                SqlCommand cmd = new SqlCommand(sql, _conn);

                cmd.Parameters.Add(new SqlParameter("@IssuedBy", p_IssuedBy));
                cmd.Parameters.Add(new SqlParameter("@DateIssued", p_DateIssued));
              
                cmd.Parameters.Add(new SqlParameter("@JobNumberFK", p_JobNumberFK));

                return ExecuteNonQuery(cmd);

            }
        }


        #endregion

        #region SQL SELECT Statements
        //  Get Details from JobDetailsTable Statment with specified columns
        public static DataTable GetJobDetails(int jobNumberSearch) {
           
            //string sql = "SELECT JobNumber, Name, Surname, IdCard, Contact, Email, Address, VehicleModel, NumberPlate, JobCreatedBy, DateCreated, StatusIDFK  FROM JobDetailsTable WHERE JobNumber='" + jobNumberSearch + "'";
            string sql = "SELECT JobNumber, Name, Surname, IdCard, Contact, Email, Address, VehicleModel, NumberPlate, JobCreatedBy, DateCreated, StatusIDFK, CurrentStatus  FROM JobDetailsTable inner join CurrentStatusTable on StatusIDFK = StatusID WHERE JobNumber = '" + jobNumberSearch + "'";

            return GetData(sql);
        }

        // Select and Get Details from SurveyTable Statment 
        public static DataTable GetSurveyDetails(int jobNumberFKSearch){

            string sql = "SELECT SurveyNotes, FaultType, FaildPart, SurveyCompletedBy FROM SurveyTable WHERE JobNumberFK='" + jobNumberFKSearch + "'";

            return GetData(sql);

        }

        // Select and Get Details from PartsAndRepairTable Statment 
        public static DataTable GetPartsAndRepairDetails(int jobNumberFKSearch)
        {

            string sql = "SELECT PartsAndRepairNotes, PartsAndRepairRepairedBy From PartsAndRepairTable WHERE JobNumberFK='" + jobNumberFKSearch + "'";

            return GetData(sql);

        }
        
    

        // Select and Get Details from PartsListTable Statment 
        public static DataTable GetPartsListDetails(int jobNumberFKSearch)
        {

            string sql = "SELECT PartNumber, PartDescription, Brand, Quantity, Price From PartsListTable WHERE JobNumberFK='" + jobNumberFKSearch + "'";

            return GetData(sql);
        }

        // Select and Get Details statment to view in Datagrid view from PartsListTable
        public static DataTable GetPartsItemsToView(int jobNumberFKSearch)
        {

            string sql = "SELECT PartsListID, PartNumber, PartDescription, Brand, Quantity, Price  From PartsListTable WHERE JobNumberFK='" + jobNumberFKSearch + "'";

            return GetData(sql);         

        }

        // Select and Get ALL Details from PartListTable Statment 
        public static DataTable GetJobsToView()
        {
            

                string sql = "SELECT * FROM JobDetailsTable";
           
            return GetData(sql);

        }

        // Select and Get Details from RepairReportTable Statment 
        public static DataTable GetRepairReportDetails(int jobNumberFKSearch)
        {

            string sql = "SELECT IssuedBy, DateIssued FROM RepairReportTable WHERE JobNumberFK = '" + jobNumberFKSearch + "'";

            return GetData(sql);

        }

        // Select and Get Details from CurrentStatustTable Statment 
        public static DataTable GetAllJobStatus() {

            string sql = "SELECT StatusID, CurrentStatus FROM CurrentStatusTable";
            return GetData(sql);
        }

        // Select and Get Details from CurrentStatustTable Statment where CurrentStatus contains Survey
        public static DataTable GetAllJobStatus_Survey()
        {

            string sql = "SELECT StatusID, CurrentStatus FROM CurrentStatusTable WHERE CurrentStatus like 'Survey%'";
            return GetData(sql);
        }

        // Select and Get Details from CurrentStatustTable Statment where CurrentStatus contains Parts
        public static DataTable GetAllJobStatus_Parts()
        {

            string sql = "SELECT StatusID, CurrentStatus FROM CurrentStatusTable WHERE CurrentStatus like 'Parts%'";
            return GetData(sql);
        }

        // Select and Get Details from CurrentStatustTable Statment where CurrentStatus contains End
        public static DataTable GetAllJobStatus_EndJob()
        {

            string sql = "SELECT StatusID, CurrentStatus FROM CurrentStatusTable WHERE CurrentStatus like 'End%'";
            return GetData(sql);
        }

        // Select and Get Details from CurrentStatustTable Statment where CurrentStatus contains StartJob
        public static DataTable GetAllJobStatus_StartJob()
        {

            string sql = "SELECT StatusID, CurrentStatus FROM CurrentStatusTable WHERE CurrentStatus like 'StartJob%'";
            return GetData(sql);
        }

        // Select and Get Details from CurrentStatustTable Statment where CurrentStatus contains Job
        public static DataTable GetAllJobStatus_Job()
        {

            string sql = "SELECT StatusID, CurrentStatus FROM CurrentStatusTable WHERE CurrentStatus like 'Job%'";
            return GetData(sql);
        }

       
        #endregion



        #region SQL UPDATE Statements
        // Update CurrentStatusDetails inside JobDetailsTable Statment
        public static bool updateCurrentStatus(int p_jobNumber, int p_StatusIDFK) {

            string sql = "UPDATE JobDetailsTable SET StatusIDFK=@StatusIDFK WHERE JobNumber=@JobNumber";

            SqlCommand cmd = new SqlCommand(sql, _conn);
            cmd.Parameters.Add(new SqlParameter("@StatusIDFK", p_StatusIDFK));
            
            cmd.Parameters.Add(new SqlParameter("@JobNumber", p_jobNumber));

            return ExecuteNonQuery(cmd);

        }

        // Update JobDetails inside JobDetailsTable Statment
        public static bool UpdateJobDetails(string p_Name, string p_Surname, string p_IdCard, int p_Contact, string p_Email, string p_Address, string p_VehicleModel, string p_NumberPlate, int p_StatusIDFK, int p_jobNumber)
        {
            // the update statment for job dedatil table setting parameter name with value
            string sql = "UPDATE JobDetailsTable SET Name = @Name, Surname = @Surname, IdCard = @IdCard, Contact = @Contact, Email = @Email, Address = @Address, VehicleModel = @VehicleModel, NumberPlate = @NumberPlate, StatusIDFK = @StatusIDFK WHERE JobNumber=@JobNumber";
            // using sql command to add new sql parameters
            SqlCommand cmd = new SqlCommand(sql, _conn);
            cmd.Parameters.Add(new SqlParameter("@Name", p_Name));
            cmd.Parameters.Add(new SqlParameter("@Surname", p_Surname));
            cmd.Parameters.Add(new SqlParameter("@IdCard", p_IdCard));
            cmd.Parameters.Add(new SqlParameter("@Contact", p_Contact));
            cmd.Parameters.Add(new SqlParameter("@Email", p_Email));
            cmd.Parameters.Add(new SqlParameter("@Address", p_Address));
            cmd.Parameters.Add(new SqlParameter("@VehicleModel", p_VehicleModel));
            cmd.Parameters.Add(new SqlParameter("@NumberPlate", p_NumberPlate));
            cmd.Parameters.Add(new SqlParameter("@StatusIDFK", p_StatusIDFK));
            cmd.Parameters.Add(new SqlParameter("@JobNumber", p_jobNumber));

            return ExecuteNonQuery(cmd);

        }

        // Update SurveyDetails inside SurveyTable Statment
        public static bool UpdateSurveyDetails(string p_FaultType, string p_FaildPart, string p_SurveyCompletedBy, int p_JobNumberFK)
        {

            string sql = "UPDATE SurveyTable SET FaultType = @FaultType, FaildPart = @FaildPart, SurveyCompletedBy = @SurveyCompletedBy WHERE JobNumberFK=@JobNumberFK";

            SqlCommand cmd = new SqlCommand(sql, _conn);

            cmd.Parameters.Add(new SqlParameter("@FaultType", p_FaultType));
            cmd.Parameters.Add(new SqlParameter("@FaildPart", p_FaildPart));
            cmd.Parameters.Add(new SqlParameter("@SurveyCompletedBy", p_SurveyCompletedBy));
            cmd.Parameters.Add(new SqlParameter("@JobNumberFK", p_JobNumberFK));

            return ExecuteNonQuery(cmd);

        }

        // Update PartsAnRepairDetails inside PartsAnRepairTable Statment
        public static bool UpdatePartsandRepairDetails(string p_PartsAndRepairNotes, string p_PartsAndRepairRepairedBy, int p_JobNumberFK)
        {

            string sql = "Update PartsAndRepairTable SET PartsAndRepairNotes = @PartsAndRepairNotes, PartsAndRepairRepairedBy = @PartsAndRepairRepairedBy WHERE JobNumberFK = @JobNumberFK";

            SqlCommand cmd = new SqlCommand(sql, _conn);

           
            cmd.Parameters.Add(new SqlParameter("@PartsAndRepairNotes", p_PartsAndRepairNotes));
            cmd.Parameters.Add(new SqlParameter("@PartsAndRepairRepairedBy", p_PartsAndRepairRepairedBy));
            cmd.Parameters.Add(new SqlParameter("@JobNumberFK", p_JobNumberFK));

            return ExecuteNonQuery(cmd);

        }

        // Update PartsListDetails inside PartsListTable Statment
        public static bool UpdatePartsListDetails(string p_PartNumber, string p_PartDescription, string p_Brand, int p_Quantity, decimal p_Price, int p_JobNumberFK)
        {

            string sql = "UPDATE PartsListTable SET PartNumber = @PartNumbe, PartDescription = @PartDescription, Brand = @Brand, Quantity = @Quantity, Price = @Price wHERE JobNumberFK = @JobNumberFK";

            SqlCommand cmd = new SqlCommand(sql, _conn);

         
            cmd.Parameters.Add(new SqlParameter("@PartNumber", p_PartNumber));
            cmd.Parameters.Add(new SqlParameter("@PartDescription", p_PartDescription));
            cmd.Parameters.Add(new SqlParameter("@Brand", p_Brand));
            cmd.Parameters.Add(new SqlParameter("@Quantity", p_Quantity));
            cmd.Parameters.Add(new SqlParameter("@Price", p_Price));
            cmd.Parameters.Add(new SqlParameter("@JobNumberFK", p_JobNumberFK));

            return ExecuteNonQuery(cmd);
        }

        #endregion

        #region SQL Delete Statements

        // Delete SurveyDetails inside SurveyTable Statment
        public static bool DeleteSurveyDetails(int p_jobNumberFK)
        {

            string sql = "delete from SurveyTable where JobNumberFK=@JobNumberFK";

            SqlCommand cmd = new SqlCommand(sql, _conn);
            cmd.Parameters.Add(new SqlParameter("@JobNumberFK", p_jobNumberFK));


            return ExecuteNonQuery(cmd);

        }

        // Delete PartsAndRepairDetails inside PartsAndRepairTable Statment
        public static bool DeletePartsAndRepairDetails(int p_jobNumberFK)
        {

            string sql = "delete from PartsAndRepairTable where JobNumberFK=@JobNumberFK";

            SqlCommand cmd = new SqlCommand(sql, _conn);
            cmd.Parameters.Add(new SqlParameter("@JobNumberFK", p_jobNumberFK));


            return ExecuteNonQuery(cmd);

        }

        // Delete PartsListDetails inside PartsListTable Statment
        public static bool DeletePartsDetails(int p_jobNumberFK)
        {

            string sql = "delete from PartsListTable where JobNumberFK=@JobNumberFK";

            SqlCommand cmd = new SqlCommand(sql, _conn);
            cmd.Parameters.Add(new SqlParameter("@JobNumberFK", p_jobNumberFK));


            return ExecuteNonQuery(cmd);

        }

        // Delete JobDetails inside JobDetailsTable Statment by JobNumber
        public static bool DeleteJobDetails(int p_jobNumber)
            {

                string sql = "delete from JobDetailsTable where JobNumber=@JobNumber";

                SqlCommand cmd = new SqlCommand(sql, _conn);
                cmd.Parameters.Add(new SqlParameter("@JobNumber", p_jobNumber));


                return ExecuteNonQuery(cmd);
            }

        // Delete PartsListDetails inside PartsListTable Statment from StaffUpdateJobDetailsFORM
        public static bool DeletePart(int p_PartsListID)
        {

            string sql = "delete from PartsListTable where PartsListID=@PartsListID";

            SqlCommand cmd = new SqlCommand(sql, _conn);
            cmd.Parameters.Add(new SqlParameter("@PartsListID", p_PartsListID));


            return ExecuteNonQuery(cmd);

        }




        #endregion


        #region SLA Logic

        public static DataTable SLA()
        {
            //the Following SQl query is calculating the difference in date created and curremt date based on the Job status and the sla frequency. the output is shown in percentage
            string sql = "select JobNumber, Name, Surname, IdCard, Contact, Email, VehicleModel, NumberPlate,CurrentStatus,DateCreated, DATEDIFF(day,DateCreated,GETDATE()) as SLA_DAYS_PASSED, concat(DATEDIFF(day,DateCreated,GETDATE())/nullif(SlaFrequency,0)*100,'%') as Percentage, case when DATEDIFF(day,DateCreated,GETDATE()) > 0  and DATEDIFF(day,DateCreated,GETDATE()) < 2 then 'StartJob - Created and proceed to Survey' when DATEDIFF(day,DateCreated,GETDATE()) > 1  and DATEDIFF(day,DateCreated,GETDATE()) < 3 then 'Survey - Started' when DATEDIFF(day,DateCreated,GETDATE()) > 2  and DATEDIFF(day,DateCreated,GETDATE()) < 4 then 'Survey - Done Send Email' when DATEDIFF(day,DateCreated,GETDATE()) > 3  and DATEDIFF(day,DateCreated,GETDATE()) < 5 then 'Parts Gathering' when DATEDIFF(day,DateCreated,GETDATE()) > 4  and DATEDIFF(day,DateCreated,GETDATE()) < 6 then 'Parts Repairs - Started' when DATEDIFF(day,DateCreated,GETDATE()) > 5  and DATEDIFF(day,DateCreated,GETDATE()) < 7 then 'Parts Repairs -Testing' when DATEDIFF(day,DateCreated,GETDATE()) > 6  and DATEDIFF(day,DateCreated,GETDATE()) < 8 then 'Parts Repairs - Completed Send Email' when DATEDIFF(day,DateCreated,GETDATE()) > 7  and DATEDIFF(day,DateCreated,GETDATE()) < 9 then 'EndJob - Vehicle Ready' when DATEDIFF(day,DateCreated,GETDATE()) > 8  and DATEDIFF(day,DateCreated,GETDATE()) < 10 then 'Job - Email with quote sent' when DATEDIFF(day,DateCreated,GETDATE()) > 11  and DATEDIFF(day,DateCreated,GETDATE()) < 13 then 'Job - Proceed to Parts and Repairs' when DATEDIFF(day,DateCreated,GETDATE()) > 11  and DATEDIFF(day,DateCreated,GETDATE()) < 13 then 'Job - Repair completed Email for pickup sent' end as NEXT_SLA_STEP from JobDetailsTable inner join CurrentStatusTable on StatusIDFK = StatusID";

        return GetData(sql);

        }


        #endregion


    }
}
