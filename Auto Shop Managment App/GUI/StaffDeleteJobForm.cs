﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace GUI
{
    public partial class StaffDeleteJobForm : Form
    {
        public StaffDeleteJobForm()
        {
            InitializeComponent();
        }

        private void txtUsername_TextChanged(object sender, EventArgs e)
        {

        }

        private void StaffDeleteJobForm_Load(object sender, EventArgs e)
        {

        }

        private void btnDeleteJob_Click(object sender, EventArgs e)
        {
            try
            {
                bool success2 = AutoShopService.DeletePartsDetails(int.Parse(this.txtDeleteJob.Text));
                //Showing messagebox to the user informing him that entry was successful
                MessageBox.Show("Parts Details Deleted", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);

                bool success3 = AutoShopService.DeletePartsAndRepairDetails(int.Parse(this.txtDeleteJob.Text));
                //Showing messagebox to the user informing him that entry was successful
                MessageBox.Show("Parts and Repair Details Deleted", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);

                bool success = AutoShopService.DeleteSurveyDetails(int.Parse(this.txtDeleteJob.Text));
                //Showing messagebox to the user informing him that entry was successful
                MessageBox.Show("Survey Details Deleted", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);

              //Delete Job Details if user input matches JobNumber from Delete Job TextBox 
                bool success1 = AutoShopService.DeleteJobDetails(int.Parse(this.txtDeleteJob.Text));
                //Showing messagebox to the user informing him Deteled succsessful
                MessageBox.Show("Job Details Deleted", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);        

            }
            catch (SqlException ex)
            {
                //Showing messagebox to the user informing him that Errors while deleting job were encountered
                MessageBox.Show("Error Encountered whilst Deleting Job", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw ex;
            }

           

        }
    }
}

    
