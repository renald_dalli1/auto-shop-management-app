﻿namespace GUI
{
    partial class StaffSurveyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cmbCurrentStatus = new System.Windows.Forms.ComboBox();
            this.lblCurrentStatusTitle = new System.Windows.Forms.Label();
            this.txtSurveyCompletedBy = new System.Windows.Forms.TextBox();
            this.lblJSurveyCompletedBy = new System.Windows.Forms.Label();
            this.pnlCurrentStatus = new System.Windows.Forms.Panel();
            this.txtCurrentStatusD = new System.Windows.Forms.TextBox();
            this.lblDetailsTitle = new System.Windows.Forms.Label();
            this.pnlDetails = new System.Windows.Forms.Panel();
            this.txtSurname = new System.Windows.Forms.TextBox();
            this.lblSurname = new System.Windows.Forms.Label();
            this.txtNumberPlate = new System.Windows.Forms.TextBox();
            this.lblNumberPlate = new System.Windows.Forms.Label();
            this.txtVehicleModel = new System.Windows.Forms.TextBox();
            this.lblVehicleModel = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.lblLoggedAs = new System.Windows.Forms.Label();
            this.txtJobNumber = new System.Windows.Forms.TextBox();
            this.lblJobNumber = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblSurveyNotes = new System.Windows.Forms.Label();
            this.lblFaildPart = new System.Windows.Forms.Label();
            this.lblFaultType = new System.Windows.Forms.Label();
            this.txtSurveyNotes = new System.Windows.Forms.RichTextBox();
            this.cmbFaildPart = new System.Windows.Forms.ComboBox();
            this.cmbFaultType = new System.Windows.Forms.ComboBox();
            this.lblSurveyDetailsTitle = new System.Windows.Forms.Label();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.pnlCurrentStatus.SuspendLayout();
            this.pnlDetails.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbCurrentStatus
            // 
            this.cmbCurrentStatus.FormattingEnabled = true;
            this.cmbCurrentStatus.Location = new System.Drawing.Point(44, 47);
            this.cmbCurrentStatus.Name = "cmbCurrentStatus";
            this.cmbCurrentStatus.Size = new System.Drawing.Size(291, 24);
            this.cmbCurrentStatus.TabIndex = 0;
            this.cmbCurrentStatus.Text = "Select";
            // 
            // lblCurrentStatusTitle
            // 
            this.lblCurrentStatusTitle.AutoSize = true;
            this.lblCurrentStatusTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentStatusTitle.Location = new System.Drawing.Point(669, 93);
            this.lblCurrentStatusTitle.Name = "lblCurrentStatusTitle";
            this.lblCurrentStatusTitle.Size = new System.Drawing.Size(147, 24);
            this.lblCurrentStatusTitle.TabIndex = 39;
            this.lblCurrentStatusTitle.Text = "Current Status:";
            // 
            // txtSurveyCompletedBy
            // 
            this.txtSurveyCompletedBy.Location = new System.Drawing.Point(976, 722);
            this.txtSurveyCompletedBy.Name = "txtSurveyCompletedBy";
            this.txtSurveyCompletedBy.Size = new System.Drawing.Size(165, 22);
            this.txtSurveyCompletedBy.TabIndex = 0;
            this.txtSurveyCompletedBy.TextChanged += new System.EventHandler(this.txtSurveyCompletedBy_TextChanged);
            this.txtSurveyCompletedBy.Leave += new System.EventHandler(this.txtSurveyCompletedBy_Leave);
            // 
            // lblJSurveyCompletedBy
            // 
            this.lblJSurveyCompletedBy.AutoSize = true;
            this.lblJSurveyCompletedBy.Location = new System.Drawing.Point(828, 724);
            this.lblJSurveyCompletedBy.Name = "lblJSurveyCompletedBy";
            this.lblJSurveyCompletedBy.Size = new System.Drawing.Size(147, 17);
            this.lblJSurveyCompletedBy.TabIndex = 37;
            this.lblJSurveyCompletedBy.Text = "Survey Completed By:";
            this.lblJSurveyCompletedBy.Click += new System.EventHandler(this.lblJobCreatedBy_Click);
            // 
            // pnlCurrentStatus
            // 
            this.pnlCurrentStatus.Controls.Add(this.txtCurrentStatusD);
            this.pnlCurrentStatus.Controls.Add(this.cmbCurrentStatus);
            this.pnlCurrentStatus.Location = new System.Drawing.Point(673, 118);
            this.pnlCurrentStatus.Name = "pnlCurrentStatus";
            this.pnlCurrentStatus.Size = new System.Drawing.Size(370, 147);
            this.pnlCurrentStatus.TabIndex = 36;
            // 
            // txtCurrentStatusD
            // 
            this.txtCurrentStatusD.Location = new System.Drawing.Point(44, 96);
            this.txtCurrentStatusD.Name = "txtCurrentStatusD";
            this.txtCurrentStatusD.ReadOnly = true;
            this.txtCurrentStatusD.Size = new System.Drawing.Size(291, 22);
            this.txtCurrentStatusD.TabIndex = 2;
            // 
            // lblDetailsTitle
            // 
            this.lblDetailsTitle.AutoSize = true;
            this.lblDetailsTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDetailsTitle.Location = new System.Drawing.Point(31, 93);
            this.lblDetailsTitle.Name = "lblDetailsTitle";
            this.lblDetailsTitle.Size = new System.Drawing.Size(291, 24);
            this.lblDetailsTitle.TabIndex = 35;
            this.lblDetailsTitle.Text = "Customer and Vehicle Details:";
            // 
            // pnlDetails
            // 
            this.pnlDetails.Controls.Add(this.txtSurname);
            this.pnlDetails.Controls.Add(this.lblSurname);
            this.pnlDetails.Controls.Add(this.txtNumberPlate);
            this.pnlDetails.Controls.Add(this.lblNumberPlate);
            this.pnlDetails.Controls.Add(this.txtVehicleModel);
            this.pnlDetails.Controls.Add(this.lblVehicleModel);
            this.pnlDetails.Controls.Add(this.txtName);
            this.pnlDetails.Controls.Add(this.lblName);
            this.pnlDetails.Location = new System.Drawing.Point(20, 118);
            this.pnlDetails.Name = "pnlDetails";
            this.pnlDetails.Size = new System.Drawing.Size(625, 147);
            this.pnlDetails.TabIndex = 34;
            // 
            // txtSurname
            // 
            this.txtSurname.Location = new System.Drawing.Point(107, 78);
            this.txtSurname.Name = "txtSurname";
            this.txtSurname.ReadOnly = true;
            this.txtSurname.Size = new System.Drawing.Size(124, 22);
            this.txtSurname.TabIndex = 17;
            // 
            // lblSurname
            // 
            this.lblSurname.AutoSize = true;
            this.lblSurname.Location = new System.Drawing.Point(12, 78);
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Size = new System.Drawing.Size(69, 17);
            this.lblSurname.TabIndex = 16;
            this.lblSurname.Text = "Surname:";
            // 
            // txtNumberPlate
            // 
            this.txtNumberPlate.Location = new System.Drawing.Point(427, 75);
            this.txtNumberPlate.Name = "txtNumberPlate";
            this.txtNumberPlate.ReadOnly = true;
            this.txtNumberPlate.Size = new System.Drawing.Size(151, 22);
            this.txtNumberPlate.TabIndex = 15;
            // 
            // lblNumberPlate
            // 
            this.lblNumberPlate.AutoSize = true;
            this.lblNumberPlate.Location = new System.Drawing.Point(318, 76);
            this.lblNumberPlate.Name = "lblNumberPlate";
            this.lblNumberPlate.Size = new System.Drawing.Size(98, 17);
            this.lblNumberPlate.TabIndex = 14;
            this.lblNumberPlate.Text = "Number Plate:";
            // 
            // txtVehicleModel
            // 
            this.txtVehicleModel.Location = new System.Drawing.Point(427, 21);
            this.txtVehicleModel.Name = "txtVehicleModel";
            this.txtVehicleModel.ReadOnly = true;
            this.txtVehicleModel.Size = new System.Drawing.Size(151, 22);
            this.txtVehicleModel.TabIndex = 13;
            // 
            // lblVehicleModel
            // 
            this.lblVehicleModel.AutoSize = true;
            this.lblVehicleModel.Location = new System.Drawing.Point(318, 24);
            this.lblVehicleModel.Name = "lblVehicleModel";
            this.lblVehicleModel.Size = new System.Drawing.Size(100, 17);
            this.lblVehicleModel.TabIndex = 12;
            this.lblVehicleModel.Text = "Vehicle Model:";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(107, 21);
            this.txtName.Name = "txtName";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(124, 22);
            this.txtName.TabIndex = 5;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(12, 21);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(49, 17);
            this.lblName.TabIndex = 4;
            this.lblName.Text = "Name:";
            // 
            // lblLoggedAs
            // 
            this.lblLoggedAs.AutoSize = true;
            this.lblLoggedAs.Location = new System.Drawing.Point(999, 784);
            this.lblLoggedAs.Name = "lblLoggedAs";
            this.lblLoggedAs.Size = new System.Drawing.Size(142, 17);
            this.lblLoggedAs.TabIndex = 40;
            this.lblLoggedAs.Text = "Logged In: Staff User";
            // 
            // txtJobNumber
            // 
            this.txtJobNumber.Location = new System.Drawing.Point(127, 43);
            this.txtJobNumber.Name = "txtJobNumber";
            this.txtJobNumber.ReadOnly = true;
            this.txtJobNumber.Size = new System.Drawing.Size(100, 22);
            this.txtJobNumber.TabIndex = 31;
            // 
            // lblJobNumber
            // 
            this.lblJobNumber.AutoSize = true;
            this.lblJobNumber.Location = new System.Drawing.Point(32, 43);
            this.lblJobNumber.Name = "lblJobNumber";
            this.lblJobNumber.Size = new System.Drawing.Size(89, 17);
            this.lblJobNumber.TabIndex = 30;
            this.lblJobNumber.Text = "Job Number:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblSurveyNotes);
            this.panel1.Controls.Add(this.lblFaildPart);
            this.panel1.Controls.Add(this.lblFaultType);
            this.panel1.Controls.Add(this.txtSurveyNotes);
            this.panel1.Controls.Add(this.cmbFaildPart);
            this.panel1.Controls.Add(this.cmbFaultType);
            this.panel1.Location = new System.Drawing.Point(20, 314);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1023, 361);
            this.panel1.TabIndex = 41;
            // 
            // lblSurveyNotes
            // 
            this.lblSurveyNotes.AutoSize = true;
            this.lblSurveyNotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSurveyNotes.Location = new System.Drawing.Point(21, 118);
            this.lblSurveyNotes.Name = "lblSurveyNotes";
            this.lblSurveyNotes.Size = new System.Drawing.Size(114, 18);
            this.lblSurveyNotes.TabIndex = 17;
            this.lblSurveyNotes.Text = "Survey Notes:";
            this.lblSurveyNotes.Click += new System.EventHandler(this.label3_Click);
            // 
            // lblFaildPart
            // 
            this.lblFaildPart.AutoSize = true;
            this.lblFaildPart.Location = new System.Drawing.Point(586, 46);
            this.lblFaildPart.Name = "lblFaildPart";
            this.lblFaildPart.Size = new System.Drawing.Size(72, 17);
            this.lblFaildPart.TabIndex = 16;
            this.lblFaildPart.Text = "Faild Part:";
            // 
            // lblFaultType
            // 
            this.lblFaultType.AutoSize = true;
            this.lblFaultType.Location = new System.Drawing.Point(109, 46);
            this.lblFaultType.Name = "lblFaultType";
            this.lblFaultType.Size = new System.Drawing.Size(79, 17);
            this.lblFaultType.TabIndex = 15;
            this.lblFaultType.Text = "Fault Type:";
            // 
            // txtSurveyNotes
            // 
            this.txtSurveyNotes.Location = new System.Drawing.Point(24, 149);
            this.txtSurveyNotes.Name = "txtSurveyNotes";
            this.txtSurveyNotes.Size = new System.Drawing.Size(982, 192);
            this.txtSurveyNotes.TabIndex = 2;
            this.txtSurveyNotes.Text = "";
            // 
            // cmbFaildPart
            // 
            this.cmbFaildPart.FormattingEnabled = true;
            this.cmbFaildPart.Items.AddRange(new object[] {
            "Disc Brake Front",
            "Disc Brake Rear",
            "Water Pump",
            "Spark Plug",
            "ABS Sensor",
            "Alternator",
            "Radiator",
            "Master Cylinder",
            "Shock Absorber Front",
            "Shock Absorber Rear",
            "Springs Front",
            "Springs Rear",
            "Air Filter",
            "Glow Plug",
            "Fan Belt"});
            this.cmbFaildPart.Location = new System.Drawing.Point(673, 43);
            this.cmbFaildPart.Name = "cmbFaildPart";
            this.cmbFaildPart.Size = new System.Drawing.Size(291, 24);
            this.cmbFaildPart.TabIndex = 1;
            this.cmbFaildPart.Text = "Select";
            // 
            // cmbFaultType
            // 
            this.cmbFaultType.FormattingEnabled = true;
            this.cmbFaultType.Items.AddRange(new object[] {
            "Suspension",
            "Brakes",
            "Cooling",
            "Fuel",
            "Ignition",
            "Steering",
            "Transmission",
            "Electronics",
            "Lights",
            "Exhaust"});
            this.cmbFaultType.Location = new System.Drawing.Point(199, 43);
            this.cmbFaultType.Name = "cmbFaultType";
            this.cmbFaultType.Size = new System.Drawing.Size(291, 24);
            this.cmbFaultType.TabIndex = 0;
            this.cmbFaultType.Text = "Select";
            // 
            // lblSurveyDetailsTitle
            // 
            this.lblSurveyDetailsTitle.AutoSize = true;
            this.lblSurveyDetailsTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSurveyDetailsTitle.Location = new System.Drawing.Point(31, 287);
            this.lblSurveyDetailsTitle.Name = "lblSurveyDetailsTitle";
            this.lblSurveyDetailsTitle.Size = new System.Drawing.Size(148, 24);
            this.lblSurveyDetailsTitle.TabIndex = 43;
            this.lblSurveyDetailsTitle.Text = "Survey Details:";
            this.lblSurveyDetailsTitle.Click += new System.EventHandler(this.label1_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Image = global::GUI.Properties.Resources.edit;
            this.btnUpdate.Location = new System.Drawing.Point(570, 722);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 42);
            this.btnUpdate.TabIndex = 64;
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::GUI.Properties.Resources.search_icon;
            this.btnSearch.Location = new System.Drawing.Point(458, 722);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 42);
            this.btnSearch.TabIndex = 63;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // StaffSurveyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1153, 819);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.lblSurveyDetailsTitle);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblCurrentStatusTitle);
            this.Controls.Add(this.txtSurveyCompletedBy);
            this.Controls.Add(this.lblJSurveyCompletedBy);
            this.Controls.Add(this.pnlCurrentStatus);
            this.Controls.Add(this.lblDetailsTitle);
            this.Controls.Add(this.pnlDetails);
            this.Controls.Add(this.lblLoggedAs);
            this.Controls.Add(this.txtJobNumber);
            this.Controls.Add(this.lblJobNumber);
            this.Name = "StaffSurveyForm";
            this.Text = "Survey";
            this.Load += new System.EventHandler(this.StaffSurveyForm_Load);
            this.pnlCurrentStatus.ResumeLayout(false);
            this.pnlCurrentStatus.PerformLayout();
            this.pnlDetails.ResumeLayout(false);
            this.pnlDetails.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbCurrentStatus;
        private System.Windows.Forms.Label lblCurrentStatusTitle;
        private System.Windows.Forms.TextBox txtSurveyCompletedBy;
        private System.Windows.Forms.Label lblJSurveyCompletedBy;
        private System.Windows.Forms.Panel pnlCurrentStatus;
        private System.Windows.Forms.Label lblDetailsTitle;
        private System.Windows.Forms.Panel pnlDetails;
        private System.Windows.Forms.TextBox txtSurname;
        private System.Windows.Forms.Label lblSurname;
        private System.Windows.Forms.TextBox txtNumberPlate;
        private System.Windows.Forms.Label lblNumberPlate;
        private System.Windows.Forms.TextBox txtVehicleModel;
        private System.Windows.Forms.Label lblVehicleModel;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblLoggedAs;
        private System.Windows.Forms.TextBox txtJobNumber;
        private System.Windows.Forms.Label lblJobNumber;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cmbFaultType;
        private System.Windows.Forms.Label lblSurveyNotes;
        private System.Windows.Forms.Label lblFaildPart;
        private System.Windows.Forms.Label lblFaultType;
        private System.Windows.Forms.RichTextBox txtSurveyNotes;
        private System.Windows.Forms.ComboBox cmbFaildPart;
        private System.Windows.Forms.Label lblSurveyDetailsTitle;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtCurrentStatusD;
    }
}