﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class StaffSurveyForm : Form
    {
        public StaffSurveyForm()
        {
            InitializeComponent();
        }

        private void lblDateCreated_Click(object sender, EventArgs e)
        {

        }

        private void txtDateCreated_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblJobCreatedBy_Click(object sender, EventArgs e)
        {

        }

        private void txtSurveyCompletedBy_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            // clear part input text box after search
            cmbFaildPart.Text = String.Empty;
            cmbFaultType.Text = String.Empty;
            txtSurveyCompletedBy.Text = String.Empty;
            txtSurveyNotes.Text = String.Empty;
            

            string userInput = Microsoft.VisualBasic.Interaction.InputBox("Please Enter Job Number", "Database Search");

            // check user input is is null
            if (userInput != "")
            {
                int search = int.Parse(userInput);



                DataTable dt = AutoShopService.GetJobDetails(search);

               

                if (Convert.ToInt32(dt.Rows.Count.ToString()) > 0)
                {
                    this.txtJobNumber.Text = dt.Rows[0]["JobNumber"].ToString();
                    this.txtName.Text = dt.Rows[0]["Name"].ToString();
                    this.txtSurname.Text = dt.Rows[0]["Surname"].ToString();
                    this.txtVehicleModel.Text = dt.Rows[0]["VehicleModel"].ToString();
                    this.txtNumberPlate.Text = dt.Rows[0]["NumberPlate"].ToString();
                    this.cmbCurrentStatus.Text = dt.Rows[0]["StatusIDFK"].ToString();
                    this.txtCurrentStatusD.Text = dt.Rows[0]["CurrentStatus"].ToString();

                }
                else
                {
                    AutoShopService.CloseConnection();

                }

                DataTable ds = AutoShopService.GetSurveyDetails(search);

               

                if (Convert.ToInt32(ds.Rows.Count.ToString()) > 0)
                {

                    this.txtSurveyNotes.Text = ds.Rows[0]["SurveyNotes"].ToString();
                    this.cmbFaultType.Text = ds.Rows[0]["FaultType"].ToString();
                    this.cmbFaildPart.Text = ds.Rows[0]["FaildPart"].ToString();
                    this.txtSurveyCompletedBy.Text = ds.Rows[0]["SurveyCompletedBy"].ToString();


                }
                else
                {
                    AutoShopService.CloseConnection();

                }

            }

            else
            {
                MessageBox.Show("Job Number is required to find Job", "Job Search Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //Validation if update button is clicked and important textboxex are left null
            if (cmbFaultType.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Please Select Fault Type");
                return;
            }

           
            if (cmbFaildPart.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Please Select Faild Part");
                return;
            }

            if (txtSurveyCompletedBy.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Please enter Survey Completed By");
                return;
            }


            try { 

            bool success = AutoShopService.AddSurvey(DateTime.Today, this.cmbFaultType.Text, this.cmbFaildPart.Text, this.txtSurveyNotes.Text, this.txtSurveyCompletedBy.Text, int.Parse(this.txtJobNumber.Text));
            //Showing messagebox to the user informing him that entry was successful
            MessageBox.Show("New Survey Created", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);

                bool updateSuccess = AutoShopService.updateCurrentStatus(int.Parse(this.txtJobNumber.Text), cmbCurrentStatus.SelectedIndex + 3);
                MessageBox.Show(" Job Status Updated Successfully", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (SqlException ex)
            {

                MessageBox.Show("Error Encountered whilst updating Job Status", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw ex;
            }

}

        private void StaffSurveyForm_Load(object sender, EventArgs e)
        {
            this.cmbCurrentStatus.DisplayMember = "CurrentStatus";
            this.cmbCurrentStatus.ValueMember = "StatusID";
            this.cmbCurrentStatus.DataSource = AutoShopService.GetAllJobStatus_Survey();
            this.cmbCurrentStatus.Text = "Please Select";
        }

        private void txtSurveyCompletedBy_Leave(object sender, EventArgs e)
        {
           
        }
    }
}
