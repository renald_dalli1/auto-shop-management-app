﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace GUI
{
    public partial class StaffUpdateJobDetailsForm : Form
    {
       
        private string userInput;

        public StaffUpdateJobDetailsForm()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            // on Search Button Click a messege box to enter Job number will be shown 
            string userInput = Microsoft.VisualBasic.Interaction.InputBox("Please Enter Job Number", "Database Search");


            // check user input is is null
            if (userInput != "")
            {
                int search = int.Parse(userInput);

                //View JobDetails if user input Job Number matches
                DataTable dt = AutoShopService.GetJobDetails(search);

               

                if (Convert.ToInt32(dt.Rows.Count.ToString()) > 0)
                {
                    // Insert Data get from database to the respective textbox
                    this.txtJobNumber.Text = dt.Rows[0]["JobNumber"].ToString();
                    this.txtName.Text = dt.Rows[0]["Name"].ToString();
                    this.txtSurname.Text = dt.Rows[0]["Surname"].ToString();
                    this.txtIdCard.Text = dt.Rows[0]["IdCard"].ToString();
                    this.txtContact.Text = dt.Rows[0]["Contact"].ToString();
                    this.txtEmail.Text = dt.Rows[0]["Email"].ToString();
                    this.txtAddress.Text = dt.Rows[0]["Address"].ToString();
                    this.txtVehicleModel.Text = dt.Rows[0]["VehicleModel"].ToString();
                    this.txtNumberPlate.Text = dt.Rows[0]["NumberPlate"].ToString();
                    this.cmbCurrentStatus.Text = dt.Rows[0]["StatusIDFK"].ToString();
                    this.txtCurrentStatusD.Text = dt.Rows[0]["CurrentStatus"].ToString();
                }
                else
                {
                    AutoShopService.CloseConnection();

                }

                //View Survey Details if user input Job Number matches
                DataTable ds = AutoShopService.GetSurveyDetails(search);

                

                if (Convert.ToInt32(ds.Rows.Count.ToString()) > 0)
                {

                    this.cmbFaultType.Text = ds.Rows[0]["FaultType"].ToString();
                    this.cmbFaildPart.Text = ds.Rows[0]["FaildPart"].ToString();
                    this.txtSurveyCompletedBy.Text = ds.Rows[0]["SurveyCompletedBy"].ToString();
                }
                else
                {
                    AutoShopService.CloseConnection();

                }

                //ViewGetPartsandRepair if user input Job Number matches
                DataTable dr = AutoShopService.GetPartsAndRepairDetails(search);

                

                if (Convert.ToInt32(dr.Rows.Count.ToString()) > 0)
                {

                    this.txtRepairNotes.Text = dr.Rows[0]["PartsAndRepairNotes"].ToString();
                    this.txtRepairedBy.Text = dr.Rows[0]["PartsAndRepairRepairedBy"].ToString();

                }
                else
                {
                    AutoShopService.CloseConnection();

                }

                // On search button click dgPartsList datagrid view is refreshed and updated 
                dgPartsList();
                dgPartList.Update();
                dgPartList.Refresh();
            }
            else
            {
                MessageBox.Show("Job Number is required to find Job", "Job Search Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

            
        }

        public void dgPartsList()
        {
            //Data Grid View dgPartsList function
            System.Data.DataTable dtPartsList = AutoShopService.GetPartsItemsToView(int.Parse(txtJobNumber.Text));
            // dgPartsList is created and configured
            dgPartList.Columns.Clear();
            dgPartList.AutoSize = true;
            dgPartList.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCells;
            dgPartList.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            //DataGrid view new columns are created and configured
            DataGridViewLinkColumn link = new DataGridViewLinkColumn();
            link.UseColumnTextForLinkValue = false;
            link.LinkBehavior = LinkBehavior.SystemDefault;
            link.HeaderText = "PartsListLink";
            link.LinkColor = Color.Blue;
            link.TrackVisitedState = false;
            link.Text = "Link";
            link.UseColumnTextForLinkValue = false;
            this.dgPartList.Columns.Add(link);
            dgPartList.Columns[0].Visible = false;
            dgPartList.DataSource = dtPartsList;

        }

        private void StaffUpdateJobDetailsForm_Load(object sender, EventArgs e)
        {
            this.cmbCurrentStatus.ValueMember = "StatusID";
            this.cmbCurrentStatus.DisplayMember = "CurrentStatus";
            
            this.cmbCurrentStatus.DataSource = AutoShopService.GetAllJobStatus();
            this.cmbCurrentStatus.Text = "Please Select";
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //Validation if update button is clicked and important textboxex are left null
            if (txtCurrentStatusD.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Please Search Record or Update Job");
                return;
            }
            try
            {
                //on Update Button click the values in the components will be added to the database as per sql statment UpdateJobDetails

                bool success = AutoShopService.UpdateJobDetails(txtName.Text, txtSurname.Text, txtIdCard.Text, int.Parse(txtContact.Text), txtEmail.Text, txtAddress.Text, txtVehicleModel.Text, txtNumberPlate.Text, cmbCurrentStatus.SelectedIndex, int.Parse(this.txtJobNumber.Text));

                //Showing messagebox to the user informing him that Job Update was successful
                MessageBox.Show("Job Updated", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);


                //Showing messagebox to the user informing him that Job Status Update was successful
                bool updateSuccess = AutoShopService.updateCurrentStatus(int.Parse(this.txtJobNumber.Text), cmbCurrentStatus.SelectedIndex + 1);
                MessageBox.Show("Job Status Updated Successfully", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);      

            }
            catch (SqlException ex)
            {
                //Showing messagebox to the user informing him that Job Status Update was not successful
                MessageBox.Show("Error Encountered whilst Updating Job", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw ex;
            }

            try
            {
                //on Update Button click the values in the components will be added to the database as per sql statment UpdateSurveyDetails

                bool success1 = AutoShopService.UpdateSurveyDetails(this.cmbFaultType.Text, this.cmbFaildPart.Text, this.txtSurveyCompletedBy.Text, int.Parse(this.txtJobNumber.Text));
           
                

            }
            catch (SqlException ex)
            {
                //Showing messagebox to the user informing him that entry was not successful
                MessageBox.Show("Error Encountered whilst Updating Job", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw ex;
            }

            try
            {
                //on Update Button click the values in the components will be added to the database as per sql statment UpdatePartsandRepairDetails

                bool success1 = AutoShopService.UpdatePartsandRepairDetails(this.txtRepairNotes.Text, this.txtRepairedBy.Text, int.Parse(this.txtJobNumber.Text));

               

            }
            catch (SqlException ex)
            {
                //Showing messagebox to the user informing him that entry was not successful
                MessageBox.Show("Error Encountered whilst Updating Job", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw ex;
            }

        }
        #region Data input Validation
        private void txtName_Leave(object sender, EventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtName.Text, "^[a-zA-Z ]"))
            {
                //If the character is a numeric character a message box is displayed to the user
                MessageBox.Show("This textbox accepts only Alphabetical characters");

                //Component text is reset
                this.txtName.ResetText();

                //Component regain focus
                this.txtName.Focus();

            }
        }

        private void txtSurname_Leave(object sender, EventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtSurname.Text, "^[a-zA-Z ]"))
            {
                //If the character is a numeric character a message box is displayed to the user
                MessageBox.Show("This textbox accepts only Alphabetical characters");

                //Component text is reset
                this.txtSurname.ResetText();

                //Component regain focus
                this.txtSurname.Focus();

            }
        }

        private void txtIdCard_Leave(object sender, EventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtIdCard.Text, "^[a-zA-Z0-9]"))
            {
                //If the character is not either a numeric character and alpha character a message box is displayed to the user
                MessageBox.Show("This textbox accepts only AlphaNumeric characters");

                //Component text is reset
                this.txtIdCard.ResetText();

                //Component regain focus
                this.txtIdCard.Focus();

            }
        }

        private void txtVehicleModel_Leave(object sender, EventArgs e)
        {

            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtVehicleModel.Text, "^[a-zA-Z ]"))
            {
                //If the character is a numeric character a message box is displayed to the user
                MessageBox.Show("This textbox accepts only Alphabetical characters");

                //Component text is reset
                this.txtVehicleModel.ResetText();

                //Component regain focus
                this.txtVehicleModel.Focus();

            }
        }

        private void txtContact_Leave(object sender, EventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtContact.Text, "^[0-9]"))
            {
                //If the character is a alphabetic character a message box is displayed to the user
                MessageBox.Show("This textbox accepts only Numeric characters");

                //Component text is reset
                this.txtContact.ResetText();

                //Component regain focus
                this.txtContact.Focus();


            }
        }

        private void txtNumberPlate_Leave(object sender, EventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtNumberPlate.Text, "^[a-zA-Z0-9]"))
            {
                //If the character is not either a numeric character and alpha character a message box is displayed to the user
                MessageBox.Show("This textbox accepts only AlphaNumeric characters");

                //Component text is reset
                this.txtNumberPlate.ResetText();

                //Component regain focus
                this.txtNumberPlate.Focus();

            }
        }

        private void txtSurveyCompletedBy_Leave(object sender, EventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtSurveyCompletedBy.Text, "^[a-zA-Z ]"))
            {
                //If the character is a numeric character a message box is displayed to the user
                MessageBox.Show("This textbox accepts only Alphabetical characters");

                //Component text is reset
                this.txtSurveyCompletedBy.ResetText();

                //Component regain focus
                this.txtSurveyCompletedBy.Focus();

            }
        }

        private void txtRepairedBy_Leave(object sender, EventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtRepairedBy.Text, "^[a-zA-Z ]"))
            {
                //If the character is a numeric character a message box is displayed to the user
                MessageBox.Show("This textbox accepts only Alphabetical characters");

                //Component text is reset
                this.txtRepairedBy.ResetText();

                //Component regain focus
                this.txtRepairedBy.Focus();

            }

        }

        private void txtEmail_Leave(object sender, EventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtEmail.Text, "^[a-zA-Z0-9]"))
            {
                //If the character is not either a numeric character and alpha character a message box is displayed to the user
                MessageBox.Show("This textbox accepts only AlphaNumeric characters");

                //Component text is reset
                this.txtEmail.ResetText();

                //Component regain focus
                this.txtEmail.Focus();

            }
        }


        #endregion Data input Validation

        private void lblRepairNotes_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void btnAddPart_Click_1(object sender, EventArgs e)
         {

            //Validation if update button is clicked and important textboxex are left null
            if (txtPartNumber.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Please insert Part Number ");
                return;
            }


            if (txtQuantity.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Please enter Quantity");
                return;
            }

            try
                //on Add Part Button the values in the components will be added to the database as per sql statment AddPartsList
            {
                bool success1 = AutoShopService.AddPartsList(this.txtPartNumber.Text, this.txtPartDescription.Text, this.txtBrand.Text, int.Parse(this.txtQuantity.Text), decimal.Parse(this.txtPrice.Text), int.Parse(this.txtJobNumber.Text));

                //Showing messagebox to the user informing him that entry was successful
                MessageBox.Show("New PartsList Created", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }

            catch (SqlException ex)
            {
                //Showing messagebox to the user informing him that entry was not successful
                MessageBox.Show("Error Encountered whilst updating PartList", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw ex;
            }
        }

        private void btnDeletePart_Click_1(object sender, EventArgs e)
        {
            try
            {
                //Showing messagebox to the user to enter the PartList ID shown on dgPartslist datagridview
                string userInput = Microsoft.VisualBasic.Interaction.InputBox("Please Enter PartsListID", "Database Search");

                int search = int.Parse(userInput);

                bool success = AutoShopService.DeletePart(int.Parse(userInput));
                //Showing messagebox to the user informing him that entry was successful
                MessageBox.Show("Part Deleted", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            catch (SqlException ex)
            {
                //Showing messagebox to the user informing him that entry was not successful
                MessageBox.Show("Error Encountered whilst Deleting Job", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw ex;
            }
        }

        private void btnRefresh_Click_1(object sender, EventArgs e)
        {
            //On Refresh Button CLick the dgPartsList datagrid view is refreshed and show updated data
            dgPartsList();
            dgPartList.Update();
            dgPartList.Refresh();
        }

       
    }
}
