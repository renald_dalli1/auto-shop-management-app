﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using Excel = Microsoft.Office.Interop.Excel;

namespace GUI
{
    public partial class StaffJobListForm : Form
    {
        public StaffJobListForm()
        {
            InitializeComponent();
        }

        private void StaffJobListForm_Load(object sender, EventArgs e)
        {

            //Data Grid View PartsList

            System.Data.DataTable dtJobList = AutoShopService.SLA();
           // dgJobList is created and configured
            dgJobList.Columns.Clear();
            dgJobList.AutoSize = true;
            dgJobList.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCells;
            dgJobList.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None ;

            //DataGrid view new columns are created and configured
            DataGridViewLinkColumn link = new DataGridViewLinkColumn();
            link.UseColumnTextForLinkValue = false;
            link.LinkBehavior = LinkBehavior.SystemDefault;
            link.HeaderText = "PartsListLink";
            link.LinkColor = Color.Blue;
            link.TrackVisitedState = false;
            link.Text = "Link";
            link.UseColumnTextForLinkValue = false;
            this.dgJobList.Columns.Add(link);
            dgJobList.Columns[0].Visible = false;

            dgJobList.DataSource = dtJobList;

        }

        public void dgJobListview()
        {

           



        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

           
        }

        private void btnExport_Click(object sender, EventArgs e)
        {            
            //on button click  new savefile dialoge is created with file name and format configuration
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "Excel Documents (*.xls)|*.xls";
                sfd.FileName = "JobList.xls";

                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    // Copy DataGridView results to clipboard
                    CopyAlltoClipboard();

                    object misValue = System.Reflection.Missing.Value;
                    Excel.Application xlexcel = new Excel.Application();

                    xlexcel.DisplayAlerts = false; // Avoid to get two confirm overwrite prompts
                    Excel.Workbook xlWorkBook = xlexcel.Workbooks.Add(misValue);
                    Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

                    // Format column D as text before pasting results, this was required for my data
                    Excel.Range rng = xlWorkSheet.get_Range("D:D").Cells;
                    rng.NumberFormat = "@";

                    // Paste clipboard results to worksheet range
                    Excel.Range CR = (Excel.Range)xlWorkSheet.Cells[1, 1];
                    CR.Select();
                    xlWorkSheet.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);
      
                    // Delete blank column A and select cell A1
                    Excel.Range delRng = xlWorkSheet.get_Range("A:A").Cells;
                    delRng.Delete(Type.Missing);
                    xlWorkSheet.get_Range("A1").Select();

                    // Save the excel file under the captured location from the SaveFileDialog
                    xlWorkBook.SaveAs(sfd.FileName, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                    xlexcel.DisplayAlerts = true;
                    xlWorkBook.Close(true, misValue, misValue);
                    xlexcel.Quit();

                    releaseObject(xlWorkSheet);
                    releaseObject(xlWorkBook);
                    releaseObject(xlexcel);

                    // Clear Clipboard and DataGridView selection
                    Clipboard.Clear();
                    dgJobList.ClearSelection();

                    // Open the newly saved excel file
                    if (File.Exists(sfd.FileName))
                        System.Diagnostics.Process.Start(sfd.FileName);
                }
            }

            private void CopyAlltoClipboard()
            {
                //copying all to clipboard
                dgJobList.SelectAll();
                DataObject dataObj = dgJobList.GetClipboardContent();
                if (dataObj != null)
                    Clipboard.SetDataObject(dataObj);
            }

            private void releaseObject(object obj)
            {
                try
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                    obj = null;
                }
                catch (Exception ex)
                {
                    obj = null;

                //Showing messege box to user that error was encountered
                MessageBox.Show("Exception Occurred while releasing object " + ex.ToString());
                }
                finally
                {
                    GC.Collect();
                }
            }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void dgJobList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
 }

