﻿namespace GUI
{
    partial class SecretaryDeleteJobForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblJobNumber = new System.Windows.Forms.Label();
            this.txtDeleteJob = new System.Windows.Forms.TextBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.pbDeteteJob = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbDeteteJob)).BeginInit();
            this.SuspendLayout();
            // 
            // lblJobNumber
            // 
            this.lblJobNumber.AutoSize = true;
            this.lblJobNumber.Location = new System.Drawing.Point(194, 273);
            this.lblJobNumber.Name = "lblJobNumber";
            this.lblJobNumber.Size = new System.Drawing.Size(89, 17);
            this.lblJobNumber.TabIndex = 12;
            this.lblJobNumber.Text = "Job Number:";
            // 
            // txtDeleteJob
            // 
            this.txtDeleteJob.Location = new System.Drawing.Point(304, 273);
            this.txtDeleteJob.Name = "txtDeleteJob";
            this.txtDeleteJob.Size = new System.Drawing.Size(138, 22);
            this.txtDeleteJob.TabIndex = 0;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(247, 325);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(146, 62);
            this.btnDelete.TabIndex = 1;
            this.btnDelete.Text = "Delete Job";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // pbDeteteJob
            // 
            this.pbDeteteJob.Image = global::GUI.Properties.Resources.ASmanagment_App;
            this.pbDeteteJob.Location = new System.Drawing.Point(202, 23);
            this.pbDeteteJob.Name = "pbDeteteJob";
            this.pbDeteteJob.Size = new System.Drawing.Size(240, 222);
            this.pbDeteteJob.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbDeteteJob.TabIndex = 13;
            this.pbDeteteJob.TabStop = false;
            // 
            // SecretaryDeleteJobForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(629, 417);
            this.Controls.Add(this.pbDeteteJob);
            this.Controls.Add(this.lblJobNumber);
            this.Controls.Add(this.txtDeleteJob);
            this.Controls.Add(this.btnDelete);
            this.Name = "SecretaryDeleteJobForm";
            this.Text = "Delete Job";
            this.Load += new System.EventHandler(this.SecretaryDeleteJobForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbDeteteJob)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblJobNumber;
        private System.Windows.Forms.TextBox txtDeleteJob;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.PictureBox pbDeteteJob;
    }
}