﻿namespace GUI
{
    partial class SecretaryRepairReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SecretaryRepairReportForm));
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.lblAddress = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.txtSurname = new System.Windows.Forms.TextBox();
            this.lblSurname = new System.Windows.Forms.Label();
            this.txtNumberPlate = new System.Windows.Forms.TextBox();
            this.lblNumberPlate = new System.Windows.Forms.Label();
            this.txtVehicleModel = new System.Windows.Forms.TextBox();
            this.lblVehicleModel = new System.Windows.Forms.Label();
            this.lblDetailsTitle = new System.Windows.Forms.Label();
            this.txtContact = new System.Windows.Forms.TextBox();
            this.lblContact = new System.Windows.Forms.Label();
            this.txtIdCard = new System.Windows.Forms.TextBox();
            this.lblIDCard = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtJobNumber = new System.Windows.Forms.TextBox();
            this.lblJobNumber = new System.Windows.Forms.Label();
            this.pnlDetails = new System.Windows.Forms.Panel();
            this.lblName = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgPartList = new System.Windows.Forms.DataGridView();
            this.lblPartsList = new System.Windows.Forms.Label();
            this.lblRepairNotes = new System.Windows.Forms.Label();
            this.txtRepairNotes = new System.Windows.Forms.RichTextBox();
            this.lblFaildPart = new System.Windows.Forms.Label();
            this.lblFaultType = new System.Windows.Forms.Label();
            this.cmbFaildPart = new System.Windows.Forms.ComboBox();
            this.cmbFaultType = new System.Windows.Forms.ComboBox();
            this.pnlIssuedDetails = new System.Windows.Forms.Panel();
            this.txtDateIssued = new System.Windows.Forms.MaskedTextBox();
            this.lblDateIssued = new System.Windows.Forms.Label();
            this.txtIssuedBy = new System.Windows.Forms.TextBox();
            this.lblIssuedBy = new System.Windows.Forms.Label();
            this.lblReportDetails = new System.Windows.Forms.Label();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.pnlDetails.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPartList)).BeginInit();
            this.pnlIssuedDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(107, 155);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(492, 22);
            this.txtAddress.TabIndex = 5;
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(12, 155);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(64, 17);
            this.lblAddress.TabIndex = 20;
            this.lblAddress.Text = "Address:";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(107, 116);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(492, 22);
            this.txtEmail.TabIndex = 4;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(12, 116);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(46, 17);
            this.lblEmail.TabIndex = 18;
            this.lblEmail.Text = "Email:";
            // 
            // txtSurname
            // 
            this.txtSurname.Location = new System.Drawing.Point(340, 22);
            this.txtSurname.Name = "txtSurname";
            this.txtSurname.Size = new System.Drawing.Size(124, 22);
            this.txtSurname.TabIndex = 1;
            // 
            // lblSurname
            // 
            this.lblSurname.AutoSize = true;
            this.lblSurname.Location = new System.Drawing.Point(245, 22);
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Size = new System.Drawing.Size(69, 17);
            this.lblSurname.TabIndex = 16;
            this.lblSurname.Text = "Surname:";
            // 
            // txtNumberPlate
            // 
            this.txtNumberPlate.Location = new System.Drawing.Point(810, 68);
            this.txtNumberPlate.Name = "txtNumberPlate";
            this.txtNumberPlate.Size = new System.Drawing.Size(151, 22);
            this.txtNumberPlate.TabIndex = 7;
            // 
            // lblNumberPlate
            // 
            this.lblNumberPlate.AutoSize = true;
            this.lblNumberPlate.Location = new System.Drawing.Point(701, 69);
            this.lblNumberPlate.Name = "lblNumberPlate";
            this.lblNumberPlate.Size = new System.Drawing.Size(98, 17);
            this.lblNumberPlate.TabIndex = 14;
            this.lblNumberPlate.Text = "Number Plate:";
            // 
            // txtVehicleModel
            // 
            this.txtVehicleModel.Location = new System.Drawing.Point(810, 18);
            this.txtVehicleModel.Name = "txtVehicleModel";
            this.txtVehicleModel.Size = new System.Drawing.Size(151, 22);
            this.txtVehicleModel.TabIndex = 6;
            // 
            // lblVehicleModel
            // 
            this.lblVehicleModel.AutoSize = true;
            this.lblVehicleModel.Location = new System.Drawing.Point(701, 21);
            this.lblVehicleModel.Name = "lblVehicleModel";
            this.lblVehicleModel.Size = new System.Drawing.Size(100, 17);
            this.lblVehicleModel.TabIndex = 12;
            this.lblVehicleModel.Text = "Vehicle Model:";
            // 
            // lblDetailsTitle
            // 
            this.lblDetailsTitle.AutoSize = true;
            this.lblDetailsTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDetailsTitle.Location = new System.Drawing.Point(29, 86);
            this.lblDetailsTitle.Name = "lblDetailsTitle";
            this.lblDetailsTitle.Size = new System.Drawing.Size(291, 24);
            this.lblDetailsTitle.TabIndex = 78;
            this.lblDetailsTitle.Text = "Customer and Vehicle Details:";
            // 
            // txtContact
            // 
            this.txtContact.Location = new System.Drawing.Point(340, 67);
            this.txtContact.Name = "txtContact";
            this.txtContact.Size = new System.Drawing.Size(124, 22);
            this.txtContact.TabIndex = 3;
            this.txtContact.TextChanged += new System.EventHandler(this.txtContact_TextChanged);
            // 
            // lblContact
            // 
            this.lblContact.AutoSize = true;
            this.lblContact.Location = new System.Drawing.Point(245, 67);
            this.lblContact.Name = "lblContact";
            this.lblContact.Size = new System.Drawing.Size(60, 17);
            this.lblContact.TabIndex = 8;
            this.lblContact.Text = "Contact:";
            this.lblContact.Click += new System.EventHandler(this.lblContact_Click);
            // 
            // txtIdCard
            // 
            this.txtIdCard.Location = new System.Drawing.Point(107, 67);
            this.txtIdCard.Name = "txtIdCard";
            this.txtIdCard.Size = new System.Drawing.Size(100, 22);
            this.txtIdCard.TabIndex = 2;
            // 
            // lblIDCard
            // 
            this.lblIDCard.AutoSize = true;
            this.lblIDCard.Location = new System.Drawing.Point(12, 67);
            this.lblIDCard.Name = "lblIDCard";
            this.lblIDCard.Size = new System.Drawing.Size(59, 17);
            this.lblIDCard.TabIndex = 6;
            this.lblIDCard.Text = "ID Card:";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(107, 21);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(100, 22);
            this.txtName.TabIndex = 0;
            // 
            // txtJobNumber
            // 
            this.txtJobNumber.Location = new System.Drawing.Point(125, 41);
            this.txtJobNumber.Name = "txtJobNumber";
            this.txtJobNumber.Size = new System.Drawing.Size(100, 22);
            this.txtJobNumber.TabIndex = 76;
            // 
            // lblJobNumber
            // 
            this.lblJobNumber.AutoSize = true;
            this.lblJobNumber.Location = new System.Drawing.Point(30, 41);
            this.lblJobNumber.Name = "lblJobNumber";
            this.lblJobNumber.Size = new System.Drawing.Size(89, 17);
            this.lblJobNumber.TabIndex = 75;
            this.lblJobNumber.Text = "Job Number:";
            // 
            // pnlDetails
            // 
            this.pnlDetails.Controls.Add(this.txtAddress);
            this.pnlDetails.Controls.Add(this.lblAddress);
            this.pnlDetails.Controls.Add(this.txtEmail);
            this.pnlDetails.Controls.Add(this.lblEmail);
            this.pnlDetails.Controls.Add(this.txtSurname);
            this.pnlDetails.Controls.Add(this.lblSurname);
            this.pnlDetails.Controls.Add(this.txtNumberPlate);
            this.pnlDetails.Controls.Add(this.lblNumberPlate);
            this.pnlDetails.Controls.Add(this.txtVehicleModel);
            this.pnlDetails.Controls.Add(this.lblVehicleModel);
            this.pnlDetails.Controls.Add(this.txtContact);
            this.pnlDetails.Controls.Add(this.lblContact);
            this.pnlDetails.Controls.Add(this.txtIdCard);
            this.pnlDetails.Controls.Add(this.lblIDCard);
            this.pnlDetails.Controls.Add(this.txtName);
            this.pnlDetails.Controls.Add(this.lblName);
            this.pnlDetails.Location = new System.Drawing.Point(18, 111);
            this.pnlDetails.Name = "pnlDetails";
            this.pnlDetails.Size = new System.Drawing.Size(1087, 197);
            this.pnlDetails.TabIndex = 77;
            this.pnlDetails.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlDetails_Paint);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(12, 21);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(49, 17);
            this.lblName.TabIndex = 4;
            this.lblName.Text = "Name:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgPartList);
            this.panel1.Controls.Add(this.lblPartsList);
            this.panel1.Controls.Add(this.lblRepairNotes);
            this.panel1.Controls.Add(this.txtRepairNotes);
            this.panel1.Controls.Add(this.lblFaildPart);
            this.panel1.Controls.Add(this.lblFaultType);
            this.panel1.Controls.Add(this.cmbFaildPart);
            this.panel1.Controls.Add(this.cmbFaultType);
            this.panel1.Location = new System.Drawing.Point(18, 358);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1087, 498);
            this.panel1.TabIndex = 79;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // dgPartList
            // 
            this.dgPartList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPartList.Location = new System.Drawing.Point(15, 252);
            this.dgPartList.Name = "dgPartList";
            this.dgPartList.RowTemplate.Height = 24;
            this.dgPartList.Size = new System.Drawing.Size(1014, 193);
            this.dgPartList.TabIndex = 29;
            // 
            // lblPartsList
            // 
            this.lblPartsList.AutoSize = true;
            this.lblPartsList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPartsList.Location = new System.Drawing.Point(12, 210);
            this.lblPartsList.Name = "lblPartsList";
            this.lblPartsList.Size = new System.Drawing.Size(85, 18);
            this.lblPartsList.TabIndex = 24;
            this.lblPartsList.Text = "Parts List:";
            // 
            // lblRepairNotes
            // 
            this.lblRepairNotes.AutoSize = true;
            this.lblRepairNotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRepairNotes.Location = new System.Drawing.Point(12, 71);
            this.lblRepairNotes.Name = "lblRepairNotes";
            this.lblRepairNotes.Size = new System.Drawing.Size(112, 18);
            this.lblRepairNotes.TabIndex = 22;
            this.lblRepairNotes.Text = "Repair Notes:";
            // 
            // txtRepairNotes
            // 
            this.txtRepairNotes.Location = new System.Drawing.Point(15, 103);
            this.txtRepairNotes.Name = "txtRepairNotes";
            this.txtRepairNotes.Size = new System.Drawing.Size(1014, 92);
            this.txtRepairNotes.TabIndex = 2;
            this.txtRepairNotes.Text = "";
            // 
            // lblFaildPart
            // 
            this.lblFaildPart.AutoSize = true;
            this.lblFaildPart.Location = new System.Drawing.Point(494, 21);
            this.lblFaildPart.Name = "lblFaildPart";
            this.lblFaildPart.Size = new System.Drawing.Size(72, 17);
            this.lblFaildPart.TabIndex = 20;
            this.lblFaildPart.Text = "Faild Part:";
            // 
            // lblFaultType
            // 
            this.lblFaultType.AutoSize = true;
            this.lblFaultType.Location = new System.Drawing.Point(17, 21);
            this.lblFaultType.Name = "lblFaultType";
            this.lblFaultType.Size = new System.Drawing.Size(79, 17);
            this.lblFaultType.TabIndex = 19;
            this.lblFaultType.Text = "Fault Type:";
            // 
            // cmbFaildPart
            // 
            this.cmbFaildPart.FormattingEnabled = true;
            this.cmbFaildPart.Location = new System.Drawing.Point(581, 18);
            this.cmbFaildPart.Name = "cmbFaildPart";
            this.cmbFaildPart.Size = new System.Drawing.Size(291, 24);
            this.cmbFaildPart.TabIndex = 1;
            // 
            // cmbFaultType
            // 
            this.cmbFaultType.FormattingEnabled = true;
            this.cmbFaultType.Location = new System.Drawing.Point(107, 18);
            this.cmbFaultType.Name = "cmbFaultType";
            this.cmbFaultType.Size = new System.Drawing.Size(291, 24);
            this.cmbFaultType.TabIndex = 0;
            // 
            // pnlIssuedDetails
            // 
            this.pnlIssuedDetails.Controls.Add(this.txtDateIssued);
            this.pnlIssuedDetails.Controls.Add(this.lblDateIssued);
            this.pnlIssuedDetails.Controls.Add(this.txtIssuedBy);
            this.pnlIssuedDetails.Controls.Add(this.lblIssuedBy);
            this.pnlIssuedDetails.Location = new System.Drawing.Point(33, 865);
            this.pnlIssuedDetails.Name = "pnlIssuedDetails";
            this.pnlIssuedDetails.Size = new System.Drawing.Size(517, 73);
            this.pnlIssuedDetails.TabIndex = 80;
            // 
            // txtDateIssued
            // 
            this.txtDateIssued.Location = new System.Drawing.Point(327, 22);
            this.txtDateIssued.Margin = new System.Windows.Forms.Padding(4);
            this.txtDateIssued.Mask = "00/00/0000";
            this.txtDateIssued.Name = "txtDateIssued";
            this.txtDateIssued.Size = new System.Drawing.Size(165, 22);
            this.txtDateIssued.TabIndex = 1;
            this.txtDateIssued.ValidatingType = typeof(System.DateTime);
            // 
            // lblDateIssued
            // 
            this.lblDateIssued.AutoSize = true;
            this.lblDateIssued.Location = new System.Drawing.Point(237, 22);
            this.lblDateIssued.Name = "lblDateIssued";
            this.lblDateIssued.Size = new System.Drawing.Size(87, 17);
            this.lblDateIssued.TabIndex = 16;
            this.lblDateIssued.Text = "Date Issued:";
            // 
            // txtIssuedBy
            // 
            this.txtIssuedBy.Location = new System.Drawing.Point(98, 22);
            this.txtIssuedBy.Name = "txtIssuedBy";
            this.txtIssuedBy.Size = new System.Drawing.Size(121, 22);
            this.txtIssuedBy.TabIndex = 0;
            // 
            // lblIssuedBy
            // 
            this.lblIssuedBy.AutoSize = true;
            this.lblIssuedBy.Location = new System.Drawing.Point(23, 22);
            this.lblIssuedBy.Name = "lblIssuedBy";
            this.lblIssuedBy.Size = new System.Drawing.Size(73, 17);
            this.lblIssuedBy.TabIndex = 4;
            this.lblIssuedBy.Text = "Issued By:";
            // 
            // lblReportDetails
            // 
            this.lblReportDetails.AutoSize = true;
            this.lblReportDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReportDetails.Location = new System.Drawing.Point(29, 331);
            this.lblReportDetails.Name = "lblReportDetails";
            this.lblReportDetails.Size = new System.Drawing.Size(146, 24);
            this.lblReportDetails.TabIndex = 81;
            this.lblReportDetails.Text = "Report Details:";
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            this.printPreviewDialog1.Load += new System.EventHandler(this.printPreviewDialog1_Load);
            // 
            // btnPrint
            // 
            this.btnPrint.Image = global::GUI.Properties.Resources.Printer_blue_icon;
            this.btnPrint.Location = new System.Drawing.Point(828, 880);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 42);
            this.btnPrint.TabIndex = 84;
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Image = global::GUI.Properties.Resources.edit;
            this.btnUpdate.Location = new System.Drawing.Point(722, 880);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 42);
            this.btnUpdate.TabIndex = 83;
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::GUI.Properties.Resources.search_icon;
            this.btnSearch.Location = new System.Drawing.Point(610, 880);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 42);
            this.btnSearch.TabIndex = 82;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // SecretaryRepairReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1123, 979);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.lblReportDetails);
            this.Controls.Add(this.pnlIssuedDetails);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblDetailsTitle);
            this.Controls.Add(this.txtJobNumber);
            this.Controls.Add(this.lblJobNumber);
            this.Controls.Add(this.pnlDetails);
            this.Name = "SecretaryRepairReportForm";
            this.Text = "SecretaryRepairReportForm";
            this.Load += new System.EventHandler(this.SecretaryRepairReportForm_Load);
            this.pnlDetails.ResumeLayout(false);
            this.pnlDetails.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPartList)).EndInit();
            this.pnlIssuedDetails.ResumeLayout(false);
            this.pnlIssuedDetails.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.TextBox txtSurname;
        private System.Windows.Forms.Label lblSurname;
        private System.Windows.Forms.TextBox txtNumberPlate;
        private System.Windows.Forms.Label lblNumberPlate;
        private System.Windows.Forms.TextBox txtVehicleModel;
        private System.Windows.Forms.Label lblVehicleModel;
        private System.Windows.Forms.Label lblDetailsTitle;
        private System.Windows.Forms.TextBox txtContact;
        private System.Windows.Forms.Label lblContact;
        private System.Windows.Forms.TextBox txtIdCard;
        private System.Windows.Forms.Label lblIDCard;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtJobNumber;
        private System.Windows.Forms.Label lblJobNumber;
        private System.Windows.Forms.Panel pnlDetails;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblPartsList;
        private System.Windows.Forms.Label lblRepairNotes;
        private System.Windows.Forms.RichTextBox txtRepairNotes;
        private System.Windows.Forms.Label lblFaildPart;
        private System.Windows.Forms.Label lblFaultType;
        private System.Windows.Forms.ComboBox cmbFaildPart;
        private System.Windows.Forms.ComboBox cmbFaultType;
        private System.Windows.Forms.Panel pnlIssuedDetails;
        private System.Windows.Forms.Label lblDateIssued;
        private System.Windows.Forms.TextBox txtIssuedBy;
        private System.Windows.Forms.Label lblIssuedBy;
        private System.Windows.Forms.Label lblReportDetails;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DataGridView dgPartList;
        private System.Windows.Forms.MaskedTextBox txtDateIssued;
        private System.Windows.Forms.Button btnPrint;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
    }
}