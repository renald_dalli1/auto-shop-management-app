﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class StaffPartsAndRepairForm : Form
    {
        public StaffPartsAndRepairForm()
        {
            InitializeComponent();
        }

        private void lblSurveyDetailsTitle_Click(object sender, EventArgs e)
        {

        }

        private void lblJSurveyCompletedBy_Click(object sender, EventArgs e)
        {

        }

        private void txtSurveyCompletedBy_TextChanged(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lblPartNumber_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void StaffPartsAndRepairForm_Load(object sender, EventArgs e)
        {
            this.cmbCurrentStatus.DisplayMember = "CurrentStatus";
            this.cmbCurrentStatus.ValueMember = "StatusID";
            this.cmbCurrentStatus.DataSource = AutoShopService.GetAllJobStatus_Parts();
            this.cmbCurrentStatus.Text = "Please Select";

          

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

            // clear part input text box after search
            txtPartNumber.Text = String.Empty;
            txtPartDescription.Text = String.Empty;
            txtBrand.Text = String.Empty;
            txtQuantity.Text = String.Empty;
            txtPrice.Text = String.Empty;

            string userInput = Microsoft.VisualBasic.Interaction.InputBox("Please Enter Job Number", "Database Search");

            int search = int.Parse(userInput);
            

            DataTable dt = AutoShopService.GetJobDetails(search);

            

            if (Convert.ToInt32(dt.Rows.Count.ToString()) > 0)
            {
                this.txtJobNumber.Text = dt.Rows[0]["JobNumber"].ToString();
                this.txtName.Text = dt.Rows[0]["Name"].ToString();
                this.txtSurname.Text = dt.Rows[0]["Surname"].ToString();
                this.txtVehicleModel.Text = dt.Rows[0]["VehicleModel"].ToString();
                this.txtNumberPlate.Text = dt.Rows[0]["NumberPlate"].ToString();
                this.cmbCurrentStatus.Text = dt.Rows[0]["StatusIDFK"].ToString();
                this.txtCurrentStatusD.Text = dt.Rows[0]["CurrentStatus"].ToString();
            }
            else
            {
                AutoShopService.CloseConnection();


            }

            DataTable dr = AutoShopService.GetPartsAndRepairDetails(search);

           

            if (Convert.ToInt32(dr.Rows.Count.ToString()) > 0)
            {

                this.txtRepairNotes.Text = dr.Rows[0]["PartsAndRepairNotes"].ToString();
                this.txtRepairedBy.Text = dr.Rows[0]["PartsAndRepairRepairedBy"].ToString();

            }
            else
            {
                AutoShopService.CloseConnection();
                
            }


            DataTable dpl = AutoShopService.GetPartsListDetails(search);

            

            if (Convert.ToInt32(dpl.Rows.Count.ToString()) > 0)
            {

                this.txtPartNumber.Text = dpl.Rows[0]["PartNumber"].ToString();
                this.txtPartDescription.Text = dpl.Rows[0]["PartDescription"].ToString();
                this.txtBrand.Text = dpl.Rows[0]["Brand"].ToString();
                this.txtQuantity.Text = dpl.Rows[0]["Quantity"].ToString();
                this.txtPrice.Text = dpl.Rows[0]["Price"].ToString();
              

            }
            else
            {
                
                AutoShopService.CloseConnection();
                
            }
            
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //Validation if update button is clicked and important textboxex are left null
            if (txtPartNumber.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Please Select Fault Type");
                return;
            }


            if (txtRepairedBy.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Please Select Faild Part");
                return;
            }

            if (txtQuantity.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Please enter Survey Completed By");
                return;
            }
            try
            {

                bool success = AutoShopService.AddPartsandRepair(DateTime.Today, this.txtRepairNotes.Text, this.txtRepairedBy.Text, int.Parse(this.txtJobNumber.Text));
                //Showing messagebox to the user informing him that entry was successful
                MessageBox.Show("New PartsandRepair Created", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
              
               

                bool updateSuccess = AutoShopService.updateCurrentStatus(int.Parse(this.txtJobNumber.Text), cmbCurrentStatus.SelectedIndex + 5);
                MessageBox.Show("Job Status Updated Successfully", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (SqlException ex)
            {

                MessageBox.Show("Error Encountered whilst updating Job Status", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw ex;
            }


        }


        private void btnAddPart_Click(object sender, EventArgs e)
        {
            //Validation if update button is clicked and important textboxex are left null
            if (txtPartNumber.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Please insert Part Number ");
                return;
            }


            if (txtQuantity.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Please enter Quantity");
                return;
            }

            try
            {
                bool success1 = AutoShopService.AddPartsList(this.txtPartNumber.Text, this.txtPartDescription.Text, this.txtBrand.Text, int.Parse(this.txtQuantity.Text), decimal.Parse(this.txtPrice.Text), int.Parse(this.txtJobNumber.Text));
                //Showing messagebox to the user informing him that entry was successful
                MessageBox.Show("New PartsList Created", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }

            catch (SqlException ex)
            {
                MessageBox.Show("Error Encountered whilst updating PartList", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw ex;
            }
        }


        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void lblPartsList_Click(object sender, EventArgs e)
        {

        }

        private void txtRepairedBy_Leave(object sender, EventArgs e)
        {

            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtRepairedBy.Text, "^[a-zA-Z ]"))
            {
                //If the character is a numeric character a message box is displayed to the user
                MessageBox.Show("This textbox accepts only Alphabetical characters");

                //Component text is reset
                this.txtRepairedBy.ResetText();

                //Component regain focus
                this.txtRepairedBy.Focus();

            }
        }

        private void cmbCurrentStatus_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void cmbCurrentStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
    }
}
