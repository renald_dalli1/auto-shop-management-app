﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class StaffWelcomeForm : Form
    {
        public StaffWelcomeForm()
        {
            InitializeComponent();
        }

        private void partsRepairToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void StaffWelcomeForm_Load(object sender, EventArgs e)
        {

        }

        private void tsmiAddNewJob_Click(object sender, EventArgs e)
        {
            StaffAddNewJobForm staffaddnewjobForm = new StaffAddNewJobForm();

            staffaddnewjobForm.Show();
        }

        private void tsmiSurvey_Click(object sender, EventArgs e)
        {
            StaffSurveyForm staffsurveyForm = new StaffSurveyForm();

            staffsurveyForm.Show();
        }

        private void tsmiPartsAndRepair_Click(object sender, EventArgs e)
        {
            StaffPartsAndRepairForm staffPartsAndRepairForm = new StaffPartsAndRepairForm();
            staffPartsAndRepairForm.Show();
        }

        private void tsmiEndJob_Click(object sender, EventArgs e)
        {
            StaffEndJobForm staffendjobForm = new StaffEndJobForm();
            staffendjobForm.Show();

        }

        private void tsmiUpdateJobDetails_Click(object sender, EventArgs e)
        {
            StaffUpdateJobDetailsForm staffupdatejobdetailsForm = new StaffUpdateJobDetailsForm();
            staffupdatejobdetailsForm.Show();
        }

        private void tsmiDeleteJob_Click(object sender, EventArgs e)
        {
           StaffDeleteJobForm staffDeleteJobForm = new StaffDeleteJobForm();
            staffDeleteJobForm.Show();
        }

        private void tsmiJobList_Click(object sender, EventArgs e)
        {
            StaffJobListForm staffjoblistForm = new StaffJobListForm();
            staffjoblistForm.Show();
        }

        private void StaffWelcomeForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }
    }
}
