﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace GUI
{
    public partial class LogInForm : Form
    {
        public LogInForm()
        {
            InitializeComponent();
        }

        private static string _connStr = @"Data Source=RENSMSI\SQLEXPRESS;Initial Catalog=AutoShopManagementApplication;Integrated Security=True";

        //declare sql connection
        private static SqlConnection _conn = null;

        /// <summary>
        /// Method to open the connection of the database
        /// </summary>
        /// <returns></returns>
        public static bool OpenConnection()
        {
            //Checking if MySqlConnection is null
            if (_conn == null)

                //If MySqlConnection is null, the connection string is passed as a parameter to connect to the database
                _conn = new SqlConnection(_connStr);

            //Cheking the State of the connection if it's state is not Opened
            if (_conn.State != ConnectionState.Open)
            {
                try
                {
                    //open connection
                    _conn.Open();
                }
                catch (SqlException ex)
                {
                    //Throwing an exception error in the eventuality the connection with the database is lost
                    ex.ToString();
                }
            }

            //changing the connection state to Open
            return (_conn.State == ConnectionState.Open);
        }

        private void Log_In_Form_Load(object sender, EventArgs e)
        {
           
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (cmbUserType.SelectedIndex > -1)
            {
                string UserType = cmbUserType.SelectedItem.ToString();
            }
            else
            {
                MessageBox.Show("Please select User");
                return;
            }
            //setting connection with database server

            SqlConnection sqlcon = new SqlConnection(@"Data Source=RENSMSI\SQLEXPRESS;Initial Catalog=AutoShopManagementApplication;Integrated Security=True");
            //query or command to check data input in username and password textboxes
            string query = "Select * from LogInTable where Username= '"+txtUsername.Text.Trim() +"'and Password = '"+txtPassword.Text.Trim() +"'";
           
            //enable data adapter
            SqlDataAdapter sda = new SqlDataAdapter(query, sqlcon);
            //enable data table 
            DataTable dtbl = new DataTable();
            sda.Fill(dtbl);

            string LogInAsValue = cmbUserType.SelectedItem.ToString();
            

            if (dtbl.Rows.Count > 0)
            {
                for (int i = 0; i < dtbl.Rows.Count; i++)
                {
                    
                    //if the LogInAs user is chosen it will also be shown when the user is logged in 
                    if (dtbl.Rows[i]["LogInAs"].ToString()== LogInAsValue)
                    {
                        MessageBox.Show("Logged In as " + dtbl.Rows[i][3]);
                        //if user logged in as staffuser
                        if (cmbUserType.SelectedIndex == 0)
                        {
                            StaffWelcomeForm staffWelcomeForm = new StaffWelcomeForm();
                            this.Hide();

                            staffWelcomeForm.Show();
                        }
                        //if user logged in as secretaryuser
                        else 
                        {
                            SecretaryWelcomeForm secretaryWelcomeForm = new SecretaryWelcomeForm();
                            this.Hide();

                            secretaryWelcomeForm.Show();

                        }

                    }
                }
               
            }
            else
            //message box is shown if incorrect or no credentials will be enterd
            {
                MessageBox.Show("Check username & Password");

            }
            //username and password textboxes will be cleared
     
            txtPassword.Clear();
            txtUsername.Clear();

        }


        private void cmbUserType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
