﻿namespace GUI
{
    partial class StaffPartsAndRepairForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblPartsDetailsTitle = new System.Windows.Forms.Label();
            this.lblAddParts = new System.Windows.Forms.Label();
            this.lblRepairNotes = new System.Windows.Forms.Label();
            this.txtRepairNotes = new System.Windows.Forms.RichTextBox();
            this.lblJobNumber = new System.Windows.Forms.Label();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.lblSurname = new System.Windows.Forms.Label();
            this.lblCurrentStatusTitle = new System.Windows.Forms.Label();
            this.txtRepairedBy = new System.Windows.Forms.TextBox();
            this.lblRepairedBy = new System.Windows.Forms.Label();
            this.pnlCurrentStatus = new System.Windows.Forms.Panel();
            this.txtCurrentStatusD = new System.Windows.Forms.TextBox();
            this.cmbCurrentStatus = new System.Windows.Forms.ComboBox();
            this.txtNumberPlate = new System.Windows.Forms.TextBox();
            this.lblNumberPlate = new System.Windows.Forms.Label();
            this.txtVehicleModel = new System.Windows.Forms.TextBox();
            this.lblDetailsTitle = new System.Windows.Forms.Label();
            this.txtSurname = new System.Windows.Forms.TextBox();
            this.lblVehicleModel = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.pnlDetails = new System.Windows.Forms.Panel();
            this.lblLoggedAs = new System.Windows.Forms.Label();
            this.txtJobNumber = new System.Windows.Forms.TextBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtPartNumber = new System.Windows.Forms.TextBox();
            this.txtPartDescription = new System.Windows.Forms.TextBox();
            this.txtQuantity = new System.Windows.Forms.TextBox();
            this.txtBrand = new System.Windows.Forms.TextBox();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.lblPartNumber = new System.Windows.Forms.Label();
            this.lblPartDescription = new System.Windows.Forms.Label();
            this.lblQuantity = new System.Windows.Forms.Label();
            this.lblBrand = new System.Windows.Forms.Label();
            this.lblPrice = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnAddPart = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.pnlCurrentStatus.SuspendLayout();
            this.pnlDetails.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblPartsDetailsTitle
            // 
            this.lblPartsDetailsTitle.AutoSize = true;
            this.lblPartsDetailsTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPartsDetailsTitle.Location = new System.Drawing.Point(24, 271);
            this.lblPartsDetailsTitle.Name = "lblPartsDetailsTitle";
            this.lblPartsDetailsTitle.Size = new System.Drawing.Size(238, 24);
            this.lblPartsDetailsTitle.TabIndex = 57;
            this.lblPartsDetailsTitle.Text = "Parts and Repair Details:";
            this.lblPartsDetailsTitle.Click += new System.EventHandler(this.lblSurveyDetailsTitle_Click);
            // 
            // lblAddParts
            // 
            this.lblAddParts.AutoSize = true;
            this.lblAddParts.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddParts.Location = new System.Drawing.Point(28, 488);
            this.lblAddParts.Name = "lblAddParts";
            this.lblAddParts.Size = new System.Drawing.Size(86, 18);
            this.lblAddParts.TabIndex = 19;
            this.lblAddParts.Text = "Add Parts:";
            this.lblAddParts.Click += new System.EventHandler(this.lblPartsList_Click);
            // 
            // lblRepairNotes
            // 
            this.lblRepairNotes.AutoSize = true;
            this.lblRepairNotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRepairNotes.Location = new System.Drawing.Point(25, 313);
            this.lblRepairNotes.Name = "lblRepairNotes";
            this.lblRepairNotes.Size = new System.Drawing.Size(112, 18);
            this.lblRepairNotes.TabIndex = 17;
            this.lblRepairNotes.Text = "Repair Notes:";
            // 
            // txtRepairNotes
            // 
            this.txtRepairNotes.Location = new System.Drawing.Point(28, 349);
            this.txtRepairNotes.Name = "txtRepairNotes";
            this.txtRepairNotes.Size = new System.Drawing.Size(1014, 93);
            this.txtRepairNotes.TabIndex = 0;
            this.txtRepairNotes.Text = "";
            // 
            // lblJobNumber
            // 
            this.lblJobNumber.AutoSize = true;
            this.lblJobNumber.Location = new System.Drawing.Point(25, 41);
            this.lblJobNumber.Name = "lblJobNumber";
            this.lblJobNumber.Size = new System.Drawing.Size(89, 17);
            this.lblJobNumber.TabIndex = 45;
            this.lblJobNumber.Text = "Job Number:";
            // 
            // lblSurname
            // 
            this.lblSurname.AutoSize = true;
            this.lblSurname.Location = new System.Drawing.Point(12, 78);
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Size = new System.Drawing.Size(69, 17);
            this.lblSurname.TabIndex = 16;
            this.lblSurname.Text = "Surname:";
            // 
            // lblCurrentStatusTitle
            // 
            this.lblCurrentStatusTitle.AutoSize = true;
            this.lblCurrentStatusTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentStatusTitle.Location = new System.Drawing.Point(662, 82);
            this.lblCurrentStatusTitle.Name = "lblCurrentStatusTitle";
            this.lblCurrentStatusTitle.Size = new System.Drawing.Size(147, 24);
            this.lblCurrentStatusTitle.TabIndex = 53;
            this.lblCurrentStatusTitle.Text = "Current Status:";
            // 
            // txtRepairedBy
            // 
            this.txtRepairedBy.Location = new System.Drawing.Point(897, 675);
            this.txtRepairedBy.Name = "txtRepairedBy";
            this.txtRepairedBy.Size = new System.Drawing.Size(165, 22);
            this.txtRepairedBy.TabIndex = 1;
            this.txtRepairedBy.TextChanged += new System.EventHandler(this.txtSurveyCompletedBy_TextChanged);
            this.txtRepairedBy.Leave += new System.EventHandler(this.txtRepairedBy_Leave);
            // 
            // lblRepairedBy
            // 
            this.lblRepairedBy.AutoSize = true;
            this.lblRepairedBy.Location = new System.Drawing.Point(780, 678);
            this.lblRepairedBy.Name = "lblRepairedBy";
            this.lblRepairedBy.Size = new System.Drawing.Size(90, 17);
            this.lblRepairedBy.TabIndex = 51;
            this.lblRepairedBy.Text = "Repaired By:";
            this.lblRepairedBy.Click += new System.EventHandler(this.lblJSurveyCompletedBy_Click);
            // 
            // pnlCurrentStatus
            // 
            this.pnlCurrentStatus.Controls.Add(this.txtCurrentStatusD);
            this.pnlCurrentStatus.Controls.Add(this.cmbCurrentStatus);
            this.pnlCurrentStatus.Location = new System.Drawing.Point(666, 107);
            this.pnlCurrentStatus.Name = "pnlCurrentStatus";
            this.pnlCurrentStatus.Size = new System.Drawing.Size(370, 147);
            this.pnlCurrentStatus.TabIndex = 50;
            // 
            // txtCurrentStatusD
            // 
            this.txtCurrentStatusD.Location = new System.Drawing.Point(44, 78);
            this.txtCurrentStatusD.Name = "txtCurrentStatusD";
            this.txtCurrentStatusD.ReadOnly = true;
            this.txtCurrentStatusD.Size = new System.Drawing.Size(291, 22);
            this.txtCurrentStatusD.TabIndex = 2;
            // 
            // cmbCurrentStatus
            // 
            this.cmbCurrentStatus.FormattingEnabled = true;
            this.cmbCurrentStatus.Location = new System.Drawing.Point(44, 34);
            this.cmbCurrentStatus.Name = "cmbCurrentStatus";
            this.cmbCurrentStatus.Size = new System.Drawing.Size(291, 24);
            this.cmbCurrentStatus.TabIndex = 0;
            this.cmbCurrentStatus.Text = "Select";
            this.cmbCurrentStatus.SelectedIndexChanged += new System.EventHandler(this.cmbCurrentStatus_SelectedIndexChanged);
            this.cmbCurrentStatus.TextChanged += new System.EventHandler(this.cmbCurrentStatus_TextChanged);
            // 
            // txtNumberPlate
            // 
            this.txtNumberPlate.Location = new System.Drawing.Point(427, 75);
            this.txtNumberPlate.Name = "txtNumberPlate";
            this.txtNumberPlate.ReadOnly = true;
            this.txtNumberPlate.Size = new System.Drawing.Size(151, 22);
            this.txtNumberPlate.TabIndex = 15;
            // 
            // lblNumberPlate
            // 
            this.lblNumberPlate.AutoSize = true;
            this.lblNumberPlate.Location = new System.Drawing.Point(318, 76);
            this.lblNumberPlate.Name = "lblNumberPlate";
            this.lblNumberPlate.Size = new System.Drawing.Size(98, 17);
            this.lblNumberPlate.TabIndex = 14;
            this.lblNumberPlate.Text = "Number Plate:";
            // 
            // txtVehicleModel
            // 
            this.txtVehicleModel.Location = new System.Drawing.Point(427, 21);
            this.txtVehicleModel.Name = "txtVehicleModel";
            this.txtVehicleModel.ReadOnly = true;
            this.txtVehicleModel.Size = new System.Drawing.Size(151, 22);
            this.txtVehicleModel.TabIndex = 13;
            // 
            // lblDetailsTitle
            // 
            this.lblDetailsTitle.AutoSize = true;
            this.lblDetailsTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDetailsTitle.Location = new System.Drawing.Point(24, 82);
            this.lblDetailsTitle.Name = "lblDetailsTitle";
            this.lblDetailsTitle.Size = new System.Drawing.Size(291, 24);
            this.lblDetailsTitle.TabIndex = 49;
            this.lblDetailsTitle.Text = "Customer and Vehicle Details:";
            // 
            // txtSurname
            // 
            this.txtSurname.Location = new System.Drawing.Point(107, 78);
            this.txtSurname.Name = "txtSurname";
            this.txtSurname.ReadOnly = true;
            this.txtSurname.Size = new System.Drawing.Size(124, 22);
            this.txtSurname.TabIndex = 17;
            // 
            // lblVehicleModel
            // 
            this.lblVehicleModel.AutoSize = true;
            this.lblVehicleModel.Location = new System.Drawing.Point(318, 24);
            this.lblVehicleModel.Name = "lblVehicleModel";
            this.lblVehicleModel.Size = new System.Drawing.Size(100, 17);
            this.lblVehicleModel.TabIndex = 12;
            this.lblVehicleModel.Text = "Vehicle Model:";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(107, 21);
            this.txtName.Name = "txtName";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(124, 22);
            this.txtName.TabIndex = 5;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(12, 21);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(49, 17);
            this.lblName.TabIndex = 4;
            this.lblName.Text = "Name:";
            // 
            // pnlDetails
            // 
            this.pnlDetails.Controls.Add(this.txtSurname);
            this.pnlDetails.Controls.Add(this.lblSurname);
            this.pnlDetails.Controls.Add(this.txtNumberPlate);
            this.pnlDetails.Controls.Add(this.lblNumberPlate);
            this.pnlDetails.Controls.Add(this.txtVehicleModel);
            this.pnlDetails.Controls.Add(this.lblVehicleModel);
            this.pnlDetails.Controls.Add(this.txtName);
            this.pnlDetails.Controls.Add(this.lblName);
            this.pnlDetails.Location = new System.Drawing.Point(13, 107);
            this.pnlDetails.Name = "pnlDetails";
            this.pnlDetails.Size = new System.Drawing.Size(625, 147);
            this.pnlDetails.TabIndex = 48;
            // 
            // lblLoggedAs
            // 
            this.lblLoggedAs.AutoSize = true;
            this.lblLoggedAs.Location = new System.Drawing.Point(920, 786);
            this.lblLoggedAs.Name = "lblLoggedAs";
            this.lblLoggedAs.Size = new System.Drawing.Size(142, 17);
            this.lblLoggedAs.TabIndex = 54;
            this.lblLoggedAs.Text = "Logged In: Staff User";
            // 
            // txtJobNumber
            // 
            this.txtJobNumber.Location = new System.Drawing.Point(120, 41);
            this.txtJobNumber.Name = "txtJobNumber";
            this.txtJobNumber.ReadOnly = true;
            this.txtJobNumber.Size = new System.Drawing.Size(100, 22);
            this.txtJobNumber.TabIndex = 46;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Image = global::GUI.Properties.Resources.edit;
            this.btnUpdate.Location = new System.Drawing.Point(564, 724);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 42);
            this.btnUpdate.TabIndex = 64;
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::GUI.Properties.Resources.search_icon;
            this.btnSearch.Location = new System.Drawing.Point(440, 724);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 42);
            this.btnSearch.TabIndex = 63;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtPartNumber
            // 
            this.txtPartNumber.Location = new System.Drawing.Point(136, 12);
            this.txtPartNumber.Name = "txtPartNumber";
            this.txtPartNumber.Size = new System.Drawing.Size(151, 22);
            this.txtPartNumber.TabIndex = 0;
            // 
            // txtPartDescription
            // 
            this.txtPartDescription.Location = new System.Drawing.Point(536, 12);
            this.txtPartDescription.Name = "txtPartDescription";
            this.txtPartDescription.Size = new System.Drawing.Size(478, 22);
            this.txtPartDescription.TabIndex = 1;
            // 
            // txtQuantity
            // 
            this.txtQuantity.Location = new System.Drawing.Point(136, 53);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new System.Drawing.Size(151, 22);
            this.txtQuantity.TabIndex = 2;
            // 
            // txtBrand
            // 
            this.txtBrand.Location = new System.Drawing.Point(536, 52);
            this.txtBrand.Name = "txtBrand";
            this.txtBrand.Size = new System.Drawing.Size(151, 22);
            this.txtBrand.TabIndex = 3;
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new System.Drawing.Point(536, 98);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(151, 22);
            this.txtPrice.TabIndex = 4;
            // 
            // lblPartNumber
            // 
            this.lblPartNumber.AutoSize = true;
            this.lblPartNumber.Location = new System.Drawing.Point(36, 12);
            this.lblPartNumber.Name = "lblPartNumber";
            this.lblPartNumber.Size = new System.Drawing.Size(88, 17);
            this.lblPartNumber.TabIndex = 70;
            this.lblPartNumber.Text = "PartNumber:";
            // 
            // lblPartDescription
            // 
            this.lblPartDescription.AutoSize = true;
            this.lblPartDescription.Location = new System.Drawing.Point(426, 12);
            this.lblPartDescription.Name = "lblPartDescription";
            this.lblPartDescription.Size = new System.Drawing.Size(109, 17);
            this.lblPartDescription.TabIndex = 71;
            this.lblPartDescription.Text = "PartDescription:";
            // 
            // lblQuantity
            // 
            this.lblQuantity.AutoSize = true;
            this.lblQuantity.Location = new System.Drawing.Point(44, 58);
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Size = new System.Drawing.Size(65, 17);
            this.lblQuantity.TabIndex = 72;
            this.lblQuantity.Text = "Quantity:";
            // 
            // lblBrand
            // 
            this.lblBrand.AutoSize = true;
            this.lblBrand.Location = new System.Drawing.Point(480, 53);
            this.lblBrand.Name = "lblBrand";
            this.lblBrand.Size = new System.Drawing.Size(50, 17);
            this.lblBrand.TabIndex = 73;
            this.lblBrand.Text = "Brand:";
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Location = new System.Drawing.Point(480, 98);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(44, 17);
            this.lblPrice.TabIndex = 74;
            this.lblPrice.Text = "Price:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnAddPart);
            this.panel1.Controls.Add(this.lblPrice);
            this.panel1.Controls.Add(this.lblBrand);
            this.panel1.Controls.Add(this.lblQuantity);
            this.panel1.Controls.Add(this.lblPartDescription);
            this.panel1.Controls.Add(this.lblPartNumber);
            this.panel1.Controls.Add(this.txtPrice);
            this.panel1.Controls.Add(this.txtBrand);
            this.panel1.Controls.Add(this.txtQuantity);
            this.panel1.Controls.Add(this.txtPartDescription);
            this.panel1.Controls.Add(this.txtPartNumber);
            this.panel1.Location = new System.Drawing.Point(28, 509);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1034, 148);
            this.panel1.TabIndex = 75;
            // 
            // btnAddPart
            // 
            this.btnAddPart.Image = global::GUI.Properties.Resources.add;
            this.btnAddPart.Location = new System.Drawing.Point(802, 58);
            this.btnAddPart.Name = "btnAddPart";
            this.btnAddPart.Size = new System.Drawing.Size(60, 35);
            this.btnAddPart.TabIndex = 75;
            this.btnAddPart.UseVisualStyleBackColor = true;
            this.btnAddPart.Click += new System.EventHandler(this.btnAddPart_Click);
            // 
            // StaffPartsAndRepairForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1132, 822);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblAddParts);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.txtRepairNotes);
            this.Controls.Add(this.lblRepairNotes);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.lblPartsDetailsTitle);
            this.Controls.Add(this.lblJobNumber);
            this.Controls.Add(this.lblCurrentStatusTitle);
            this.Controls.Add(this.txtRepairedBy);
            this.Controls.Add(this.lblRepairedBy);
            this.Controls.Add(this.pnlCurrentStatus);
            this.Controls.Add(this.lblDetailsTitle);
            this.Controls.Add(this.pnlDetails);
            this.Controls.Add(this.lblLoggedAs);
            this.Controls.Add(this.txtJobNumber);
            this.Name = "StaffPartsAndRepairForm";
            this.Text = "PartsAndRepairForm";
            this.Load += new System.EventHandler(this.StaffPartsAndRepairForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.pnlCurrentStatus.ResumeLayout(false);
            this.pnlCurrentStatus.PerformLayout();
            this.pnlDetails.ResumeLayout(false);
            this.pnlDetails.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPartsDetailsTitle;
        private System.Windows.Forms.Label lblRepairNotes;
        private System.Windows.Forms.RichTextBox txtRepairNotes;
        private System.Windows.Forms.Label lblJobNumber;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.Label lblSurname;
        private System.Windows.Forms.Label lblCurrentStatusTitle;
        private System.Windows.Forms.TextBox txtRepairedBy;
        private System.Windows.Forms.Label lblRepairedBy;
        private System.Windows.Forms.Panel pnlCurrentStatus;
        private System.Windows.Forms.ComboBox cmbCurrentStatus;
        private System.Windows.Forms.TextBox txtNumberPlate;
        private System.Windows.Forms.Label lblNumberPlate;
        private System.Windows.Forms.TextBox txtVehicleModel;
        private System.Windows.Forms.Label lblDetailsTitle;
        private System.Windows.Forms.TextBox txtSurname;
        private System.Windows.Forms.Label lblVehicleModel;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Panel pnlDetails;
        private System.Windows.Forms.Label lblLoggedAs;
        private System.Windows.Forms.TextBox txtJobNumber;
        private System.Windows.Forms.Label lblAddParts;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtPartNumber;
        private System.Windows.Forms.TextBox txtPartDescription;
        private System.Windows.Forms.TextBox txtQuantity;
        private System.Windows.Forms.TextBox txtBrand;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.Label lblPartNumber;
        private System.Windows.Forms.Label lblPartDescription;
        private System.Windows.Forms.Label lblQuantity;
        private System.Windows.Forms.Label lblBrand;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnAddPart;
        private System.Windows.Forms.TextBox txtCurrentStatusD;
    }
}