﻿namespace GUI
{
    partial class StaffDeleteJobForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbDeteteJob = new System.Windows.Forms.PictureBox();
            this.lblJobNumber = new System.Windows.Forms.Label();
            this.txtDeleteJob = new System.Windows.Forms.TextBox();
            this.btnDeleteJob = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbDeteteJob)).BeginInit();
            this.SuspendLayout();
            // 
            // pbDeteteJob
            // 
            this.pbDeteteJob.Image = global::GUI.Properties.Resources.ASmanagment_App;
            this.pbDeteteJob.Location = new System.Drawing.Point(185, 38);
            this.pbDeteteJob.Name = "pbDeteteJob";
            this.pbDeteteJob.Size = new System.Drawing.Size(240, 222);
            this.pbDeteteJob.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbDeteteJob.TabIndex = 17;
            this.pbDeteteJob.TabStop = false;
            // 
            // lblJobNumber
            // 
            this.lblJobNumber.AutoSize = true;
            this.lblJobNumber.Location = new System.Drawing.Point(177, 299);
            this.lblJobNumber.Name = "lblJobNumber";
            this.lblJobNumber.Size = new System.Drawing.Size(89, 17);
            this.lblJobNumber.TabIndex = 16;
            this.lblJobNumber.Text = "Job Number:";
            // 
            // txtDeleteJob
            // 
            this.txtDeleteJob.Location = new System.Drawing.Point(287, 299);
            this.txtDeleteJob.Name = "txtDeleteJob";
            this.txtDeleteJob.Size = new System.Drawing.Size(138, 22);
            this.txtDeleteJob.TabIndex = 0;
            // 
            // btnDeleteJob
            // 
            this.btnDeleteJob.Location = new System.Drawing.Point(230, 351);
            this.btnDeleteJob.Name = "btnDeleteJob";
            this.btnDeleteJob.Size = new System.Drawing.Size(146, 62);
            this.btnDeleteJob.TabIndex = 1;
            this.btnDeleteJob.Text = "Delete Job";
            this.btnDeleteJob.UseVisualStyleBackColor = true;
            this.btnDeleteJob.Click += new System.EventHandler(this.btnDeleteJob_Click);
            // 
            // StaffDeleteJobForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(602, 450);
            this.Controls.Add(this.pbDeteteJob);
            this.Controls.Add(this.lblJobNumber);
            this.Controls.Add(this.txtDeleteJob);
            this.Controls.Add(this.btnDeleteJob);
            this.Name = "StaffDeleteJobForm";
            this.Text = "Delete Job ";
            this.Load += new System.EventHandler(this.StaffDeleteJobForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbDeteteJob)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbDeteteJob;
        private System.Windows.Forms.Label lblJobNumber;
        private System.Windows.Forms.TextBox txtDeleteJob;
        private System.Windows.Forms.Button btnDeleteJob;
    }
}