﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace GUI
{
    public partial class SecretaryDeleteJobForm : Form
    {
        public SecretaryDeleteJobForm()
        {
            InitializeComponent();
        }

        private void SecretaryDeleteJobForm_Load(object sender, EventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {

                bool success = AutoShopService.DeleteSurveyDetails(int.Parse(this.txtDeleteJob.Text));
                //Showing messagebox to the user informing him that entry was successful
                MessageBox.Show("Job Deleted", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);

                bool success1 = AutoShopService.DeleteJobDetails(int.Parse(this.txtDeleteJob.Text));
                //Showing messagebox to the user informing him that entry was successful
                MessageBox.Show("Job Deleted", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);


            }
            catch (SqlException ex)
            {

                MessageBox.Show("Error Encountered whilst Deleting Job", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw ex;
            }

        }
    }
}
