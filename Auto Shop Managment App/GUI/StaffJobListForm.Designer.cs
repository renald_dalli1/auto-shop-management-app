﻿namespace GUI
{
    partial class StaffJobListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExport = new System.Windows.Forms.Button();
            this.dgJobList = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgJobList)).BeginInit();
            this.SuspendLayout();
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(829, 12);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(266, 58);
            this.btnExport.TabIndex = 49;
            this.btnExport.Text = "Export to Excel";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // dgJobList
            // 
            this.dgJobList.AllowUserToOrderColumns = true;
            this.dgJobList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgJobList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgJobList.Location = new System.Drawing.Point(40, 89);
            this.dgJobList.Name = "dgJobList";
            this.dgJobList.RowTemplate.Height = 24;
            this.dgJobList.Size = new System.Drawing.Size(1855, 860);
            this.dgJobList.TabIndex = 53;
            this.dgJobList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgJobList_CellContentClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1736, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 17);
            this.label1.TabIndex = 64;
            this.label1.Text = "Logged In: StaffUser";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // StaffJobListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1943, 968);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgJobList);
            this.Controls.Add(this.btnExport);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "StaffJobListForm";
            this.Text = "Job List ";
            this.Load += new System.EventHandler(this.StaffJobListForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgJobList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.DataGridView dgJobList;
        private System.Windows.Forms.Label label1;
    }
}