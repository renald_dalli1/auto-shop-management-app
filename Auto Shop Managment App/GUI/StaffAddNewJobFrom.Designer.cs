﻿namespace GUI
{
    partial class StaffAddNewJobForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblJobNumber = new System.Windows.Forms.Label();
            this.txtJobNumber = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.txtIdCard = new System.Windows.Forms.TextBox();
            this.lblIDCard = new System.Windows.Forms.Label();
            this.lblDateCreated = new System.Windows.Forms.Label();
            this.lblContact = new System.Windows.Forms.Label();
            this.txtNumberPlate = new System.Windows.Forms.TextBox();
            this.lblNumberPlate = new System.Windows.Forms.Label();
            this.txtVehicleModel = new System.Windows.Forms.TextBox();
            this.lblVehicleModel = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.txtSurname = new System.Windows.Forms.TextBox();
            this.lblSurname = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.lblAddress = new System.Windows.Forms.Label();
            this.pnlDetails = new System.Windows.Forms.Panel();
            this.txtContact = new System.Windows.Forms.MaskedTextBox();
            this.lblDetailsTitle = new System.Windows.Forms.Label();
            this.pnlCurrentStatus = new System.Windows.Forms.Panel();
            this.txtCurrentStatusD = new System.Windows.Forms.TextBox();
            this.cmbCurrentStatus = new System.Windows.Forms.ComboBox();
            this.txtJobCreatedBy = new System.Windows.Forms.TextBox();
            this.lblJobCreatedBy = new System.Windows.Forms.Label();
            this.lblCurrentStatusTitle = new System.Windows.Forms.Label();
            this.lblLoggedAs = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.txtDateCreated = new System.Windows.Forms.MaskedTextBox();
            this.pnlDetails.SuspendLayout();
            this.pnlCurrentStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblJobNumber
            // 
            this.lblJobNumber.AutoSize = true;
            this.lblJobNumber.Location = new System.Drawing.Point(48, 45);
            this.lblJobNumber.Name = "lblJobNumber";
            this.lblJobNumber.Size = new System.Drawing.Size(89, 17);
            this.lblJobNumber.TabIndex = 2;
            this.lblJobNumber.Text = "Job Number:";
            this.lblJobNumber.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtJobNumber
            // 
            this.txtJobNumber.Location = new System.Drawing.Point(143, 45);
            this.txtJobNumber.Name = "txtJobNumber";
            this.txtJobNumber.ReadOnly = true;
            this.txtJobNumber.Size = new System.Drawing.Size(100, 22);
            this.txtJobNumber.TabIndex = 3;
            this.txtJobNumber.Leave += new System.EventHandler(this.txtJobNumber_Leave);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(107, 21);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(100, 22);
            this.txtName.TabIndex = 0;
            this.txtName.TextChanged += new System.EventHandler(this.txtName_TextChanged);
            this.txtName.Leave += new System.EventHandler(this.txtName_Leave);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(12, 21);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(49, 17);
            this.lblName.TabIndex = 4;
            this.lblName.Text = "Name:";
            this.lblName.Click += new System.EventHandler(this.label2_Click);
            // 
            // txtIdCard
            // 
            this.txtIdCard.Location = new System.Drawing.Point(107, 91);
            this.txtIdCard.Name = "txtIdCard";
            this.txtIdCard.Size = new System.Drawing.Size(100, 22);
            this.txtIdCard.TabIndex = 2;
            this.txtIdCard.Leave += new System.EventHandler(this.txtIdCard_Leave);
            // 
            // lblIDCard
            // 
            this.lblIDCard.AutoSize = true;
            this.lblIDCard.Location = new System.Drawing.Point(12, 91);
            this.lblIDCard.Name = "lblIDCard";
            this.lblIDCard.Size = new System.Drawing.Size(59, 17);
            this.lblIDCard.TabIndex = 6;
            this.lblIDCard.Text = "ID Card:";
            // 
            // lblDateCreated
            // 
            this.lblDateCreated.AutoSize = true;
            this.lblDateCreated.Location = new System.Drawing.Point(755, 319);
            this.lblDateCreated.Name = "lblDateCreated";
            this.lblDateCreated.Size = new System.Drawing.Size(96, 17);
            this.lblDateCreated.TabIndex = 10;
            this.lblDateCreated.Text = "Date Created:";
            // 
            // lblContact
            // 
            this.lblContact.AutoSize = true;
            this.lblContact.Location = new System.Drawing.Point(12, 142);
            this.lblContact.Name = "lblContact";
            this.lblContact.Size = new System.Drawing.Size(60, 17);
            this.lblContact.TabIndex = 8;
            this.lblContact.Text = "Contact:";
            // 
            // txtNumberPlate
            // 
            this.txtNumberPlate.Location = new System.Drawing.Point(448, 138);
            this.txtNumberPlate.Name = "txtNumberPlate";
            this.txtNumberPlate.Size = new System.Drawing.Size(151, 22);
            this.txtNumberPlate.TabIndex = 5;
            this.txtNumberPlate.Leave += new System.EventHandler(this.txtNumberPlate_Leave);
            // 
            // lblNumberPlate
            // 
            this.lblNumberPlate.AutoSize = true;
            this.lblNumberPlate.Location = new System.Drawing.Point(339, 139);
            this.lblNumberPlate.Name = "lblNumberPlate";
            this.lblNumberPlate.Size = new System.Drawing.Size(98, 17);
            this.lblNumberPlate.TabIndex = 14;
            this.lblNumberPlate.Text = "Number Plate:";
            // 
            // txtVehicleModel
            // 
            this.txtVehicleModel.Location = new System.Drawing.Point(448, 88);
            this.txtVehicleModel.Name = "txtVehicleModel";
            this.txtVehicleModel.Size = new System.Drawing.Size(151, 22);
            this.txtVehicleModel.TabIndex = 3;
            this.txtVehicleModel.Leave += new System.EventHandler(this.txtVehicleModel_Leave);
            // 
            // lblVehicleModel
            // 
            this.lblVehicleModel.AutoSize = true;
            this.lblVehicleModel.Location = new System.Drawing.Point(339, 91);
            this.lblVehicleModel.Name = "lblVehicleModel";
            this.lblVehicleModel.Size = new System.Drawing.Size(100, 17);
            this.lblVehicleModel.TabIndex = 12;
            this.lblVehicleModel.Text = "Vehicle Model:";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(107, 207);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(492, 22);
            this.txtEmail.TabIndex = 6;
            this.txtEmail.Leave += new System.EventHandler(this.txtEmail_Leave);
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(12, 207);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(46, 17);
            this.lblEmail.TabIndex = 18;
            this.lblEmail.Text = "Email:";
            // 
            // txtSurname
            // 
            this.txtSurname.Location = new System.Drawing.Point(340, 22);
            this.txtSurname.Name = "txtSurname";
            this.txtSurname.Size = new System.Drawing.Size(124, 22);
            this.txtSurname.TabIndex = 1;
            this.txtSurname.Leave += new System.EventHandler(this.txtSurname_Leave);
            // 
            // lblSurname
            // 
            this.lblSurname.AutoSize = true;
            this.lblSurname.Location = new System.Drawing.Point(245, 22);
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Size = new System.Drawing.Size(69, 17);
            this.lblSurname.TabIndex = 16;
            this.lblSurname.Text = "Surname:";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(107, 263);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(492, 22);
            this.txtAddress.TabIndex = 7;
            this.txtAddress.Leave += new System.EventHandler(this.txtAddress_Leave);
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(12, 263);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(64, 17);
            this.lblAddress.TabIndex = 20;
            this.lblAddress.Text = "Address:";
            // 
            // pnlDetails
            // 
            this.pnlDetails.Controls.Add(this.txtContact);
            this.pnlDetails.Controls.Add(this.txtAddress);
            this.pnlDetails.Controls.Add(this.lblAddress);
            this.pnlDetails.Controls.Add(this.txtEmail);
            this.pnlDetails.Controls.Add(this.lblEmail);
            this.pnlDetails.Controls.Add(this.txtSurname);
            this.pnlDetails.Controls.Add(this.lblSurname);
            this.pnlDetails.Controls.Add(this.txtNumberPlate);
            this.pnlDetails.Controls.Add(this.lblNumberPlate);
            this.pnlDetails.Controls.Add(this.txtVehicleModel);
            this.pnlDetails.Controls.Add(this.lblVehicleModel);
            this.pnlDetails.Controls.Add(this.lblContact);
            this.pnlDetails.Controls.Add(this.txtIdCard);
            this.pnlDetails.Controls.Add(this.lblIDCard);
            this.pnlDetails.Controls.Add(this.txtName);
            this.pnlDetails.Controls.Add(this.lblName);
            this.pnlDetails.Location = new System.Drawing.Point(36, 136);
            this.pnlDetails.Name = "pnlDetails";
            this.pnlDetails.Size = new System.Drawing.Size(625, 298);
            this.pnlDetails.TabIndex = 22;
            // 
            // txtContact
            // 
            this.txtContact.Location = new System.Drawing.Point(107, 142);
            this.txtContact.Margin = new System.Windows.Forms.Padding(4);
            this.txtContact.Mask = "00000000";
            this.txtContact.Name = "txtContact";
            this.txtContact.Size = new System.Drawing.Size(135, 22);
            this.txtContact.TabIndex = 4;
            // 
            // lblDetailsTitle
            // 
            this.lblDetailsTitle.AutoSize = true;
            this.lblDetailsTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDetailsTitle.Location = new System.Drawing.Point(47, 111);
            this.lblDetailsTitle.Name = "lblDetailsTitle";
            this.lblDetailsTitle.Size = new System.Drawing.Size(291, 24);
            this.lblDetailsTitle.TabIndex = 23;
            this.lblDetailsTitle.Text = "Customer and Vehicle Details:";
            this.lblDetailsTitle.Click += new System.EventHandler(this.label11_Click);
            // 
            // pnlCurrentStatus
            // 
            this.pnlCurrentStatus.Controls.Add(this.txtCurrentStatusD);
            this.pnlCurrentStatus.Controls.Add(this.cmbCurrentStatus);
            this.pnlCurrentStatus.Location = new System.Drawing.Point(689, 136);
            this.pnlCurrentStatus.Name = "pnlCurrentStatus";
            this.pnlCurrentStatus.Size = new System.Drawing.Size(370, 147);
            this.pnlCurrentStatus.TabIndex = 24;
            // 
            // txtCurrentStatusD
            // 
            this.txtCurrentStatusD.Location = new System.Drawing.Point(44, 101);
            this.txtCurrentStatusD.Name = "txtCurrentStatusD";
            this.txtCurrentStatusD.ReadOnly = true;
            this.txtCurrentStatusD.Size = new System.Drawing.Size(291, 22);
            this.txtCurrentStatusD.TabIndex = 1;
            // 
            // cmbCurrentStatus
            // 
            this.cmbCurrentStatus.FormattingEnabled = true;
            this.cmbCurrentStatus.Location = new System.Drawing.Point(44, 47);
            this.cmbCurrentStatus.Name = "cmbCurrentStatus";
            this.cmbCurrentStatus.Size = new System.Drawing.Size(291, 24);
            this.cmbCurrentStatus.TabIndex = 0;
            this.cmbCurrentStatus.Text = "Select";
            this.cmbCurrentStatus.Leave += new System.EventHandler(this.cmbCurrentStatus_Leave);
            // 
            // txtJobCreatedBy
            // 
            this.txtJobCreatedBy.Location = new System.Drawing.Point(859, 370);
            this.txtJobCreatedBy.Name = "txtJobCreatedBy";
            this.txtJobCreatedBy.Size = new System.Drawing.Size(165, 22);
            this.txtJobCreatedBy.TabIndex = 1;
            this.txtJobCreatedBy.Leave += new System.EventHandler(this.txtJobCreatedBy_Leave);
            // 
            // lblJobCreatedBy
            // 
            this.lblJobCreatedBy.AutoSize = true;
            this.lblJobCreatedBy.Location = new System.Drawing.Point(748, 373);
            this.lblJobCreatedBy.Name = "lblJobCreatedBy";
            this.lblJobCreatedBy.Size = new System.Drawing.Size(109, 17);
            this.lblJobCreatedBy.TabIndex = 25;
            this.lblJobCreatedBy.Text = "Job Created By:";
            // 
            // lblCurrentStatusTitle
            // 
            this.lblCurrentStatusTitle.AutoSize = true;
            this.lblCurrentStatusTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentStatusTitle.Location = new System.Drawing.Point(685, 111);
            this.lblCurrentStatusTitle.Name = "lblCurrentStatusTitle";
            this.lblCurrentStatusTitle.Size = new System.Drawing.Size(147, 24);
            this.lblCurrentStatusTitle.TabIndex = 27;
            this.lblCurrentStatusTitle.Text = "Current Status:";
            this.lblCurrentStatusTitle.Click += new System.EventHandler(this.label12_Click);
            // 
            // lblLoggedAs
            // 
            this.lblLoggedAs.AutoSize = true;
            this.lblLoggedAs.Location = new System.Drawing.Point(917, 529);
            this.lblLoggedAs.Name = "lblLoggedAs";
            this.lblLoggedAs.Size = new System.Drawing.Size(142, 17);
            this.lblLoggedAs.TabIndex = 28;
            this.lblLoggedAs.Text = "Logged In: Staff User";
            // 
            // btnClear
            // 
            this.btnClear.Image = global::GUI.Properties.Resources.Clear_icon;
            this.btnClear.Location = new System.Drawing.Point(643, 471);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 42);
            this.btnClear.TabIndex = 66;
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Image = global::GUI.Properties.Resources.add;
            this.btnAdd.Location = new System.Drawing.Point(425, 471);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 42);
            this.btnAdd.TabIndex = 65;
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::GUI.Properties.Resources.search_icon;
            this.btnSearch.Location = new System.Drawing.Point(537, 471);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 42);
            this.btnSearch.TabIndex = 63;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // txtDateCreated
            // 
            this.txtDateCreated.Location = new System.Drawing.Point(859, 319);
            this.txtDateCreated.Margin = new System.Windows.Forms.Padding(4);
            this.txtDateCreated.Mask = "00/00/0000";
            this.txtDateCreated.Name = "txtDateCreated";
            this.txtDateCreated.Size = new System.Drawing.Size(165, 22);
            this.txtDateCreated.TabIndex = 0;
            this.txtDateCreated.ValidatingType = typeof(System.DateTime);
            // 
            // StaffAddNewJobForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1071, 555);
            this.Controls.Add(this.txtDateCreated);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.lblLoggedAs);
            this.Controls.Add(this.lblCurrentStatusTitle);
            this.Controls.Add(this.txtJobCreatedBy);
            this.Controls.Add(this.lblJobCreatedBy);
            this.Controls.Add(this.pnlCurrentStatus);
            this.Controls.Add(this.lblDetailsTitle);
            this.Controls.Add(this.pnlDetails);
            this.Controls.Add(this.txtJobNumber);
            this.Controls.Add(this.lblJobNumber);
            this.Controls.Add(this.lblDateCreated);
            this.Name = "StaffAddNewJobForm";
            this.Text = "Add New Job";
            this.Load += new System.EventHandler(this.AddNewJobForm_Load);
            this.pnlDetails.ResumeLayout(false);
            this.pnlDetails.PerformLayout();
            this.pnlCurrentStatus.ResumeLayout(false);
            this.pnlCurrentStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblJobNumber;
        private System.Windows.Forms.TextBox txtJobNumber;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtIdCard;
        private System.Windows.Forms.Label lblIDCard;
        private System.Windows.Forms.Label lblDateCreated;
        private System.Windows.Forms.Label lblContact;
        private System.Windows.Forms.TextBox txtNumberPlate;
        private System.Windows.Forms.Label lblNumberPlate;
        private System.Windows.Forms.TextBox txtVehicleModel;
        private System.Windows.Forms.Label lblVehicleModel;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.TextBox txtSurname;
        private System.Windows.Forms.Label lblSurname;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Panel pnlDetails;
        private System.Windows.Forms.Label lblDetailsTitle;
        private System.Windows.Forms.Panel pnlCurrentStatus;
        private System.Windows.Forms.TextBox txtJobCreatedBy;
        private System.Windows.Forms.Label lblJobCreatedBy;
        private System.Windows.Forms.Label lblCurrentStatusTitle;
        private System.Windows.Forms.ComboBox cmbCurrentStatus;
        private System.Windows.Forms.Label lblLoggedAs;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.MaskedTextBox txtDateCreated;
        private System.Windows.Forms.MaskedTextBox txtContact;
        private System.Windows.Forms.TextBox txtCurrentStatusD;
    }
}