﻿namespace GUI
{
    partial class SecretarySearchJobNumberForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbDeteteJob = new System.Windows.Forms.PictureBox();
            this.lblJobNumber = new System.Windows.Forms.Label();
            this.txtSearchJobNumber = new System.Windows.Forms.TextBox();
            this.btnSearchJob = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbDeteteJob)).BeginInit();
            this.SuspendLayout();
            // 
            // pbDeteteJob
            // 
            this.pbDeteteJob.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbDeteteJob.Image = global::GUI.Properties.Resources.ASmanagment_App;
            this.pbDeteteJob.Location = new System.Drawing.Point(159, 32);
            this.pbDeteteJob.Name = "pbDeteteJob";
            this.pbDeteteJob.Size = new System.Drawing.Size(240, 222);
            this.pbDeteteJob.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbDeteteJob.TabIndex = 14;
            this.pbDeteteJob.TabStop = false;
            // 
            // lblJobNumber
            // 
            this.lblJobNumber.AutoSize = true;
            this.lblJobNumber.Location = new System.Drawing.Point(158, 292);
            this.lblJobNumber.Name = "lblJobNumber";
            this.lblJobNumber.Size = new System.Drawing.Size(89, 17);
            this.lblJobNumber.TabIndex = 17;
            this.lblJobNumber.Text = "Job Number:";
            // 
            // txtSearchJobNumber
            // 
            this.txtSearchJobNumber.Location = new System.Drawing.Point(250, 291);
            this.txtSearchJobNumber.Name = "txtSearchJobNumber";
            this.txtSearchJobNumber.Size = new System.Drawing.Size(138, 22);
            this.txtSearchJobNumber.TabIndex = 16;
            // 
            // btnSearchJob
            // 
            this.btnSearchJob.Location = new System.Drawing.Point(219, 337);
            this.btnSearchJob.Name = "btnSearchJob";
            this.btnSearchJob.Size = new System.Drawing.Size(123, 53);
            this.btnSearchJob.TabIndex = 15;
            this.btnSearchJob.Text = "Search";
            this.btnSearchJob.UseVisualStyleBackColor = true;
            // 
            // SecretarySearchJobNumberForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(569, 450);
            this.Controls.Add(this.lblJobNumber);
            this.Controls.Add(this.txtSearchJobNumber);
            this.Controls.Add(this.btnSearchJob);
            this.Controls.Add(this.pbDeteteJob);
            this.Name = "SecretarySearchJobNumberForm";
            this.Text = "Search Job Number";
            ((System.ComponentModel.ISupportInitialize)(this.pbDeteteJob)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbDeteteJob;
        private System.Windows.Forms.Label lblJobNumber;
        private System.Windows.Forms.TextBox txtSearchJobNumber;
        private System.Windows.Forms.Button btnSearchJob;
    }
}