﻿namespace GUI
{
    partial class SecretaryWelcomeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStripStaffWelcomeForm = new System.Windows.Forms.MenuStrip();
            this.tsmiJobStatusUpdate = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEndJob = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSendEmail = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiRepairReport = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiJobList = new System.Windows.Forms.ToolStripMenuItem();
            this.lblLoggedAs = new System.Windows.Forms.Label();
            this.pbLogin = new System.Windows.Forms.PictureBox();
            this.menuStripStaffWelcomeForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogin)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStripStaffWelcomeForm
            // 
            this.menuStripStaffWelcomeForm.BackColor = System.Drawing.Color.Coral;
            this.menuStripStaffWelcomeForm.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStripStaffWelcomeForm.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiJobStatusUpdate,
            this.tsmiEndJob,
            this.tsmiSendEmail,
            this.tsmiRepairReport,
            this.tsmiJobList});
            this.menuStripStaffWelcomeForm.Location = new System.Drawing.Point(0, 0);
            this.menuStripStaffWelcomeForm.Name = "menuStripStaffWelcomeForm";
            this.menuStripStaffWelcomeForm.Size = new System.Drawing.Size(869, 28);
            this.menuStripStaffWelcomeForm.TabIndex = 0;
            this.menuStripStaffWelcomeForm.Text = "menuStrip1";
            // 
            // tsmiJobStatusUpdate
            // 
            this.tsmiJobStatusUpdate.Name = "tsmiJobStatusUpdate";
            this.tsmiJobStatusUpdate.Size = new System.Drawing.Size(141, 24);
            this.tsmiJobStatusUpdate.Text = "Job Status Update";
            this.tsmiJobStatusUpdate.Click += new System.EventHandler(this.tsmiAddNewJob_Click);
            // 
            // tsmiEndJob
            // 
            this.tsmiEndJob.Name = "tsmiEndJob";
            this.tsmiEndJob.Size = new System.Drawing.Size(73, 24);
            this.tsmiEndJob.Text = "End Job";
            this.tsmiEndJob.Click += new System.EventHandler(this.tsmiEndJob_Click);
            // 
            // tsmiSendEmail
            // 
            this.tsmiSendEmail.Name = "tsmiSendEmail";
            this.tsmiSendEmail.Size = new System.Drawing.Size(95, 24);
            this.tsmiSendEmail.Text = "Send Email";
            this.tsmiSendEmail.Click += new System.EventHandler(this.tsmiSendEmail_Click);
            // 
            // tsmiRepairReport
            // 
            this.tsmiRepairReport.Name = "tsmiRepairReport";
            this.tsmiRepairReport.Size = new System.Drawing.Size(113, 24);
            this.tsmiRepairReport.Text = "Repair Report";
            this.tsmiRepairReport.Click += new System.EventHandler(this.tsmiRepairReport_Click);
            // 
            // tsmiJobList
            // 
            this.tsmiJobList.Name = "tsmiJobList";
            this.tsmiJobList.Size = new System.Drawing.Size(70, 24);
            this.tsmiJobList.Text = "Job List";
            this.tsmiJobList.Click += new System.EventHandler(this.tsmiJobList_Click);
            // 
            // lblLoggedAs
            // 
            this.lblLoggedAs.AutoSize = true;
            this.lblLoggedAs.Location = new System.Drawing.Point(687, 447);
            this.lblLoggedAs.Name = "lblLoggedAs";
            this.lblLoggedAs.Size = new System.Drawing.Size(174, 17);
            this.lblLoggedAs.TabIndex = 4;
            this.lblLoggedAs.Text = "Logged In: Secretary User";
            this.lblLoggedAs.Click += new System.EventHandler(this.label1_Click);
            // 
            // pbLogin
            // 
            this.pbLogin.Image = global::GUI.Properties.Resources.ASmanagment_App;
            this.pbLogin.Location = new System.Drawing.Point(194, 78);
            this.pbLogin.Name = "pbLogin";
            this.pbLogin.Size = new System.Drawing.Size(496, 325);
            this.pbLogin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLogin.TabIndex = 8;
            this.pbLogin.TabStop = false;
            // 
            // SecretaryWelcomeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(869, 475);
            this.Controls.Add(this.pbLogin);
            this.Controls.Add(this.lblLoggedAs);
            this.Controls.Add(this.menuStripStaffWelcomeForm);
            this.Name = "SecretaryWelcomeForm";
            this.Text = "Auto Shop Managment App";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SecretaryWelcomeForm_FormClosing);
            this.Load += new System.EventHandler(this.SecretaryWelcomeForm_Load);
            this.menuStripStaffWelcomeForm.ResumeLayout(false);
            this.menuStripStaffWelcomeForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogin)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStripStaffWelcomeForm;
        private System.Windows.Forms.ToolStripMenuItem tsmiJobStatusUpdate;
        private System.Windows.Forms.ToolStripMenuItem tsmiEndJob;
        private System.Windows.Forms.ToolStripMenuItem tsmiSendEmail;
        private System.Windows.Forms.ToolStripMenuItem tsmiRepairReport;
        private System.Windows.Forms.ToolStripMenuItem tsmiJobList;
        private System.Windows.Forms.Label lblLoggedAs;
        private System.Windows.Forms.PictureBox pbLogin;
    }
}