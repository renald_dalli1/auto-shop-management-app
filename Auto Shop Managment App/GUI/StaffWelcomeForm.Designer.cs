﻿namespace GUI
{
    partial class StaffWelcomeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblLoggedAs = new System.Windows.Forms.Label();
            this.menuStripStaffWelcomeForm = new System.Windows.Forms.MenuStrip();
            this.tsmiAddNewJob = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSurvey = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiPartsAndRepair = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEndJob = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiUpdateJobDetails = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDeleteJob = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiJobList = new System.Windows.Forms.ToolStripMenuItem();
            this.pbLogin = new System.Windows.Forms.PictureBox();
            this.menuStripStaffWelcomeForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogin)).BeginInit();
            this.SuspendLayout();
            // 
            // lblLoggedAs
            // 
            this.lblLoggedAs.AutoSize = true;
            this.lblLoggedAs.Location = new System.Drawing.Point(719, 496);
            this.lblLoggedAs.Name = "lblLoggedAs";
            this.lblLoggedAs.Size = new System.Drawing.Size(142, 17);
            this.lblLoggedAs.TabIndex = 5;
            this.lblLoggedAs.Text = "Logged In: Staff User";
            // 
            // menuStripStaffWelcomeForm
            // 
            this.menuStripStaffWelcomeForm.BackColor = System.Drawing.Color.Coral;
            this.menuStripStaffWelcomeForm.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStripStaffWelcomeForm.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiAddNewJob,
            this.tsmiSurvey,
            this.tsmiPartsAndRepair,
            this.tsmiEndJob,
            this.tsmiUpdateJobDetails,
            this.tsmiDeleteJob,
            this.tsmiJobList});
            this.menuStripStaffWelcomeForm.Location = new System.Drawing.Point(0, 0);
            this.menuStripStaffWelcomeForm.Name = "menuStripStaffWelcomeForm";
            this.menuStripStaffWelcomeForm.Size = new System.Drawing.Size(873, 28);
            this.menuStripStaffWelcomeForm.TabIndex = 6;
            this.menuStripStaffWelcomeForm.Text = "menuStrip1";
            // 
            // tsmiAddNewJob
            // 
            this.tsmiAddNewJob.Name = "tsmiAddNewJob";
            this.tsmiAddNewJob.Size = new System.Drawing.Size(110, 24);
            this.tsmiAddNewJob.Text = "Add New Job";
            this.tsmiAddNewJob.Click += new System.EventHandler(this.tsmiAddNewJob_Click);
            // 
            // tsmiSurvey
            // 
            this.tsmiSurvey.Name = "tsmiSurvey";
            this.tsmiSurvey.Size = new System.Drawing.Size(64, 24);
            this.tsmiSurvey.Text = "Survey";
            this.tsmiSurvey.Click += new System.EventHandler(this.tsmiSurvey_Click);
            // 
            // tsmiPartsAndRepair
            // 
            this.tsmiPartsAndRepair.Name = "tsmiPartsAndRepair";
            this.tsmiPartsAndRepair.Size = new System.Drawing.Size(128, 24);
            this.tsmiPartsAndRepair.Text = "Parts and Repair";
            this.tsmiPartsAndRepair.Click += new System.EventHandler(this.tsmiPartsAndRepair_Click);
            // 
            // tsmiEndJob
            // 
            this.tsmiEndJob.Name = "tsmiEndJob";
            this.tsmiEndJob.Size = new System.Drawing.Size(73, 24);
            this.tsmiEndJob.Text = "End Job";
            this.tsmiEndJob.Click += new System.EventHandler(this.tsmiEndJob_Click);
            // 
            // tsmiUpdateJobDetails
            // 
            this.tsmiUpdateJobDetails.Name = "tsmiUpdateJobDetails";
            this.tsmiUpdateJobDetails.Size = new System.Drawing.Size(147, 24);
            this.tsmiUpdateJobDetails.Text = "Update Job Details";
            this.tsmiUpdateJobDetails.Click += new System.EventHandler(this.tsmiUpdateJobDetails_Click);
            // 
            // tsmiDeleteJob
            // 
            this.tsmiDeleteJob.Name = "tsmiDeleteJob";
            this.tsmiDeleteJob.Size = new System.Drawing.Size(92, 24);
            this.tsmiDeleteJob.Text = "Delete Job";
            this.tsmiDeleteJob.Click += new System.EventHandler(this.tsmiDeleteJob_Click);
            // 
            // tsmiJobList
            // 
            this.tsmiJobList.Name = "tsmiJobList";
            this.tsmiJobList.Size = new System.Drawing.Size(70, 24);
            this.tsmiJobList.Text = "Job List";
            this.tsmiJobList.Click += new System.EventHandler(this.tsmiJobList_Click);
            // 
            // pbLogin
            // 
            this.pbLogin.Image = global::GUI.Properties.Resources.ASmanagment_App;
            this.pbLogin.Location = new System.Drawing.Point(166, 88);
            this.pbLogin.Name = "pbLogin";
            this.pbLogin.Size = new System.Drawing.Size(535, 342);
            this.pbLogin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLogin.TabIndex = 7;
            this.pbLogin.TabStop = false;
            // 
            // StaffWelcomeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(873, 522);
            this.Controls.Add(this.pbLogin);
            this.Controls.Add(this.menuStripStaffWelcomeForm);
            this.Controls.Add(this.lblLoggedAs);
            this.Name = "StaffWelcomeForm";
            this.Text = "Auto Shop Managment App";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.StaffWelcomeForm_FormClosing);
            this.Load += new System.EventHandler(this.StaffWelcomeForm_Load);
            this.menuStripStaffWelcomeForm.ResumeLayout(false);
            this.menuStripStaffWelcomeForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogin)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblLoggedAs;
        private System.Windows.Forms.MenuStrip menuStripStaffWelcomeForm;
        private System.Windows.Forms.ToolStripMenuItem tsmiAddNewJob;
        private System.Windows.Forms.ToolStripMenuItem tsmiSurvey;
        private System.Windows.Forms.ToolStripMenuItem tsmiPartsAndRepair;
        private System.Windows.Forms.ToolStripMenuItem tsmiEndJob;
        private System.Windows.Forms.ToolStripMenuItem tsmiUpdateJobDetails;
        private System.Windows.Forms.ToolStripMenuItem tsmiDeleteJob;
        private System.Windows.Forms.ToolStripMenuItem tsmiJobList;
        private System.Windows.Forms.PictureBox pbLogin;
    }
}