﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
using ApplicationLogic;
using DataAccessForm;

namespace GUI
{
    public partial class StaffAddNewJobForm : Form
    {


        public StaffAddNewJobForm()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void AddNewJobForm_Load(object sender, EventArgs e)
        {
            //on combobox cmbCurrentStatus, the Current Status wull be shown in the drop down and the value will be its StatusID. Only CurrentStatus with Start will be shown
            this.cmbCurrentStatus.DisplayMember = "CurrentStatus";
            this.cmbCurrentStatus.ValueMember = "StatusID";
            this.cmbCurrentStatus.DataSource = AutoShopService.GetAllJobStatus_StartJob();
            this.cmbCurrentStatus.Text = "Please Select";

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {

            //Validation if add button is clicked and important textboxex are left null
            if (txtName.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Please enter Name");
                return;
            }
            if (txtSurname.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Please enter Surname");
                return;
            }

            if (txtIdCard.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Please enter Id Card");
                return;
            }


            if (txtNumberPlate.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Please enter Number Plate");
                return;
            }
            if (txtContact.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Please enter Contact");
                return;
            }
            if (txtVehicleModel.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Please enter Vehicle Model");
                return;
            }
            if (txtDateCreated.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Please enter Date");
                return;
            }

            try
            {
                //on button Add click the values of the components will be added in database
                bool success = AutoShopService.AddJob(txtName.Text, txtSurname.Text, txtIdCard.Text, int.Parse(txtContact.Text), txtEmail.Text, txtAddress.Text, txtVehicleModel.Text, txtNumberPlate.Text, txtJobCreatedBy.Text, DateTime.Parse(txtDateCreated.Text), cmbCurrentStatus.SelectedIndex + 1);

                //Showing messagebox to the user informing him that entry was successful
                MessageBox.Show("New Job Created", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (SqlException ex)
            {
                //Showing messagebox to the user informing him that entry was not successful
                MessageBox.Show("Error Encountered whilst submitting New Job Order", "Database Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw ex;


            }



        }

        //_______________________________SearchJob Function_________________________________________________________________________________________________________________

        private void btnSearch_Click(object sender, EventArgs e)
        {
            //On search button click a messege box is shown and if the value entered matched the job number, the details will be shown in the form components
            string userInput = Microsoft.VisualBasic.Interaction.InputBox("Please Enter Job Number", "Database Search");
            // check user input is is null
            if (userInput != "")
            {
                int search = int.Parse(userInput);



                DataTable dt = AutoShopService.GetJobDetails(search);

                

                if (Convert.ToInt32(dt.Rows.Count.ToString()) > 0)
                {
                    this.txtJobNumber.Text = dt.Rows[0]["JobNumber"].ToString();
                    this.txtName.Text = dt.Rows[0]["Name"].ToString();
                    this.txtSurname.Text = dt.Rows[0]["Surname"].ToString();
                    this.txtIdCard.Text = dt.Rows[0]["IdCard"].ToString();
                    this.txtContact.Text = dt.Rows[0]["Contact"].ToString();
                    this.txtEmail.Text = dt.Rows[0]["Email"].ToString();
                    this.txtAddress.Text = dt.Rows[0]["Address"].ToString();
                    this.txtVehicleModel.Text = dt.Rows[0]["VehicleModel"].ToString();
                    this.txtNumberPlate.Text = dt.Rows[0]["NumberPlate"].ToString();
                    this.txtJobCreatedBy.Text = dt.Rows[0]["JobCreatedBy"].ToString();
                    this.txtDateCreated.Text = dt.Rows[0]["DateCreated"].ToString();
                    this.cmbCurrentStatus.Text = dt.Rows[0]["StatusIDFK"].ToString();
                    this.txtCurrentStatusD.Text = dt.Rows[0]["CurrentStatus"].ToString();

                }
                else
                {
                    AutoShopService.CloseConnection();

                }

            }
            else
            {
                MessageBox.Show("Job Number is required to find Job", "Job Search Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

            #region No Null Values Validation
            //_______________________________Validation AddNewJob_________________________________________________________________________________________________________________

            string Name1;
            string Surname;
            bool quit = true;
            string Address;
            object IdCard;
            int Contact = 0;
            DateTime DateCreated;
            string Email;
            string VehicleModel;
            string NumberPlate;
            string JobCreatedBy;
            int CurrentStatus;


            do
            {

                //--------------------------------------------Name (Validation)----------------------------------------------------------------------------------------------------

                //assigning the variable name with the submitted text in the textbox txtName
                Name1 = this.txtName.Text;

                //Checking if the name variable is not null
                if (Name1 != null)
                {
                    //Checking if the name variable is null
                    if (Name1 == "")
                    {
                        //Error checking - If the textbox component txtName is left blank a message box will be displayed an error will blinks next to the component
                        MessageBox.Show("Name is required. Please do not leave the textbox empty");


                        //The error message will inform the user to provide the Name.
                        this.errorProvider1.SetError(this.txtName, "Name is required. Kindly submit the Name");

                        //Setting the boolean variable to True to break the DO WHILE Loop
                        quit = true;

                        //Breaking the loop
                        break;
                    }

                    else
                    {
                        //name variable is not null and satisfies the condition of name is not null = true
                        quit = false;

                        //In case where the Error provider was initiated in a previous try, the error provider will be removed from screen
                        this.errorProvider1.Dispose();
                    }
                }

                else
                {
                    //in the case where the variable name is null, the loop will break
                    return;
                }

                //-----------------------------Surname (Validation)----------------------------------------------------------------------------------------------------

                //assigning the variable name with the submitted text in the textbox txtSurname
                Surname = this.txtSurname.Text;

                //Checking if the surnname variable is not null
                if (Surname != null)
                {
                    //Checking if the surname variable is null
                    if (Surname == "")
                    {
                        //Error checking - If the textbox component txtSurname is left blank a message box will be displayed an error will blinks next to the component
                        MessageBox.Show("Surname is required. Please do not leave the textbox empty");

                        //The error message will inform the user to provide the Teacher's Surname
                        this.errorProvider1.SetError(this.txtSurname, "Surname is required. Kindly submit the Surname");

                        //Setting the boolean variable to True to break the DO WHILE Loop
                        quit = true;

                        //Breaking the loop
                        break;
                    }

                    else
                    {
                        //surname variable is not null and satisfies the condition of surname is not null = true
                        quit = false;

                        //In case where the Error provider was initiated in a previous try, the error provider will be removed from screen
                        this.errorProvider1.Dispose();
                    }

                }

                else
                {
                    //in the case where the variable surname is null, the loop will break
                    return;
                }

                //-------------------------Address (Validation)----------------------------------------------------------------------------------------------------

                //assigning the variable address with the submitted text in the textbox txtAddress
                Address = this.txtAddress.Text;

                //Checking if the address variable is not null
                if (Address != null)
                {
                    //Checking if the address variable is null
                    if (Address == "")
                    {
                        //Error checking - If the textbox component txtAddress is left blank a message box will be displayed an error will blinks next to the component
                        MessageBox.Show("Address is required. Please do not leave the textbox empty");

                        //The error message will inform the user to provide the Teacher's Address
                        this.errorProvider1.SetError(this.txtAddress, "Address is required. Kindly submit the Address");

                        //Setting the boolean variable to True to break the DO WHILE Loop
                        quit = true;

                        //Breaking the loop
                        break;
                    }

                    else
                    {
                        //address variable is not null and satisfies the condition of address is not null = true
                        quit = false;

                        //In case where the Error provider was initiated in a previous try, the error provider will be removed from screen
                        this.errorProvider1.Dispose();
                    }
                }

                else
                {
                    //in the case where the variable surname is null, the loop will break
                    return;
                }

                //--------------------------IdCard (Validation)---------------------------------------------------------------------------------------------------------------

                //assigning the variable cID with the submitted text in the textbox txtIdCard
                IdCard = this.txtIdCard.Text;

                //Checking if the IdCard variable is not null
                if (IdCard != null)
                {
                    //Checking if the IdCard variable is null
                    if (IdCard == "")
                    {
                        //Error checking - If the textbox component txtIDCard is left blank a message box will be displayed an error will blinks next to the component
                        MessageBox.Show(" ID Card is required. Please do not leave the textbox empty");

                        //The error message will inform the user to provide the  ID Card
                        this.errorProvider1.SetError(this.txtIdCard, "ID Card Number is required. Kindly submit the ID Card");

                        //Setting the boolean variable to True to break the DO WHILE Loop
                        quit = true;

                        //Breaking the loop
                        break;
                    }

                    else
                    {
                        //cID variable is not null and satisfies the condition of cID is not null = true
                        quit = false;

                        //In case where the Error provider was initiated in a previous try, the error provider will be removed from screen
                        this.errorProvider1.Dispose();
                    }
                }

                else
                {
                    //in the case where the variable cID is null, the loop will break
                    return;
                }

                // ------------------------------Contact (Validation)---------------------------------------------------------------------------------------------------------

                //assigning the variable contact with the submitted text in the textbox txtContact
                //data submitted in the textbox txtContactNo is of type String and has to be casted to Integer prior assining it to the variable contact
                Contact = int.Parse(txtContact.Text);

                //Checking if the contact variable is not 0 (Zero)
                if (Contact != 0)
                {

                    //Checking if the contact variable is 0 (Zero)
                    if (Contact == 0)
                    {
                        //Error checking - If the textbox component txtContactNo is left blank a message box will be displayed an error will blinks next to the component
                        MessageBox.Show("Contact Number is required. Please submit correct Contact Number");

                        //The error message will inform the user to provide the ContactNo
                        this.errorProvider1.SetError(this.txtContact, "Contact Number is required. Kindly submit the Contact Number");

                        //Setting the boolean variable to True to break the DO WHILE Loop
                        quit = true;

                        //Breaking the loop
                        break;
                    }

                    else
                    {
                        //variable is not null and satisfies the condition of contact is not null = true
                        quit = false;

                        //In case where the Error provider was initiated in a previous try, the error provider will be removed from screen
                        this.errorProvider1.Dispose();
                    }
                }

                else
                {
                    //in the case where the variable contact is null, the loop will break
                    return;
                }

                // -----------------------CurrentStatus (Validation)---------------------------------------------------------------------------------------------------------

                //assigning the variable CurrentStatus with the submitted text in the textbox CurrentStatus
                //data submitted in the textbox txtContactNo is of type String and has to be casted to Integer prior assining it to the variable contact
                CurrentStatus = int.Parse(cmbCurrentStatus.Text);

                //Checking if the contact variable is not 0 (Zero)
                if (CurrentStatus != 0)
                {

                    //Checking if the contact variable is 0 (Zero)
                    if (CurrentStatus == 0)
                    {
                        //Error checking - If the textbox component CurrentStatus is left blank a message box will be displayed an error will blinks next to the component
                        MessageBox.Show("CurrentStatus is required. Please Select  CurrentStatus");

                        //The error message will inform the user to provide the CurrentStatus
                        this.errorProvider1.SetError(this.cmbCurrentStatus, "CurrentStatus is required. Please Select  CurrentStatus");

                        //Setting the boolean variable to True to break the DO WHILE Loop
                        quit = true;

                        //Breaking the loop
                        break;
                    }

                    else
                    {
                        // variable is not null and satisfies the condition of contact is not null = true
                        quit = false;

                        //In case where the Error provider was initiated in a previous try, the error provider will be removed from screen
                        this.errorProvider1.Dispose();
                    }
                }

                else
                {
                    //in the case where the variable contact is null, the loop will break
                    return;
                }

                // -----------------------DateCreated (Validation)---------------------------------------------------------------------------------------------------------

                DateCreated = DateTime.Parse(this.txtDateCreated.Text);

                //Checking if the txtDateCreated variable is not null
                if (DateCreated != null)
                {
                    //Checking if the dob variable is null
                    if (DateCreated > DateTime.Today)
                    {
                        //Error checking - If the textbox component txtDateCreated is left blank a message box will be displayed an error will blinks next to the component
                        MessageBox.Show("Date Created is invalid");

                        //The error message will inform the user to provide the txtDateCreated
                        this.errorProvider1.SetError(this.txtDateCreated, "Date Created is required. Kindly submit the Date Created");

                        //Setting the boolean variable to True to break the DO WHILE Loop
                        quit = true;

                        //Breaking the loop
                        break;
                    }

                    else
                    {
                        //variable is not null and satisfies the condition of dob is not null = true
                        quit = false;

                        //In case where the Error provider was initiated in a previous try, the error provider will be removed from screen
                        this.errorProvider1.Dispose();
                    }
                }

                else
                {
                    //in the case where the variable dob is null, the loop will break
                    return;
                }




                //---------------------------------Email(Validation)----------------------------------------------------------------------------------------------------

                //assigning the variable Email with the submitted text in the textbox txtEmail
                Email = this.txtEmail.Text;

                //Checking if the variable is not null
                if (Email != null)
                {
                    //Checking if the variable is null
                    if (Email == "")
                    {
                        //Error checking - If the textbox component Email is left blank a message box will be displayed an error will blinks next to the component
                        MessageBox.Show("Email is required. Please do not leave the textbox empty");

                        //The error message will inform the user to provide Email
                        this.errorProvider1.SetError(this.txtEmail, "Email is required. Kindly submit the Email");

                        //Setting the boolean variable to True to break the DO WHILE Loop
                        quit = true;

                        //Breaking the loop
                        break;
                    }

                    else
                    {
                        // variable is not null and satisfies the condition of address is not null = true
                        quit = false;

                        //In case where the Error provider was initiated in a previous try, the error provider will be removed from screen
                        this.errorProvider1.Dispose();
                    }
                }

                else
                {
                    //in the case where the variable is null, the loop will break
                    return;
                }

                //----------------------------VehicleModel(Validation)----------------------------------------------------------------------------------------------------

                //assigning the variable VehicleModel with the submitted text in the textbox txtVehicleModel
                VehicleModel = this.txtVehicleModel.Text;

                //Checking if the variable is not null
                if (VehicleModel != null)
                {
                    //Checking if the variable is null
                    if (VehicleModel == "")
                    {
                        //Error checking - If the textbox component VehicleModel is left blank a message box will be displayed an error will blinks next to the component
                        MessageBox.Show("VehicleModel is required. Please do not leave the textbox empty");

                        //The error message will inform the user to provide the VehicleModel
                        this.errorProvider1.SetError(this.txtVehicleModel, "VehicleModel is required. Kindly submit the VehicleModel");

                        //Setting the boolean variable to True to break the DO WHILE Loop
                        quit = true;

                        //Breaking the loop
                        break;
                    }

                    else
                    {
                        // variable is not null and satisfies the condition of address is not null = true
                        quit = false;

                        //In case where the Error provider was initiated in a previous try, the error provider will be removed from screen
                        this.errorProvider1.Dispose();
                    }
                }

                else
                {
                    //in the case where the variable surname is null, the loop will break
                    return;
                }

                //-------------------------------NumberPlate(Validation)----------------------------------------------------------------------------------------------------

                //assigning the variable NumberPlate with the submitted text in the textbox txtNumberPlate
                NumberPlate = this.txtNumberPlate.Text;

                //Checking if the NumberPlate variable is not null
                if (NumberPlate != null)
                {
                    //Checking if the NumberPlate variable is null
                    if (NumberPlate == "")
                    {
                        //Error checking - If the textbox component NumberPlate is left blank a message box will be displayed an error will blinks next to the component
                        MessageBox.Show("NumberPlate is required. Please do not leave the textbox empty");

                        //The error message will inform the user to provide the NumberPlate
                        this.errorProvider1.SetError(this.txtVehicleModel, "NumberPlate is required. Kindly submit the NumberPlate");

                        //Setting the boolean variable to True to break the DO WHILE Loop
                        quit = true;

                        //Breaking the loop
                        break;
                    }

                    else
                    {
                        //NumberPlate variable is not null and satisfies the condition of address is not null = true
                        quit = false;

                        //In case where the Error provider was initiated in a previous try, the error provider will be removed from screen
                        this.errorProvider1.Dispose();
                    }
                }

                else
                {
                    //in the case where the variable surname is null, the loop will break
                    return;
                }

                //---------------------------------JobCreatedBy(Validation)----------------------------------------------------------------------------------------------------

                //assigning the variable JobCreatedBy with the submitted text in the textbox txtJobCreatedBy
                JobCreatedBy = this.txtJobCreatedBy.Text;

                //Checking if the JobCreatedBy variable is not null
                if (JobCreatedBy != null)
                {
                    //Checking if the JobCreatedBy variable is null
                    if (JobCreatedBy == "")
                    {
                        //Error checking - If the textbox component JobCreatedBy is left blank a message box will be displayed an error will blinks next to the component
                        MessageBox.Show("JobCreatedBy is required. Please do not leave the textbox empty");

                        //The error message will inform the user to provide the JobCreatedBy
                        this.errorProvider1.SetError(this.txtJobCreatedBy, "JobCreatedBy is required. Kindly submit the CreatedBy");

                        //Setting the boolean variable to True to break the DO WHILE Loop
                        quit = true;

                        //Breaking the loop
                        break;
                    }

                    else
                    {
                        //JobCreatedBy variable is not null and satisfies the condition of address is not null = true
                        quit = false;

                        //In case where the Error provider was initiated in a previous try, the error provider will be removed from screen
                        this.errorProvider1.Dispose();
                    }
                }

                else
                {
                    //in the case where the variable surname is null, the loop will break
                    return;
                }

            } while (quit == true); //The DO Wile contine to loop until quit is not equal to true; meaning that all the logic was satisfied



            //_________________________________________________________________End Validation__________________________________________________________________________________________
            #endregion Null Values Validation


        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            //on clear button click all the compnents will be empty
            this.txtJobNumber.Text = "";
            this.txtName.Text = "";
            this.txtSurname.Text = "";
            this.txtIdCard.Text = "";
            this.txtContact.Text = "";
            this.txtEmail.Text = "";
            this.txtAddress.Text = "";
            this.txtVehicleModel.Text = "";
            this.txtNumberPlate.Text = "";
            this.txtJobCreatedBy.Text = "";
            this.txtDateCreated.Text = "";
            this.cmbCurrentStatus.Text = string.Empty;

            MessageBox.Show("Entries Cleared");

        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {

        }

    
                





        #region Data input Validation
        //_________________________________________________________________Start Data input Validation__________________________________________________________________________________________

        private void txtName_Leave(object sender, EventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtName.Text, "^[a-zA-Z ]"))
            {
                //If the character is a numeric character a message box is displayed to the user
                MessageBox.Show("This textbox accepts only Alphabetical characters");

                //Component text is reset
                this.txtName.ResetText();

                //Component regain focus
                this.txtName.Focus();

            }
        }

        private void txtSurname_Leave(object sender, EventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtSurname.Text, "^[a-zA-Z ]"))
            {
                //If the character is a numeric character a message box is displayed to the user
                MessageBox.Show("This textbox accepts only Alphabetical characters");

                //Component text is reset
                this.txtSurname.ResetText();

                //Component regain focus
                this.txtSurname.Focus();

            }
        }

        private void txtIdCard_Leave(object sender, EventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtIdCard.Text, "^[a-zA-Z0-9]"))
            {
                //If the character is not either a numeric character and alpha character a message box is displayed to the user
                MessageBox.Show("This textbox accepts only AlphaNumeric characters");

                //Component text is reset
                this.txtIdCard.ResetText();

                //Component regain focus
                this.txtIdCard.Focus();

            }

        }

        private void txtVehicleModel_Leave(object sender, EventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtVehicleModel.Text, "^[a-zA-Z ]"))
            {
                //If the character is a numeric character a message box is displayed to the user
                MessageBox.Show("This textbox accepts only Alphabetical characters");

                //Component text is reset
                this.txtVehicleModel.ResetText();

                //Component regain focus
                this.txtVehicleModel.Focus();

            }

        }

        private void txtContact_Leave(object sender, EventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtContact.Text, "^[0-9]"))
            {
                //If the character is a alphabetic character a message box is displayed to the user
                MessageBox.Show("This textbox accepts only Numeric characters");

                //Component text is reset
                this.txtContact.ResetText();

                //Component regain focus
                this.txtContact.Focus();


            }
        }
            private void txtNumberPlate_Leave(object sender, EventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtNumberPlate.Text, "^[a-zA-Z0-9]"))
            {
                //If the character is not either a numeric character and alpha character a message box is displayed to the user
                MessageBox.Show("This textbox accepts only AlphaNumeric characters");

                //Component text is reset
                this.txtNumberPlate.ResetText();

                //Component regain focus
                this.txtNumberPlate.Focus();

            }

        }

        private void txtEmail_Leave(object sender, EventArgs e)
        {


        }

        private void txtAddress_Leave(object sender, EventArgs e)
        {

        }

        private void cmbCurrentStatus_Leave(object sender, EventArgs e)
        {

        }

        private void txtDateCreated_Leave(object sender, EventArgs e)
        {

        }

        private void txtJobCreatedBy_Leave(object sender, EventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(this.txtJobCreatedBy.Text, "^[a-zA-Z ]"))
            {
                //If the character is a numeric character a message box is displayed to the user
                MessageBox.Show("This textbox accepts only Alphabetical characters");

                //Component text is reset
                this.txtJobCreatedBy.ResetText();

                //Component regain focus
                this.txtJobCreatedBy.Focus();

            }
        }

        private void txtJobNumber_Leave(object sender, EventArgs e)
        {

        }
        //_________________________________________________________________End Data input Validation__________________________________________________________________________________________
        #endregion Data input Validation
    }

}
 