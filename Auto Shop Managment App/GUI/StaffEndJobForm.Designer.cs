﻿namespace GUI
{
    partial class StaffEndJobForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblJobNumber = new System.Windows.Forms.Label();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblRepairNotes = new System.Windows.Forms.Label();
            this.txtRepairNotes = new System.Windows.Forms.RichTextBox();
            this.lblFaildPart = new System.Windows.Forms.Label();
            this.lblFaultType = new System.Windows.Forms.Label();
            this.cmbFaildPart = new System.Windows.Forms.ComboBox();
            this.cmbFaultType = new System.Windows.Forms.ComboBox();
            this.txtRepairedBy = new System.Windows.Forms.TextBox();
            this.lblRepairedBy = new System.Windows.Forms.Label();
            this.txtSurveyCompletedBy = new System.Windows.Forms.TextBox();
            this.lblJSurveyCompletedBy = new System.Windows.Forms.Label();
            this.lblCurrentStatusTitle = new System.Windows.Forms.Label();
            this.cmbCurrentStatus = new System.Windows.Forms.ComboBox();
            this.pnlCurrentStatus = new System.Windows.Forms.Panel();
            this.txtCurrentStatusD = new System.Windows.Forms.TextBox();
            this.lblDetailsTitle = new System.Windows.Forms.Label();
            this.pnlDetails = new System.Windows.Forms.Panel();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.lblAddress = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.txtSurname = new System.Windows.Forms.TextBox();
            this.lblSurname = new System.Windows.Forms.Label();
            this.txtNumberPlate = new System.Windows.Forms.TextBox();
            this.lblNumberPlate = new System.Windows.Forms.Label();
            this.txtVehicleModel = new System.Windows.Forms.TextBox();
            this.lblVehicleModel = new System.Windows.Forms.Label();
            this.txtContact = new System.Windows.Forms.TextBox();
            this.lblContact = new System.Windows.Forms.Label();
            this.txtIdCard = new System.Windows.Forms.TextBox();
            this.lblIDCard = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.lblLoggedAs = new System.Windows.Forms.Label();
            this.txtJobNumber = new System.Windows.Forms.TextBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.panel1.SuspendLayout();
            this.pnlCurrentStatus.SuspendLayout();
            this.pnlDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblJobNumber
            // 
            this.lblJobNumber.AutoSize = true;
            this.lblJobNumber.Location = new System.Drawing.Point(29, 39);
            this.lblJobNumber.Name = "lblJobNumber";
            this.lblJobNumber.Size = new System.Drawing.Size(89, 17);
            this.lblJobNumber.TabIndex = 58;
            this.lblJobNumber.Text = "Job Number:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblRepairNotes);
            this.panel1.Controls.Add(this.txtRepairNotes);
            this.panel1.Controls.Add(this.lblFaildPart);
            this.panel1.Controls.Add(this.lblFaultType);
            this.panel1.Controls.Add(this.cmbFaildPart);
            this.panel1.Controls.Add(this.cmbFaultType);
            this.panel1.Location = new System.Drawing.Point(17, 347);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1042, 272);
            this.panel1.TabIndex = 61;
            // 
            // lblRepairNotes
            // 
            this.lblRepairNotes.AutoSize = true;
            this.lblRepairNotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRepairNotes.Location = new System.Drawing.Point(12, 71);
            this.lblRepairNotes.Name = "lblRepairNotes";
            this.lblRepairNotes.Size = new System.Drawing.Size(112, 18);
            this.lblRepairNotes.TabIndex = 22;
            this.lblRepairNotes.Text = "Repair Notes:";
            // 
            // txtRepairNotes
            // 
            this.txtRepairNotes.Location = new System.Drawing.Point(15, 103);
            this.txtRepairNotes.Name = "txtRepairNotes";
            this.txtRepairNotes.ReadOnly = true;
            this.txtRepairNotes.Size = new System.Drawing.Size(1008, 135);
            this.txtRepairNotes.TabIndex = 2;
            this.txtRepairNotes.Text = "";
            // 
            // lblFaildPart
            // 
            this.lblFaildPart.AutoSize = true;
            this.lblFaildPart.Location = new System.Drawing.Point(494, 21);
            this.lblFaildPart.Name = "lblFaildPart";
            this.lblFaildPart.Size = new System.Drawing.Size(72, 17);
            this.lblFaildPart.TabIndex = 20;
            this.lblFaildPart.Text = "Faild Part:";
            // 
            // lblFaultType
            // 
            this.lblFaultType.AutoSize = true;
            this.lblFaultType.Location = new System.Drawing.Point(17, 21);
            this.lblFaultType.Name = "lblFaultType";
            this.lblFaultType.Size = new System.Drawing.Size(79, 17);
            this.lblFaultType.TabIndex = 19;
            this.lblFaultType.Text = "Fault Type:";
            // 
            // cmbFaildPart
            // 
            this.cmbFaildPart.FormattingEnabled = true;
            this.cmbFaildPart.Location = new System.Drawing.Point(581, 18);
            this.cmbFaildPart.Name = "cmbFaildPart";
            this.cmbFaildPart.Size = new System.Drawing.Size(291, 24);
            this.cmbFaildPart.TabIndex = 1;
            // 
            // cmbFaultType
            // 
            this.cmbFaultType.FormattingEnabled = true;
            this.cmbFaultType.Location = new System.Drawing.Point(107, 18);
            this.cmbFaultType.Name = "cmbFaultType";
            this.cmbFaultType.Size = new System.Drawing.Size(291, 24);
            this.cmbFaultType.TabIndex = 0;
            // 
            // txtRepairedBy
            // 
            this.txtRepairedBy.Location = new System.Drawing.Point(875, 309);
            this.txtRepairedBy.Name = "txtRepairedBy";
            this.txtRepairedBy.ReadOnly = true;
            this.txtRepairedBy.Size = new System.Drawing.Size(165, 22);
            this.txtRepairedBy.TabIndex = 69;
            // 
            // lblRepairedBy
            // 
            this.lblRepairedBy.AutoSize = true;
            this.lblRepairedBy.Location = new System.Drawing.Point(779, 312);
            this.lblRepairedBy.Name = "lblRepairedBy";
            this.lblRepairedBy.Size = new System.Drawing.Size(90, 17);
            this.lblRepairedBy.TabIndex = 68;
            this.lblRepairedBy.Text = "Repaired By:";
            // 
            // txtSurveyCompletedBy
            // 
            this.txtSurveyCompletedBy.Location = new System.Drawing.Point(875, 263);
            this.txtSurveyCompletedBy.Name = "txtSurveyCompletedBy";
            this.txtSurveyCompletedBy.ReadOnly = true;
            this.txtSurveyCompletedBy.Size = new System.Drawing.Size(165, 22);
            this.txtSurveyCompletedBy.TabIndex = 67;
            // 
            // lblJSurveyCompletedBy
            // 
            this.lblJSurveyCompletedBy.AutoSize = true;
            this.lblJSurveyCompletedBy.Location = new System.Drawing.Point(727, 265);
            this.lblJSurveyCompletedBy.Name = "lblJSurveyCompletedBy";
            this.lblJSurveyCompletedBy.Size = new System.Drawing.Size(147, 17);
            this.lblJSurveyCompletedBy.TabIndex = 66;
            this.lblJSurveyCompletedBy.Text = "Survey Completed By:";
            // 
            // lblCurrentStatusTitle
            // 
            this.lblCurrentStatusTitle.AutoSize = true;
            this.lblCurrentStatusTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentStatusTitle.Location = new System.Drawing.Point(666, 69);
            this.lblCurrentStatusTitle.Name = "lblCurrentStatusTitle";
            this.lblCurrentStatusTitle.Size = new System.Drawing.Size(147, 24);
            this.lblCurrentStatusTitle.TabIndex = 64;
            this.lblCurrentStatusTitle.Text = "Current Status:";
            // 
            // cmbCurrentStatus
            // 
            this.cmbCurrentStatus.FormattingEnabled = true;
            this.cmbCurrentStatus.Items.AddRange(new object[] {
            "Job Cancelled",
            "Vehicle Ready for Pickup"});
            this.cmbCurrentStatus.Location = new System.Drawing.Point(44, 47);
            this.cmbCurrentStatus.Name = "cmbCurrentStatus";
            this.cmbCurrentStatus.Size = new System.Drawing.Size(291, 24);
            this.cmbCurrentStatus.TabIndex = 0;
            this.cmbCurrentStatus.Text = "Select";
            // 
            // pnlCurrentStatus
            // 
            this.pnlCurrentStatus.Controls.Add(this.txtCurrentStatusD);
            this.pnlCurrentStatus.Controls.Add(this.cmbCurrentStatus);
            this.pnlCurrentStatus.Location = new System.Drawing.Point(670, 94);
            this.pnlCurrentStatus.Name = "pnlCurrentStatus";
            this.pnlCurrentStatus.Size = new System.Drawing.Size(370, 147);
            this.pnlCurrentStatus.TabIndex = 63;
            // 
            // txtCurrentStatusD
            // 
            this.txtCurrentStatusD.Location = new System.Drawing.Point(44, 93);
            this.txtCurrentStatusD.Name = "txtCurrentStatusD";
            this.txtCurrentStatusD.ReadOnly = true;
            this.txtCurrentStatusD.Size = new System.Drawing.Size(291, 22);
            this.txtCurrentStatusD.TabIndex = 2;
            // 
            // lblDetailsTitle
            // 
            this.lblDetailsTitle.AutoSize = true;
            this.lblDetailsTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDetailsTitle.Location = new System.Drawing.Point(28, 69);
            this.lblDetailsTitle.Name = "lblDetailsTitle";
            this.lblDetailsTitle.Size = new System.Drawing.Size(291, 24);
            this.lblDetailsTitle.TabIndex = 62;
            this.lblDetailsTitle.Text = "Customer and Vehicle Details:";
            // 
            // pnlDetails
            // 
            this.pnlDetails.Controls.Add(this.txtAddress);
            this.pnlDetails.Controls.Add(this.lblAddress);
            this.pnlDetails.Controls.Add(this.txtEmail);
            this.pnlDetails.Controls.Add(this.lblEmail);
            this.pnlDetails.Controls.Add(this.txtSurname);
            this.pnlDetails.Controls.Add(this.lblSurname);
            this.pnlDetails.Controls.Add(this.txtNumberPlate);
            this.pnlDetails.Controls.Add(this.lblNumberPlate);
            this.pnlDetails.Controls.Add(this.txtVehicleModel);
            this.pnlDetails.Controls.Add(this.lblVehicleModel);
            this.pnlDetails.Controls.Add(this.txtContact);
            this.pnlDetails.Controls.Add(this.lblContact);
            this.pnlDetails.Controls.Add(this.txtIdCard);
            this.pnlDetails.Controls.Add(this.lblIDCard);
            this.pnlDetails.Controls.Add(this.txtName);
            this.pnlDetails.Controls.Add(this.lblName);
            this.pnlDetails.Location = new System.Drawing.Point(17, 94);
            this.pnlDetails.Name = "pnlDetails";
            this.pnlDetails.Size = new System.Drawing.Size(625, 237);
            this.pnlDetails.TabIndex = 60;
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(107, 196);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.ReadOnly = true;
            this.txtAddress.Size = new System.Drawing.Size(492, 22);
            this.txtAddress.TabIndex = 21;
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(12, 196);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(64, 17);
            this.lblAddress.TabIndex = 20;
            this.lblAddress.Text = "Address:";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(107, 157);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.ReadOnly = true;
            this.txtEmail.Size = new System.Drawing.Size(492, 22);
            this.txtEmail.TabIndex = 19;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(12, 157);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(46, 17);
            this.lblEmail.TabIndex = 18;
            this.lblEmail.Text = "Email:";
            // 
            // txtSurname
            // 
            this.txtSurname.Location = new System.Drawing.Point(340, 22);
            this.txtSurname.Name = "txtSurname";
            this.txtSurname.ReadOnly = true;
            this.txtSurname.Size = new System.Drawing.Size(124, 22);
            this.txtSurname.TabIndex = 17;
            // 
            // lblSurname
            // 
            this.lblSurname.AutoSize = true;
            this.lblSurname.Location = new System.Drawing.Point(245, 22);
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Size = new System.Drawing.Size(69, 17);
            this.lblSurname.TabIndex = 16;
            this.lblSurname.Text = "Surname:";
            // 
            // txtNumberPlate
            // 
            this.txtNumberPlate.Location = new System.Drawing.Point(448, 112);
            this.txtNumberPlate.Name = "txtNumberPlate";
            this.txtNumberPlate.ReadOnly = true;
            this.txtNumberPlate.Size = new System.Drawing.Size(151, 22);
            this.txtNumberPlate.TabIndex = 15;
            // 
            // lblNumberPlate
            // 
            this.lblNumberPlate.AutoSize = true;
            this.lblNumberPlate.Location = new System.Drawing.Point(339, 113);
            this.lblNumberPlate.Name = "lblNumberPlate";
            this.lblNumberPlate.Size = new System.Drawing.Size(98, 17);
            this.lblNumberPlate.TabIndex = 14;
            this.lblNumberPlate.Text = "Number Plate:";
            // 
            // txtVehicleModel
            // 
            this.txtVehicleModel.Location = new System.Drawing.Point(448, 62);
            this.txtVehicleModel.Name = "txtVehicleModel";
            this.txtVehicleModel.ReadOnly = true;
            this.txtVehicleModel.Size = new System.Drawing.Size(151, 22);
            this.txtVehicleModel.TabIndex = 13;
            // 
            // lblVehicleModel
            // 
            this.lblVehicleModel.AutoSize = true;
            this.lblVehicleModel.Location = new System.Drawing.Point(339, 65);
            this.lblVehicleModel.Name = "lblVehicleModel";
            this.lblVehicleModel.Size = new System.Drawing.Size(100, 17);
            this.lblVehicleModel.TabIndex = 12;
            this.lblVehicleModel.Text = "Vehicle Model:";
            // 
            // txtContact
            // 
            this.txtContact.Location = new System.Drawing.Point(107, 116);
            this.txtContact.Name = "txtContact";
            this.txtContact.ReadOnly = true;
            this.txtContact.Size = new System.Drawing.Size(100, 22);
            this.txtContact.TabIndex = 9;
            // 
            // lblContact
            // 
            this.lblContact.AutoSize = true;
            this.lblContact.Location = new System.Drawing.Point(12, 116);
            this.lblContact.Name = "lblContact";
            this.lblContact.Size = new System.Drawing.Size(60, 17);
            this.lblContact.TabIndex = 8;
            this.lblContact.Text = "Contact:";
            // 
            // txtIdCard
            // 
            this.txtIdCard.Location = new System.Drawing.Point(107, 65);
            this.txtIdCard.Name = "txtIdCard";
            this.txtIdCard.ReadOnly = true;
            this.txtIdCard.Size = new System.Drawing.Size(100, 22);
            this.txtIdCard.TabIndex = 7;
            // 
            // lblIDCard
            // 
            this.lblIDCard.AutoSize = true;
            this.lblIDCard.Location = new System.Drawing.Point(12, 65);
            this.lblIDCard.Name = "lblIDCard";
            this.lblIDCard.Size = new System.Drawing.Size(59, 17);
            this.lblIDCard.TabIndex = 6;
            this.lblIDCard.Text = "ID Card:";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(107, 21);
            this.txtName.Name = "txtName";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(100, 22);
            this.txtName.TabIndex = 5;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(12, 21);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(49, 17);
            this.lblName.TabIndex = 4;
            this.lblName.Text = "Name:";
            // 
            // lblLoggedAs
            // 
            this.lblLoggedAs.AutoSize = true;
            this.lblLoggedAs.Location = new System.Drawing.Point(925, 647);
            this.lblLoggedAs.Name = "lblLoggedAs";
            this.lblLoggedAs.Size = new System.Drawing.Size(142, 17);
            this.lblLoggedAs.TabIndex = 65;
            this.lblLoggedAs.Text = "Logged In: Staff User";
            // 
            // txtJobNumber
            // 
            this.txtJobNumber.Location = new System.Drawing.Point(124, 39);
            this.txtJobNumber.Name = "txtJobNumber";
            this.txtJobNumber.ReadOnly = true;
            this.txtJobNumber.Size = new System.Drawing.Size(100, 22);
            this.txtJobNumber.TabIndex = 59;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Image = global::GUI.Properties.Resources.edit;
            this.btnUpdate.Location = new System.Drawing.Point(537, 647);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 42);
            this.btnUpdate.TabIndex = 73;
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Image = global::GUI.Properties.Resources.search_icon;
            this.btnSearch.Location = new System.Drawing.Point(425, 647);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 42);
            this.btnSearch.TabIndex = 72;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // StaffEndJobForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1079, 706);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.lblJobNumber);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtRepairedBy);
            this.Controls.Add(this.lblRepairedBy);
            this.Controls.Add(this.txtSurveyCompletedBy);
            this.Controls.Add(this.lblJSurveyCompletedBy);
            this.Controls.Add(this.lblCurrentStatusTitle);
            this.Controls.Add(this.pnlCurrentStatus);
            this.Controls.Add(this.lblDetailsTitle);
            this.Controls.Add(this.pnlDetails);
            this.Controls.Add(this.lblLoggedAs);
            this.Controls.Add(this.txtJobNumber);
            this.Name = "StaffEndJobForm";
            this.Text = "End Job";
            this.Load += new System.EventHandler(this.StaffEndJobForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlCurrentStatus.ResumeLayout(false);
            this.pnlCurrentStatus.PerformLayout();
            this.pnlDetails.ResumeLayout(false);
            this.pnlDetails.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblJobNumber;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblRepairNotes;
        private System.Windows.Forms.RichTextBox txtRepairNotes;
        private System.Windows.Forms.Label lblFaildPart;
        private System.Windows.Forms.Label lblFaultType;
        private System.Windows.Forms.ComboBox cmbFaildPart;
        private System.Windows.Forms.ComboBox cmbFaultType;
        private System.Windows.Forms.TextBox txtRepairedBy;
        private System.Windows.Forms.Label lblRepairedBy;
        private System.Windows.Forms.TextBox txtSurveyCompletedBy;
        private System.Windows.Forms.Label lblJSurveyCompletedBy;
        private System.Windows.Forms.Label lblCurrentStatusTitle;
        private System.Windows.Forms.ComboBox cmbCurrentStatus;
        private System.Windows.Forms.Panel pnlCurrentStatus;
        private System.Windows.Forms.Label lblDetailsTitle;
        private System.Windows.Forms.Panel pnlDetails;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.TextBox txtSurname;
        private System.Windows.Forms.Label lblSurname;
        private System.Windows.Forms.TextBox txtNumberPlate;
        private System.Windows.Forms.Label lblNumberPlate;
        private System.Windows.Forms.TextBox txtVehicleModel;
        private System.Windows.Forms.Label lblVehicleModel;
        private System.Windows.Forms.TextBox txtContact;
        private System.Windows.Forms.Label lblContact;
        private System.Windows.Forms.TextBox txtIdCard;
        private System.Windows.Forms.Label lblIDCard;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblLoggedAs;
        private System.Windows.Forms.TextBox txtJobNumber;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtCurrentStatusD;
    }
}