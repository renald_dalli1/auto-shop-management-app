﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GettersAndSetters
{
    //making the class PartsandRepair Serializable
    [Serializable]

    class PartsandRepair : JobDetails
    {
        //private attributes
        private string pr_RepairNotes, pr_RepairedBy, pr_PartNumber, pr_PartDescription, pr_Brand;
        private int pr_Quantity, pr_Price;

        //Default Constructor
        public PartsandRepair() { }


        //over-ride Constructor
        public PartsandRepair(string CurrentStatus, DateTime StatusUpdateDate, int JobNumber, string Name, string Surname, string VehicleModel,

        string NumberPlate, string RepairNotes, string RepairedBy, string PartNumber, string PartDescription, string Brand, int Quantity, int Price)

            //The Survey Over-ride constructor uses the attributes from the Base Class Job Details
             :base(CurrentStatus, StatusUpdateDate, JobNumber, Name, Surname, VehicleModel, NumberPlate)
        {

            this.pr_RepairNotes = RepairNotes;
            this.pr_RepairedBy = RepairedBy;
            this.pr_PartNumber= PartNumber;
            this.pr_PartDescription= PartDescription;
            this.pr_Brand = Brand;
            this.pr_Quantity = Quantity;
            this.pr_Price= Price;
        }

        //_____________________________________________________________________GET AND SET METHODS_____________________________________________________________

        /**
         * This method returns the RepairNotes of the PartsaAndRepair
         *
         * @return String
         * 
         */

        public string RepairNotes
        { 
            get
            {
                return this.pr_RepairNotes;
            }
            set
            {
                this.pr_RepairNotes = value;
            }
        }

        /**
        * This method returns the RepairedBy of the PartsaAndRepair
        *
        * @return String
        * 
        */

        public string RepairedBy
        {
            get
            {
                return this.pr_RepairedBy;
            }
            set
            {
                this.pr_RepairedBy = value;
            }
        }

        /**
        * This method returns the PartNumber of the PartsaAndRepair
        *
        * @return String
        * 
        */

        public string PartNumber
        {
            get
            {
                return this.pr_PartNumber;
            }
            set
            {
                this.pr_PartNumber = value;
            }
        }

        /**
        * This method returns the PartDescription of the PartsaAndRepair
        *
        * @return String
        * 
        */

        public string PartDescription
        {
            get
            {
                return this.pr_PartDescription;
            }
            set
            {
                this.pr_PartDescription = value;
            }
        }

        /**
        * This method returns the Brand of the PartsaAndRepair
        *
        * @return String
        * 
        */

        public string Brand
        {
            get
            {
                return this.pr_Brand;
            }
            set
            {
                this.pr_Brand = value;
            }
        }

        /**
        * This method returns the Quantity of the PartsaAndRepair
        *
        * @return String
        * 
        */

        public int Quantity
        {
            get
            {
                return this.pr_Quantity;
            }
            set
            {
                this.pr_Quantity = value;
            }
        }

        /**
        * This method returns the PartDescription of the PartsaAndRepair
        *
        * @return String
        * 
        */

        public int Price
        {
            get
            {
                return this.pr_Price;
            }
            set
            {
                this.pr_Price = value;
            }
        }
    }
}

