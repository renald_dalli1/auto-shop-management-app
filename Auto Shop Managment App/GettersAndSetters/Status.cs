﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GettersAndSetters
{
    //making the class Survey Serializable
    [Serializable]


    public class Status
    { 

        //private attributes
        private string st_CurrentStatus;
        private DateTime st_StatusUpdateDate;

        public Status() { }

        //Override constructor
        //In the over-ride constructor all the parameters will be passed and for any instance of the class JobDetails all the required parameters need to be passed

        public Status(string CurrentStatus, DateTime StatusUpdateDate)
       { 
            this.st_CurrentStatus = CurrentStatus;
            this.st_StatusUpdateDate = StatusUpdateDate;
            
        }


        //_____________________________________________________________________GET AND SET METHODS_____________________________________________________________


        /**
       * This method returns the CurrentStatus of the Status
       *
       * @return String
       * 
       */

        public string CurrentStatus
        {
            get
            {
                return this.st_CurrentStatus;
            }
            set
            {
                this.st_CurrentStatus = value;
            }
        }

        /**
        * This method returns the StatusUpdateDate of the Status
        *
        * @return String
        * 
        */


        public DateTime StatusUpdateDate
        {
            get
            {
                return this.st_StatusUpdateDate;
            }
            set
            {
                this.st_StatusUpdateDate = value;
            }
        }















    }

}
