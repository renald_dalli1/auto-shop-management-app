﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GettersAndSetters
{
    //making the class JobDetails Serializable
    [Serializable]

    public class JobDetails : Status
    {
        //private attributes
        private string jd_Name, jd_Surname, jd_IdCard, jd_Address, jd_Email, jd_VehicleModel, jd_NumberPlate, jd_JobCreatedBy;
        private DateTime jd_DateCreated;
        private int jd_JobNumber, jd_Contact;

        public JobDetails() { }

     
        //Base Constructor
        public JobDetails(string currentStatus, DateTime statusUpdateDate, int jobNumber, string name, string surname, string vehicleModel, string numberPlate)
        {
            CurrentStatus = currentStatus;
            StatusUpdateDate = statusUpdateDate;
            JobNumber = jobNumber;
            Name = name;
            Surname = surname;
            VehicleModel = vehicleModel;
            NumberPlate = numberPlate;
        }

        //Override constructor
        //In the over-ride constructor all the parameters will be passed and for any instance of the class JobDetails all the required parameters need to be passed

        public JobDetails(string CurrentStatus, DateTime StatusUpdateDate, int JobNumber, string Name, string Surname, string IdCard, string Address, string Email, string VehicleModel, string NumberPlate , int Contact, string JobCreatedBy, DateTime DateCreated)

            :base( CurrentStatus, StatusUpdateDate)

        {
            this.jd_JobNumber = JobNumber;
            this.jd_Name = Name;
            this.jd_Surname = Surname;
            this.jd_IdCard = IdCard;
            this.jd_Address = Address;
            this.jd_Email = Email;
            this.jd_NumberPlate = NumberPlate;
            this.jd_VehicleModel = VehicleModel;
            this.jd_Contact = Contact;
            this.jd_JobCreatedBy = JobCreatedBy;
            this.jd_DateCreated = DateCreated;

        }

        //_____________________________________________________________________GET AND SET METHODS_____________________________________________________________

        // This method returns the JobNumber of the JobDetails
        public int JobNumber
        {
            get
            {
                return this.jd_JobNumber;

            }
            set
            {
                this.jd_JobNumber = value;
            }
        }

        // This method returns the Name of the JobDetails
        public string Name
        {
            get
            {
                return this.jd_Name;
            
            }
            set
            {
                this.jd_Name = value;
            }
        }

        // This method returns the Surname of the JobDetails
        public string Surname
        {
            get
            {
                return this.jd_Surname;

            }
            set
            {
                this.jd_Surname = value;
            }
        }

        // This method returns the IDCard of the JobDetails
        public string IdCard
        {
            get
            {
                return this.jd_IdCard;

            }
            set
            {
                this.jd_IdCard = value;
            }
        }

        // This method returns the Contact of the JobDetails
        public int Contact
        {
            get
            {
                return this.jd_Contact;

            }
            set
            {
                this.jd_Contact = value;
            }
        }

        // This method returns the Email of the JobDetails
        public string Email
        {
            get
            {
                return this.jd_Email;

            }
            set
            {
                this.jd_Email = value;
            }
        }

        // This method returns the Address of the JobDetails
        public string Address
        {
            get
            {
                return this.jd_Address;

            }
            set
            {
                this.jd_Address = value;
            }
        }

        // This method returns the Vehicle Model of the JobDetails
        public string VehicleModel
        {
            get
            {
                return this.jd_VehicleModel;

            }
            set
            {
                this.jd_VehicleModel = value;
            }
        }

        // This method returns the NumberPlate of the JobDetails
        public string NumberPlate
        {
            get
            {
                return this.jd_NumberPlate;

            }
            set
            {
                this.jd_NumberPlate = value;
            }
        }

        // This method returns the JobCreatedBy of the JobDetails
        public String JobCreatedBy
        {
            get
            {
                return this.jd_JobCreatedBy;
            }
            set
            {
                this.jd_JobCreatedBy = value;
            }
        }

        // This method returns the DateCreated of the JobDetails
        public DateTime DateCreated
        {
            get
            {
                return this.jd_DateCreated;
            }
            set
            {
                this.jd_DateCreated = value;
            }
        }


    }
}
