﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GettersAndSetters
{
    //making the class Survey Serializable
    [Serializable]

      public class Survey : JobDetails
    {

        //private attributes
        private string s_FaultType, s_FailedPart, s_SurveyNotes, s_SurveyCompletedBy;
        int s_JobNumberFK;

        //Default Constructor
        public Survey() { }


        //over-ride Constructor
        public Survey(string CurrentStatus, DateTime StatusUpdateDate, int JobNumber, string Name, string Surname, string VehicleModel,

        string NumberPlate, string s_FaultType, string s_FailedPart, string s_SurveyNotes, string s_SurveyCompletedBy, int JobNumberFK)

            //The Survey Over-ride constructor uses the attributes from the Base Class Job Details
            : base(CurrentStatus, StatusUpdateDate, JobNumber, Name, Surname, VehicleModel, NumberPlate)
        {

            this.s_FaultType = FaultType;
            this.s_FailedPart = FailedPart;
            this.s_SurveyNotes = SurveyNotes;
            this.s_SurveyCompletedBy = SurveyCompletedBy;
            this.s_JobNumberFK = JobNumberFK;



        }

        //_____________________________________________________________________GET AND SET METHODS_____________________________________________________________


        /**
         * This method returns the FaultType of the Survey
         *
         * @return String
         * 
         */

        public string FaultType
        {
            get
            {
                return this.s_FaultType;
            }
            set
            {
                this.s_FaultType = value;
            }
        }

        /**
         * This method returns the FailedPart of the Survey
         *
         * @return String
         * 
         */

        public string FailedPart
        {
            get
            {
                return this.s_FailedPart;
            }
            set
            {
                this.s_FailedPart = value;
            }
        }

        /**
         * This method returns the SurveyNotes of the Survey
         *
         * @return String
         * 
         */

        public string SurveyNotes
        { 
            get
            {
                return this.s_SurveyNotes;
            }
            set
            {
                this.s_SurveyNotes = value;
            }
        }

        /**
         * This method returns the SurveyCompletedBy of the Survey
         *
         * @return String
         * 
         */

        public string SurveyCompletedBy
        {
            get
            {
                return this.s_SurveyCompletedBy;
            }
            set
            {
                this.s_SurveyCompletedBy = value;
            }
        }

        /**
         * This method returns the JobNumberFK of the Survey
         *
         * @return String
         * 
         */

        public int JobNumberFK
        {
            get
            {
                return this.s_JobNumberFK;
            }
            set
            {
                this.s_JobNumberFK = value;
            }
        }

    }
}
