﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessForm;

namespace ApplicationLogic
{
    public class JobDetailsService
    {
        JobDetailsDA objJDDA = new JobDetailsDA();
        public void InsertJob(int JobNumber, string Name, string Surname, string IdCard, int Contact, string Email, string Address, string VehicleModel, string NumberPlate, string JobCreatedBy, DateTime DateCreated)
        {
            objJDDA.InsertData(JobNumber, Name, Surname, IdCard, Contact, Email, Address, VehicleModel, NumberPlate, JobCreatedBy, DateCreated);


        }

        public object SelectJob()
        {
            return objJDDA.SelectData();

        }

    }
}
